package com.jslps.ultrapoor.listner


interface OnFragmentListItemSelectListener {
    fun onListItemSelected(itemId: Int, data: Any?)
}