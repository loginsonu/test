package com.jslps.ultrapoor.ui.patSurvey

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.network.response.ControlDataMasterJoin

class PatAdapterMultipleChoice(
    private val mContext: Context,
    private val section: List<ControlDataMasterJoin>?,
) : RecyclerView.Adapter<PatAdapterMultipleChoice.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_multiple_choice,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.check_Box.text = section!![position].ControlData

    }

    override fun getItemCount(): Int {
        return section!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var check_Box: CheckBox = itemView.findViewById(R.id.check_Box)

    }
}