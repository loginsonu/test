package com.jslps.ultrapoor.ui.endorsementprocess.finalselectedhhs

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.ui.endorsementprocess.EndorsementProcessModel
import com.jslps.ultrapoor.util.Coroutines


class FinalSelectedHHsAdapter(
    private val mContext: Context,
    private val endorseMentProcess: List<tblAnswer>?,
    private val repository: Repository

) : RecyclerView.Adapter<FinalSelectedHHsAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_final_selected_hhs,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        Coroutines.main {
            val data = repository.getDataForQuestion(endorseMentProcess!![position].HamletID,
                endorseMentProcess[position].CatID,
                endorseMentProcess[position].PatId,
                4)
            val data1 = repository.getDataForQuestion(endorseMentProcess[position].HamletID,
                endorseMentProcess[position].CatID,
                endorseMentProcess[position].PatId,
                5)
            val getDataForAnswer2 =
                repository.getSaveDataBySectionIdtestnew(endorseMentProcess[position].HamletID,
                    endorseMentProcess[position].CatID,
                    endorseMentProcess[position].PatId)
            if (data.isNotEmpty()) {
                holder.txtDidiName.text = data[0].AnswerValue
            }
            if (data1.isNotEmpty()) {
                holder.txtDadaName.text = data1[0].AnswerValue
            }
            var score =0
            if (getDataForAnswer2.isNotEmpty()) {
                for (i in getDataForAnswer2) {
                    if (!TextUtils.isEmpty(i.Score)) {
                        score += i.Score.toInt()
                    }
                }
                holder.txtSector.text = score.toString()
            }

        }
    }

    override fun getItemCount(): Int {
        return endorseMentProcess!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtDidiName: TextView = itemView.findViewById(R.id.txtDidiName)
        var txtDadaName: TextView = itemView.findViewById(R.id.txtDadaName)
        var txtSector: TextView = itemView.findViewById(R.id.txtSector)
        var imvApproved: ImageView = itemView.findViewById(R.id.imvApproved)

    }
}