package com.jslps.ultrapoor.ui.section

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.network.response.ActivityListMaster
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines

class SectionAdapter(
    private val mContext: Context,
    private val section: List<ActivityListMaster>?,
    private val repository: Repository,

    ) : RecyclerView.Adapter<SectionAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.hamlet_section_list,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.txtSection.text = section!![position].ActivityName

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, QuestionListPage::class.java)
            intent.putExtra("sectionObject", section[position])
            mContext.startActivity(intent)
        }


        Coroutines.main {
            val getdata = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                section[position].SequNo,"")
            if (getdata.isNotEmpty()) {
                holder.imageView3.visibility = View.VISIBLE
            } else {
                holder.imageView3.visibility = View.GONE
            }

            val data5 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                1,
                AppConstant.patId)

            val data6 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                2,
                AppConstant.patId)
            val data7 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                3,
                AppConstant.patId)

            val data8= repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                4,
                AppConstant.patId)
            val data9= repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                5,
                AppConstant.patId)
            val data10 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                6,
                AppConstant.patId)
            val data11 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                7,
                AppConstant.patId)
            val data12 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                8,
                AppConstant.patId)
            val data13 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                9,
                AppConstant.patId)
            val data14 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                10,
                AppConstant.patId)

            if (data5.isEmpty()) {
                if (section[position].SequNo > 1) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f
                }
            }
            else if (data6.isEmpty()) {
                if (section[position].SequNo > 2) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
            else if (data7.isEmpty()) {
                if (section[position].SequNo > 3) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
            else if (data8.isEmpty()) {
                if (section[position].SequNo > 4) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }

            else if (data9.isEmpty()) {
                if (section[position].SequNo > 5) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
            else if (data10.isEmpty()) {
                if (section[position].SequNo >6) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
            else if (data11.isEmpty()) {
                if (section[position].SequNo > 7) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
            else if (data12.isEmpty()) {
                if (section[position].SequNo > 8) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
            else if (data13.isEmpty()) {
                if (section[position].SequNo > 9) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
            else if (data14.isEmpty()) {
                if (section[position].SequNo > 10) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }
        }

    }

    override fun getItemCount(): Int {
        return section!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtSection: TextView = itemView.findViewById(R.id.txtSection)
        var layoutaaay: ConstraintLayout = itemView.findViewById(R.id.layoutaaay)
        var imageView3: ImageView = itemView.findViewById(R.id.imageView3)

    }
}