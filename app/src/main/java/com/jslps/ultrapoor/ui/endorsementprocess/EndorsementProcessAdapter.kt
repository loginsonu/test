package com.jslps.ultrapoor.ui.endorsementprocess

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.listner.OnListItem
import com.jslps.ultrapoor.data.listner.OnListItemSelected
import com.jslps.ultrapoor.data.network.response.RejectReasonModelDb
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.ui.endorsementprocess.hhsprofile.HHsProfileActivity
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast

class EndorsementProcessAdapter(
    private val mContext: Context,
    private val endorseMentProcess: List<tblAnswer>?,
    private val repository: Repository,
    private val getListreason: List<RejectReasonModelDb>,

    ) : RecyclerView.Adapter<EndorsementProcessAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_endorment_process,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val datak = endorseMentProcess as ArrayList<tblAnswer>
        var score = 0
        Coroutines.main {

            val status = endorseMentProcess!![position].statusAcepptandRej

            when (status) {
                1 -> {
                    holder.txtApproved.text = "Ultra poor house"
                    holder.txtApproved.setTextColor(ContextCompat.getColor(mContext, R.color.green))
                    holder.txtApproved.visibility = View.VISIBLE
                    holder.ll_ApproveReject.visibility = View.GONE
                }
                2 -> {
                    holder.txtApproved.text = "Not Ultra poor house"
                    holder.txtApproved.setTextColor(Color.RED)
                    holder.txtApproved.visibility = View.VISIBLE
                    holder.ll_ApproveReject.visibility = View.GONE

                }
                else -> {
                    holder.txtApproved.visibility = View.GONE
                    holder.ll_ApproveReject.visibility = View.VISIBLE
                }
            }

            val data = repository.getDataForQuestion(endorseMentProcess!![position].HamletID,
                endorseMentProcess[position].CatID,
                endorseMentProcess[position].PatId,
                4)
            val data1 = repository.getDataForQuestion(endorseMentProcess[position].HamletID,
                endorseMentProcess[position].CatID,
                endorseMentProcess[position].PatId,
                5)
            val getDataForAnswer2 =
                repository.getSaveDataBySectionIdtestnew(endorseMentProcess[position].HamletID,
                    endorseMentProcess[position].CatID,
                    endorseMentProcess[position].PatId)
            if (data.isNotEmpty()) {
                holder.txtDidiName.text = data[0].AnswerValue
            }
            if (data1.isNotEmpty()) {
                holder.txtDadaName.text = data1[0].AnswerValue
            }
            if (getDataForAnswer2.isNotEmpty()) {
                for (i in getDataForAnswer2) {
                    if (!TextUtils.isEmpty(i.Score)) {
                        score += i.Score.toInt()
                    }
                }
               /* if (score >= 6) {
                    holder.mainlayout.visibility = View.VISIBLE
                    holder.txtSector.text = score.toString()
                } else {
                    holder.mainlayout.visibility = View.GONE
                    endorseMentProcess.removeAt(position)
                    notifyDataSetChanged()
                }*/
                holder.txtSector.text = score.toString()

            }/*else{
                holder.mainlayout.visibility = View.GONE
                endorseMentProcess.removeAt(position)
                notifyDataSetChanged()


            }*/

        }

        holder.ll_ProfileItem.setOnClickListener {
            val endorsementProcessModel =
                EndorsementProcessModel(holder.txtDidiName.text.toString(),
                    holder.txtDadaName.text.toString(), holder.txtSector.text.toString())
            val intent = Intent(mContext, HHsProfileActivity::class.java)
            intent.putExtra("endmes", endorseMentProcess!![position])
            intent.putExtra("hhsProfile", endorsementProcessModel)
            mContext.startActivity(intent)
        }

        holder.imvReject.setOnClickListener {

            popUpReject(endorseMentProcess!![position].HamletID,
                endorseMentProcess[position].CatID,
                endorseMentProcess[position].PatId)
        }
        holder.imvApproved.setOnClickListener {
            onListItemSelected?.onListItemSelected(11, endorseMentProcess!![position])
        }

    }

    override fun getItemCount(): Int {
        return endorseMentProcess!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtDidiName: TextView = itemView.findViewById(R.id.txtDidiName)
        var txtDadaName: TextView = itemView.findViewById(R.id.txtDadaName)
        var txtSector: TextView = itemView.findViewById(R.id.txtSector)
        var txtApproved: TextView = itemView.findViewById(R.id.txtApproved)
        var imvApproved: ImageView = itemView.findViewById(R.id.imvApproved)
        var imvReject: ImageView = itemView.findViewById(R.id.imvReject)
        var ll_ProfileItem: LinearLayout = itemView.findViewById(R.id.ll_ProfileItem)
        var ll_ApproveReject: LinearLayout = itemView.findViewById(R.id.ll_ApproveReject)
        var mainlayout: LinearLayout = itemView.findViewById(R.id.mainlayout)

    }


    private fun popUpReject(hamletId: String, calId: Int, patId: String) {

        var reasonCode = ""
        var rejectReason = ""
        val dialog = BottomSheetDialog(mContext)
        val view1 = LayoutInflater.from(mContext).inflate(
            R.layout.dialog_approve, null)
        val imvClosePopUp: ImageView = view1.findViewById(R.id.imvClosePopUp)
        val edtReason: EditText = view1.findViewById(R.id.edtReason)
        val btnSaveReason: Button = view1.findViewById(R.id.btnSaveReason)
        val spReject: Spinner = view1.findViewById(R.id.spReject)

        val adapter = ArrayAdapter(
            mContext,
            android.R.layout.simple_expandable_list_item_1, getListreason
        )
        spReject.adapter = adapter

        spReject.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long,
            ) {

                rejectReason = getListreason[position].ReasonForRejection!!
                reasonCode = getListreason[position].RID.toString()
                if (rejectReason == "अन्य") {
                    edtReason.visibility = View.VISIBLE
                } else {
                    edtReason.visibility = View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        btnSaveReason.setOnClickListener {


            Coroutines.main {

                if (edtReason.text.isEmpty()) {
                    repository.updateApproveData(rejectReason, reasonCode, calId, patId, hamletId)
                    onListItemSelected?.onListItemSelected(12, endorseMentProcess!![0])
                } else {
                    repository.updateApproveData(edtReason.text.toString(),
                        reasonCode,
                        calId,
                        patId,
                        hamletId)
                    onListItemSelected?.onListItemSelected(12, endorseMentProcess!![0])

                }


            }

            dialog.dismiss()
        }

        imvClosePopUp.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setContentView(view1)
        dialog.setCancelable(false)
        dialog.show()
    }

    var onListItemSelected: OnListItemSelected? = null
    fun setListner(endorsementProcessActivity: OnListItemSelected) {
        onListItemSelected = endorsementProcessActivity
    }
}