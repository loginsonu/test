package com.jslps.ultrapoor.ui.section

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.response.AnswerValueFill
import com.jslps.ultrapoor.data.network.response.AnswerValueFillNew
import com.jslps.ultrapoor.data.network.response.ControlDataMasterJoin
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.listner.OnFragmentListItemSelectListener
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast
import java.util.*
import kotlin.collections.ArrayList

class AdapterSecondMultipleChoice(
    private val mContext: Context,
    private var arrDataQuestionControl: List<ControlDataMasterJoin>,
    private val repository: Repository,
    private val updateDataBySectionId: ArrayList<tblAnswer>?,
    private val questionNo: String,
    private val answerId: String,
) : RecyclerView.Adapter<AdapterSecondMultipleChoice.MainViewHolder>(),
    OnFragmentListItemSelectListener {
    var arrayList: ArrayList<AnswerModel>? = ArrayList()
    var mListner: OnFragmentListItemSelectListener? = null

    fun setListner(listner: OnFragmentListItemSelectListener?) {
        this.mListner = listner
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_multiple_choice,
            parent, false
        )
        return MainViewHolder(view)
    }

    var arrayListAnswerFillData: ArrayList<AnswerValueFill>? = ArrayList()
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        val section = arrDataQuestionControl[position]
        holder.check_Box.text = section.ControlData
        if (AppConstant.dataSaveList!!.isNotEmpty()) {
            for (i in AppConstant.dataSaveList!!) {

                if (i.answerData.toString() != "") {
                    if (i.ControlID == 4) {
                        if (i.Qid == section.QID) {
                            if (i.answerData == section.ControlData) {
                                i.answerId = section.CID.toString()
                                holder.check_Box.isChecked = true
                            }
                        }

                    }
                }
            }
        }
        for (i in AppConstant.dataSaveList!!.indices) {
            if (AppConstant.dataSaveList!![i].Qid == section.QID) {
                if (AppConstant.dataSaveList!![i].answerData == section.ControlData) {
                    AppConstant.dataSaveList!![i].answerId = answerId
                    AppConstant.dataSaveList!![i].answerIdDelete = section.CID.toString()
                    AppConstant.dataSaveList!![i].questionToBEAdd = questionNo
                }
            }
        }
        holder.check_Box.setOnCheckedChangeListener { compoundButton, boolean ->
            if (boolean) {
                var count = 0
                var checkvaluee = true
                for (i in AppConstant.dataSaveList!!.indices) {


                    if (AppConstant.dataSaveList!![i].Qid == section.QID) {

                        if (AppConstant.dataSaveList!![i].answerData == "") {
                            AppConstant.dataSaveList!![i].answerData = section.ControlData
                            AppConstant.dataSaveList!![i].answerId = section.CID.toString()
                            AppConstant.dataSaveList!![i].questionToBEAdd = questionNo
                        } else {
                            if (AppConstant.dataSaveList!![i].answerId.toString() != section.CID.toString()) {
                                count++
                                AppConstant.dataSaveList?.add(
                                    AnswerValueFillNew(2,
                                        4,
                                        section.QID,
                                        "MultiSelect",
                                        section.SectionID,
                                        section.ControlData,
                                        questionNo,
                                        section.CID.toString(),
                                        "",
                                        0,
                                        section.CID.toString(),
                                        UUID.randomUUID().toString()))
                                break

                            } else {
                                if (AppConstant.dataSaveList!![i].answerData == section.ControlData) {
                                    AppConstant.dataSaveList!![i].answerId = section.CID.toString()
                                    AppConstant.dataSaveList!![i].answerIdDelete = section.CID.toString()
                                    AppConstant.dataSaveList!![i].questionToBEAdd = questionNo
                                }
                            }

                        }
                    }

                }

            } else {
                try {
                    var count = 0
                    for (i in AppConstant.dataSaveList!!.indices) {
                        if (AppConstant.dataSaveList!![i].Qid == section.QID) {
                            count++
                        }
                    }
                    for (j in AppConstant.dataSaveList!!.indices) {
                        if (count > 1) {
                            if (AppConstant.dataSaveList!![j].Qid == section.QID) {
                                if (AppConstant.dataSaveList!![j].answerId == section.CID.toString()) {
                                    AppConstant.dataSaveList!!.removeAt(j)
                                }
                            }
                        } else {
                            if (AppConstant.dataSaveList!![j].Qid == section.QID) {
                                if (AppConstant.dataSaveList!![j].answerId == section.CID.toString()) {
                                    AppConstant.dataSaveList!![j].answerData = ""
                                    AppConstant.dataSaveList!![j].answerId = ""
                                }
                            }
                        }

                    }
                } catch (e: Exception) {

                }
                try {

                    for (i in AppConstant.dataSaveList!!.indices) {
                        if (AppConstant.dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                            if (AppConstant.dataSaveList!![i].answerIdDelete == section.CID.toString()) {
                                AppConstant.dataSaveList!!.removeAt(i)
                            }

                        }
                    }
                } catch (e: Exception) {
                    try {

                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (AppConstant.dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                                if (AppConstant.dataSaveList!![i].answerIdDelete == section.CID.toString()) {
                                    AppConstant.dataSaveList!!.removeAt(i)
                                }

                            }
                        }
                    } catch (e: Exception) {
                        try {

                            for (i in AppConstant.dataSaveList!!.indices) {
                                if (AppConstant.dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                                    if (AppConstant.dataSaveList!![i].answerIdDelete == section.CID.toString()) {
                                        AppConstant.dataSaveList!!.removeAt(i)
                                    }

                                }
                            }
                        } catch (e: Exception) {
                            try {

                                for (i in AppConstant.dataSaveList!!.indices) {
                                    if (AppConstant.dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                                        if (AppConstant.dataSaveList!![i].answerIdDelete == section.CID.toString()) {
                                            AppConstant.dataSaveList!!.removeAt(i)
                                        }

                                    }
                                }
                            } catch (e: Exception) {

                            }
                        }
                    }
                }
                mListner?.onListItemSelected(11, arrayListAnswerFillData)

            }

        }

    }

    override fun getItemCount(): Int {
        return arrDataQuestionControl.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var check_Box: CheckBox = itemView.findViewById(R.id.check_Box)

    }

    override fun onListItemSelected(itemId: Int, data: Any?) {
        if (itemId == 11) {
            val dataRec = data as AnswerValueFill
            var check = true
            Coroutines.mainIo() {
                if (arrayListAnswerFillData!!.isNotEmpty()) {
                    for (i in arrayListAnswerFillData!!.indices) {
                        val uniqueCode =
                            (arrayListAnswerFillData!![i].Qid.toString() +
                                    arrayListAnswerFillData!![i].CatID.toString() + arrayListAnswerFillData!![i].SectionID)
                        val uniqueCode1 =
                            (dataRec.Qid.toString() +
                                    dataRec.CatID.toString() + dataRec.SectionID)
                        if (uniqueCode == uniqueCode1) {
                            arrayListAnswerFillData!!.set(i, dataRec)
                            check = false
                        }
                        if (i == (arrayListAnswerFillData!!.size - 1)) {
                            if (check) {
                                arrayListAnswerFillData!!.add(dataRec)

                            }
                        }
                    }
                } else {
                    arrayListAnswerFillData!!.add(dataRec)
                }
                mListner?.onListItemSelected(11, arrayListAnswerFillData)


            }
        } else {
            val dataRec = data as AnswerValueFill
            var check = true
            Coroutines.mainIo() {
                if (arrayListAnswerFillData!!.isNotEmpty()) {
                    for (i in arrayListAnswerFillData!!.indices) {
                        val uniqueCode =
                            (arrayListAnswerFillData!![i].Qid.toString() +
                                    arrayListAnswerFillData!![i].CatID.toString() + arrayListAnswerFillData!![i].SectionID)
                        val uniqueCode1 =
                            (dataRec.Qid.toString() +
                                    dataRec.CatID.toString() + dataRec.SectionID)
                        if (uniqueCode == uniqueCode1) {
                            arrayListAnswerFillData!!.set(i, dataRec)
                            check = false
                        }
                        if (i == (arrayListAnswerFillData!!.size - 1)) {
                            if (check) {
                                arrayListAnswerFillData!!.add(dataRec)
                            }
                            mListner?.onListItemSelected(11, arrayListAnswerFillData)
                        }
                    }
                } else {
                    arrayListAnswerFillData!!.add(dataRec)
                    mListner?.onListItemSelected(11, arrayListAnswerFillData)
                }


            }
        }
    }
}