package com.jslps.ultrapoor.ui.section

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.Coroutines

import kotlinx.android.synthetic.main.question_section_activity.*
import kotlinx.android.synthetic.main.tool_bar.*



class QuestionSectionPage : AppCompatActivity() {
    private lateinit var repository: Repository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide()
        setContentView(R.layout.question_section_activity)
        val api2 = MyApiNew(networkConnectionInterceptor)

        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        repository = Repository(api, db, api2)

    }

    override fun onResume() {
        super.onResume()
        Coroutines.main {

            //GET USER PROFILE DATA FROM USER PROFILE TABLE
            repository.getUserProfileData().let {
                districttext.text = it.districtname_h
                blocktext.text = it.blockName_H
                Clustertext.text = it.clusterName_H
                Villegetext.text = AppConstant.villageName
                Hamlettext.text = AppConstant.hamletName
                txtHeader.text = "अनुभाग सूची"
            }

            val activityList = repository.getQuestionSection(2)
            val adpSection =
                SectionAdapter(this, activityList,repository)
            recycSection.adapter = adpSection
            recycSection.visibility = View.VISIBLE
        }
    }
}