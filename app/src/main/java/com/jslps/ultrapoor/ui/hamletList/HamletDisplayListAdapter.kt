package com.jslps.ultrapoor.ui.hamletList

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AddHamlettbl
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.ui.endorsementprocess.EndorsementProcessActivity
import com.jslps.ultrapoor.ui.patList.ListPatPage
import com.jslps.ultrapoor.ui.section.QuestionSectionPage
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast


class HamletDisplayListAdapter(
    private val mContext: Context,
    private val addHamlettbl: List<AddHamlettbl>?,
    private val repository: Repository,
) : RecyclerView.Adapter<HamletDisplayListAdapter.MailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MailViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.hamlet_list,
            parent, false
        )
        return MailViewHolder(view)
    }

    override fun onBindViewHolder(holder: MailViewHolder, position: Int) {
        holder.price.text = addHamlettbl!![position].hamletName

//        holder.ProfileSurvey.setOnClickListener {
//            AppConstant.hamletName = addHamlettbl[position].hamletName!!
//            AppConstant.hamletDetails = "PROFILE DETAILS"
//            AppConstant.HamletId = addHamlettbl[position].hamletId.toString()
//            AppConstant.patId = ""
//            mContext.startActivity(Intent(mContext, QuestionSectionPage::class.java))
//        }
        holder.PatSurvey.setOnClickListener {

            AppConstant.hamletName = addHamlettbl[position].hamletName!!
            AppConstant.hamletDetails = "PAT DETAILS"
            AppConstant.HamletId = addHamlettbl[position].hamletId.toString()
            mContext.startActivity(Intent(mContext, ListPatPage::class.java))
        }
        holder.endonmentSurvey.setOnClickListener {
            var score = 0
            Coroutines.main {
                val getDataForAnswer1 =
                    repository.getSaveDataBySectionIdtest(addHamlettbl[position].hamletId.toString(),
                        1)
                /*val getDataForAnswer2 =
                    repository.getSaveDataBySectionIdtest(addHamlettbl[position].hamletId.toString(),
                        2)*/
                if (getDataForAnswer1.isNotEmpty() /*&& getDataForAnswer2.isNotEmpty()*/) {
                    for (i in getDataForAnswer1) {
                        if (!TextUtils.isEmpty(i.Score)) {
                            score += i.Score.toInt()
                        }
                    }
                    if (score != 0) {
                        AppConstant.hamletName = addHamlettbl[position].hamletName!!
                        AppConstant.HamletId = addHamlettbl[position].hamletId.toString()
                        mContext.startActivity(Intent(mContext, EndorsementProcessActivity::class.java))
                    } else {
                        mContext.toast("यह हैमलेट का स्कोर 0 से कम या उसके बराबर है, कृपया कुछ अच्छे स्कोर परिणाम चुनें।")
                    }
                } else {
//                    mContext.toast("कृपया पहले पैट और हैमलेट सर्वेक्षण डेटा दर्ज करें")
                    mContext.toast("कृपया पहले पैट सर्वेक्षण का डेटा दर्ज करें")
                }
            }

        }


    }


    override fun getItemCount(): Int {
        return addHamlettbl!!.size
    }

    inner class MailViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var price: TextView = itemView.findViewById(R.id.price)
//        var ProfileSurvey: Button = itemView.findViewById(R.id.ProfileSurvey)
        var PatSurvey: Button = itemView.findViewById(R.id.PatSurvey)
        var endonmentSurvey: Button = itemView.findViewById(R.id.endonmentSurvey)

    }
}