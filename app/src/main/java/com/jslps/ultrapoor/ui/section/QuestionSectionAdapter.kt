package com.jslps.ultrapoor.ui.section


import android.content.Context
import android.location.LocationManager
import android.text.*
import android.text.InputFilter.LengthFilter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.response.AnswerValueFill
import com.jslps.ultrapoor.data.network.response.AnswerValueFillNew
import com.jslps.ultrapoor.data.network.response.ControlDataMasterJoin
import com.jslps.ultrapoor.data.network.response.QuestionTypeMaster
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.listner.OnFragmentListItemSelectListener
import com.jslps.ultrapoor.util.*
import java.text.DecimalFormat
import java.util.*
import java.util.regex.Pattern


class QuestionSectionAdapter(
    private val mContext: Context,
    private val listQuestion: List<QuestionTypeMaster>?,
    private var arrDataQuestionControl: List<ControlDataMasterJoin>,
    val repository: Repository,
    public var arrayListAnswerFillData: ArrayList<AnswerValueFill>,
    private val updateDataBySectionId: ArrayList<tblAnswer>?,

    ) : RecyclerView.Adapter<QuestionSectionAdapter.MainViewHolder>(),
    OnFragmentListItemSelectListener/*,
    AdapterView.OnItemSelectedListener*/ {
    var couts = 0
    lateinit var arrData: ArrayList<String>

    // lateinit var item: ArrayList<QuestionTypeMaster>
    // var hashMap = HashMap<String, AnswerModel>()

    private lateinit var locationManager: LocationManager

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_questions,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        val filter =
            InputFilter { source, start, end, dest, dstart, dend ->
                for (i in start until end) {
                    if (!Pattern.compile("[1234567890]*")
                            .matcher(
                                source[i].toString()).matches()
                    ) {
                        return@InputFilter ""
                    }
                }
                null
            }
        val item = listQuestion!![position]

        Log.d("VALUE==", item.Qid.toString())
        val controlId = item.ControlID
        holder.txtQuestion.text = (position + 1).toString() + ". " + item.Qname
        if (controlId == 1) {
            holder.edtHamName.visibility = View.VISIBLE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.txtCaptureLocation.visibility = View.GONE
            holder.ll_geoLocation.visibility = View.GONE
            if (updateDataBySectionId!!.isNotEmpty()) {
                for (i in updateDataBySectionId) {
                    if (i.answer.toString() != "") {
                        if (i.ControlTypeID == 1) {
                            if (i.QID == item.Qid) {
                                holder.edtHamName.text = i.AnswerValue
                            }
                        }
                    }
                }
            }

        } else if (controlId == 2) {
            holder.spDropDown.visibility = View.VISIBLE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.edtHamName.visibility = View.GONE
            holder.txtCaptureLocation.visibility = View.GONE
            holder.ll_geoLocation.visibility = View.GONE

            Coroutines.main {
                val arrSpinner = repository.getControlMasterData(item.Staus, item.Qid, 2)
                arrData = ArrayList<String>()
                arrData.add("Select Item")
                for (i in arrSpinner.indices) {
                    arrData.add(arrSpinner[i].ControlData)
                }

                //holder.spDropDown.setOnItemSelectedListener(this)
                val adpSpinner =
                    ArrayAdapter(mContext, android.R.layout.simple_spinner_item, arrData)
                adpSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                holder.spDropDown.setAdapter(adpSpinner)
                if (updateDataBySectionId!!.isNotEmpty()) {
                    for (i in updateDataBySectionId) {
                        if (i.AnswerValue.toString() != "") {
                            if (i.ControlTypeID == 2) {
                                if (i.QID == item.Qid) {
                                    SetSpinnerText(holder.spDropDown, i.AnswerValue)
                                }
                            }
                        }
                    }
                }

            }


        } else if (controlId == 3) {
            holder.txtQuestion.text = (position + 1).toString() + ". " + item.Qname
            holder.edtHamName.inputType = InputType.TYPE_CLASS_NUMBER
            holder.edtHamName.filters = arrayOf<InputFilter>(filter, LengthFilter(3))
            holder.edtHamName.visibility = View.VISIBLE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.txtCaptureLocation.visibility = View.GONE
            holder.ll_geoLocation.visibility = View.GONE
            if (updateDataBySectionId!!.isNotEmpty()) {
                for (i in updateDataBySectionId) {
                    if (i.answer.toString() != "") {
                        if (i.ControlTypeID == 3) {
                            if (i.QID == item.Qid) {
                                holder.edtHamName.text = i.AnswerValue
                            }
                        }
                    }
                }
            }
        } else if (controlId == 4) {
            holder.recyMultipleChoice.visibility = View.VISIBLE
            holder.edtHamName.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.txtCaptureLocation.visibility = View.GONE
            holder.ll_geoLocation.visibility = View.GONE

            Coroutines.main {
                val answerModelListPage: ArrayList<AnswerModel>? = ArrayList()

                val arr = repository.getControlMasterData(item.Staus, item.Qid, 4)
                if (arr.isNotEmpty()) {
                    for (i in arr) {
                        answerModelListPage?.add(AnswerModel("",
                            i.COID,
                            i.QID,
                            item.SectionID,
                            2))
                    }
                }

                val adpSection =
                    AdapterMultipleChoice(
                        mContext,
                        repository,
                        arr,
                        arrDataQuestionControl,
                        item.SectionID,
                        item.CatID,
                        answerModelListPage,
                        updateDataBySectionId
                    )
                adpSection.setListner(this)
                holder.recyMultipleChoice.adapter = adpSection

            }

        } else if (controlId == 5) {
            holder.recyMultipleChoice.visibility = View.GONE
            holder.edtHamName.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.txtCaptureLocation.visibility = View.GONE
            holder.ll_geoLocation.visibility = View.VISIBLE

        }

        holder.btnGetLatLong.setOnClickListener {
            val gpsTracker = GpsTracker(mContext)

            if (gpsTracker.isGPSTrackingEnabled) {
                val formater = DecimalFormat("#.##")
                val latitude = formater.format(gpsTracker.latitude).toString()
                val longitude = formater.format(gpsTracker.longitude).toString()
                holder.txtLatLong.text = "Lat : $latitude, Long :$longitude"
                AppConstant.dataSaveList!![position].answerData = "$latitude,$longitude"

            } else {
                gpsTracker.showSettingsAlert()
            }
        }

        if (mSpinnerSelectedItem.containsKey(position)) {
            holder.spDropDown.setSelection(mSpinnerSelectedItem[position]!!);
        }
    }

    override fun getItemCount(): Int {
        return listQuestion!!.size
    }

    override fun getItemViewType(position: Int) = position

    override fun getItemId(position: Int) = position.toLong()
    var mSpinnerSelectedItem: HashMap<Int, Int> = HashMap()

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtQuestion: TextView = itemView.findViewById(R.id.txtQuestion)
        var edtHamName: TextView = itemView.findViewById(R.id.edtHamName)
        var spDropDown: Spinner = itemView.findViewById(R.id.spDropDown)
        var recyMultipleChoice: RecyclerView = itemView.findViewById(R.id.recyMultipleChoice)
        var txtCaptureLocation: TextView = itemView.findViewById(R.id.txtCaptureLocation)
        var ll_geoLocation: LinearLayout = itemView.findViewById(R.id.ll_geoLocation)
        var btnGetLatLong: Button = itemView.findViewById(R.id.btnGetLatLong)
        var txtLatLong: TextView = itemView.findViewById(R.id.txtLatLong)

        init {
            spDropDown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener,
                OnFragmentListItemSelectListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    l: Long,
                ) {
                    //String ASIN = gameDataArr.get(position).getAmazonId();
                    if (position == 0) {

                    } else {
                        couts++
                        var checkvalue = true
                        mSpinnerSelectedItem.put(adapterPosition, position);
                        var controlDataMasterJoin: ControlDataMasterJoin? = null
                        for (i in arrDataQuestionControl) {
                            if (listQuestion!![adapterPosition].Qid == i.QID
                            ) {
                                if (spDropDown.getItemAtPosition(position)
                                        .toString() == i.ControlData
                                ) {
                                    controlDataMasterJoin = i
                                }
                            }
                        }

                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (listQuestion!![adapterPosition].Qid == AppConstant.dataSaveList!![i].Qid) {
                                AppConstant.dataSaveList!![i].answerData =
                                    spDropDown.getItemAtPosition(position).toString()
                                AppConstant.dataSaveList!![i].ControlName = "Spinner"

                            }
                        }
                        if (updateDataBySectionId.isNullOrEmpty()) {
                            Coroutines.main {
                                val getDependencyData = repository.getDependencyData(
                                    controlDataMasterJoin!!.QID,
                                    controlDataMasterJoin.CID)
                                val arrayListAnswerFillDatanewq = arrayListAnswerFillData
                                if (getDependencyData.isNotEmpty()) {
                                    var count = 0
                                    var countCompare = 0
                                    for (i in getDependencyData.indices) {
                                        for (j in AppConstant.dataSaveList!!.indices) {
                                            val value1 = AppConstant.dataSaveList!![j]
                                                .Qid.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                                    AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                            val value2 =
                                                getDependencyData[i].Qid.toString() + getDependencyData[i].SectionID.toString() +
                                                        getDependencyData[i].CatID.toString() + AppConstant.patId
                                            if (value1 == value2) {
                                                AppConstant.dataSaveList?.set(j,
                                                    AnswerValueFillNew(getDependencyData[i].CatID,
                                                        getDependencyData[i].ControlID,
                                                        getDependencyData[i].Qid,
                                                        getDependencyData[i].ControlName.toString(),
                                                        getDependencyData[i].SectionID,
                                                        AppConstant.dataSaveList!![j].answerData,
                                                        controlDataMasterJoin.QID.toString(),
                                                        AppConstant.dataSaveList!![j].answerId.toString(),
                                                        "",
                                                        0,
                                                        AppConstant.dataSaveList!![j].answerId,
                                                        AppConstant.dataSaveList!![j].uuid))
                                                checkvalue = false
                                            }
                                            if (j == (AppConstant.dataSaveList!!.size - 1)) {
                                                if (checkvalue) {
                                                    AppConstant.dataSaveList?.add(
                                                        AnswerValueFillNew(
                                                            getDependencyData[i].CatID,
                                                            getDependencyData[i].ControlID,
                                                            getDependencyData[i].Qid,
                                                            getDependencyData[i].ControlName.toString(),
                                                            getDependencyData[i].SectionID,
                                                            "",
                                                            controlDataMasterJoin.QID.toString(),
                                                            controlDataMasterJoin.CID.toString(),
                                                            "",
                                                            0,
                                                            controlDataMasterJoin.CID.toString(),
                                                            UUID.randomUUID().toString()))
                                                }
                                            }

                                        }
                                    }

                                    val dialog = BottomSheetDialog(mContext)
                                    val view1 = LayoutInflater.from(mContext).inflate(
                                        R.layout.bottom_sheet, null)
                                    val recyclerView: RecyclerView =
                                        view1.findViewById(R.id.recyQuestionList)
                                    val layoutManager = LinearLayoutManager(mContext)
                                    recyclerView.layoutManager = layoutManager

                                    val madapter =
                                        ProfileQuestionAdapterBottomSheet(mContext,
                                            repository,
                                            getDependencyData,
                                            arrDataQuestionControl,
                                            controlDataMasterJoin.QID.toString(),
                                            updateDataBySectionId,
                                            controlDataMasterJoin.CID.toString(),
                                            dialog)
                                    recyclerView.adapter = madapter
                                    val btnClose =
                                        view1.findViewById<ImageView>(R.id.cancelQuestion)
                                    btnClose.setOnClickListener {
                                        dialog.dismiss()
                                    }
                                    val buttonBottom = view1.findViewById<Button>(R.id.buttonBottom)
                                    buttonBottom?.setOnClickListener {
                                        var count = 0
                                        for (i in AppConstant.dataSaveList!!) {
                                            for (j in getDependencyData) {
                                                if (i.Qid == j.Qid) {
                                                    count++
                                                    if (i.answerData == "") {
                                                        mContext.toast("Please enter mandatory field")
                                                    } else {
                                                        if (count == getDependencyData.size)
                                                            dialog.dismiss()
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    dialog.setContentView(view1)
                                    dialog.setCancelable(false)
                                    dialog.show()
                                } else {
                                    try {
                                        for (i in AppConstant.dataSaveList!!.indices) {
                                            if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                AppConstant.dataSaveList!!.removeAt(i)

                                            }
                                        }
                                    } catch (e: Exception) {
                                        try {
                                            for (i in AppConstant.dataSaveList!!.indices) {
                                                if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                    AppConstant.dataSaveList!!.removeAt(i)

                                                }
                                            }
                                        } catch (e: Exception) {
                                            try {
                                                for (i in AppConstant.dataSaveList!!.indices) {
                                                    if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                        AppConstant.dataSaveList!!.removeAt(
                                                            i)

                                                    }
                                                }
                                            } catch (e: Exception) {

                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            if (couts > 1) {
                                Coroutines.main {
                                    val getDependencyData = repository.getDependencyData(
                                        controlDataMasterJoin!!.QID,
                                        controlDataMasterJoin.CID)
                                    val arrayListAnswerFillDatanewq = arrayListAnswerFillData
                                    if (getDependencyData.isNotEmpty()) {
                                        var count = 0
                                        var countCompare = 0
                                        for (i in getDependencyData.indices) {
                                            for (j in AppConstant.dataSaveList!!.indices) {
                                                val value1 = AppConstant.dataSaveList!![j]
                                                    .Qid.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                                        AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                                val value2 =
                                                    getDependencyData[i].Qid.toString() + getDependencyData[i].SectionID.toString() +
                                                            getDependencyData[i].CatID.toString() + AppConstant.patId
                                                if (value1 == value2) {
                                                    AppConstant.dataSaveList?.set(j,
                                                        AnswerValueFillNew(getDependencyData[i].CatID,
                                                            getDependencyData[i].ControlID,
                                                            getDependencyData[i].Qid,
                                                            getDependencyData[i].ControlName.toString(),
                                                            getDependencyData[i].SectionID,
                                                            AppConstant.dataSaveList!![j].answerData,
                                                            controlDataMasterJoin.QID.toString(),
                                                            AppConstant.dataSaveList!![j].answerId.toString(),
                                                            "",
                                                            0,
                                                            AppConstant.dataSaveList!![j].answerId,
                                                            AppConstant.dataSaveList!![j].uuid))
                                                    checkvalue = false
                                                }
                                                if (j == (AppConstant.dataSaveList!!.size - 1)) {
                                                    if (checkvalue) {
                                                        AppConstant.dataSaveList?.add(
                                                            AnswerValueFillNew(
                                                                getDependencyData[i].CatID,
                                                                getDependencyData[i].ControlID,
                                                                getDependencyData[i].Qid,
                                                                getDependencyData[i].ControlName.toString(),
                                                                getDependencyData[i].SectionID,
                                                                "",
                                                                controlDataMasterJoin.QID.toString(),
                                                                controlDataMasterJoin.CID.toString(),
                                                                "",
                                                                0,
                                                                controlDataMasterJoin.CID.toString(),
                                                                UUID.randomUUID().toString()))
                                                    }
                                                }

                                            }
                                        }

                                        val dialog = BottomSheetDialog(mContext)
                                        val view1 = LayoutInflater.from(mContext).inflate(
                                            R.layout.bottom_sheet, null)
                                        val recyclerView: RecyclerView =
                                            view1.findViewById(R.id.recyQuestionList)
                                        val layoutManager = LinearLayoutManager(mContext)
                                        recyclerView.layoutManager = layoutManager

                                        val madapter =
                                            ProfileQuestionAdapterBottomSheet(mContext,
                                                repository,
                                                getDependencyData,
                                                arrDataQuestionControl,
                                                controlDataMasterJoin.QID.toString(),
                                                updateDataBySectionId,
                                                controlDataMasterJoin.CID.toString(),
                                                dialog)
                                        recyclerView.adapter = madapter
                                        val btnClose =
                                            view1.findViewById<ImageView>(R.id.cancelQuestion)
                                        btnClose.setOnClickListener {
                                            dialog.dismiss()
                                        }
                                        val buttonBottom =
                                            view1.findViewById<Button>(R.id.buttonBottom)
                                        buttonBottom?.setOnClickListener {
                                            var count = 0
                                            for (i in AppConstant.dataSaveList!!) {
                                                for (j in getDependencyData) {
                                                    if (i.Qid == j.Qid) {
                                                        count++
                                                        if (i.answerData == "") {
                                                            mContext.toast("Please enter mandatory field")
                                                        } else {
                                                            if (count == getDependencyData.size)
                                                                dialog.dismiss()
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        dialog.setContentView(view1)
                                        dialog.setCancelable(false)
                                        dialog.show()
                                    } else {
                                        try {
                                            for (i in AppConstant.dataSaveList!!.indices) {
                                                if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                    AppConstant.dataSaveList!!.removeAt(i)

                                                }
                                            }
                                        } catch (e: Exception) {
                                            try {
                                                for (i in AppConstant.dataSaveList!!.indices) {
                                                    if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                        AppConstant.dataSaveList!!.removeAt(i)

                                                    }
                                                }
                                            } catch (e: Exception) {
                                                try {
                                                    for (i in AppConstant.dataSaveList!!.indices) {
                                                        if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                            AppConstant.dataSaveList!!.removeAt(
                                                                i)

                                                        }
                                                    }
                                                } catch (e: Exception) {

                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                Coroutines.main {
                                    val getDependencyData = repository.getDependencyData(
                                        controlDataMasterJoin!!.QID,
                                        controlDataMasterJoin.CID)
                                    val arrayListAnswerFillDatanewq = arrayListAnswerFillData
                                    if (getDependencyData.isNotEmpty()) {
                                        for (i in getDependencyData.indices) {
                                            for (j in AppConstant.dataSaveList!!.indices) {
                                                val value1 = AppConstant.dataSaveList!![j]
                                                    .Qid.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                                        AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                                val value2 =
                                                    getDependencyData[i].Qid.toString() + getDependencyData[i].SectionID.toString() +
                                                            getDependencyData[i].CatID.toString() + AppConstant.patId
                                                if (value1 == value2) {
                                                    AppConstant.dataSaveList?.set(j,
                                                        AnswerValueFillNew(getDependencyData[i].CatID,
                                                            getDependencyData[i].ControlID,
                                                            getDependencyData[i].Qid,
                                                            getDependencyData[i].ControlName.toString(),
                                                            getDependencyData[i].SectionID,
                                                            AppConstant.dataSaveList!![j].answerData,
                                                            controlDataMasterJoin.QID.toString(),
                                                            AppConstant.dataSaveList!![j].answerId.toString(),
                                                            "",
                                                            0,
                                                            AppConstant.dataSaveList!![j].answerId,
                                                            AppConstant.dataSaveList!![j].uuid))
                                                    checkvalue = false
                                                }

                                            }
                                        }


                                    }

                                }
                            }
                        }


                    }


                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {

                }

                override fun onListItemSelected(itemId: Int, data: Any?) {
                    val dataRec = data as AnswerValueFill
                    var check = true
                    Coroutines.mainIo() {
                        if (arrayListAnswerFillData.isNotEmpty()) {
                            for (i in arrayListAnswerFillData.indices) {
                                val uniqueCode =
                                    (arrayListAnswerFillData[i].Qid.toString() +
                                            arrayListAnswerFillData[i].CatID.toString() + arrayListAnswerFillData[i].SectionID)
                                val uniqueCode1 =
                                    (dataRec.Qid.toString() +
                                            dataRec.CatID.toString() + dataRec.SectionID)
                                if (uniqueCode == uniqueCode1) {
                                    arrayListAnswerFillData.set(i, dataRec)
                                    check = false
                                }
                                if (i == (arrayListAnswerFillData.size - 1)) {
                                    if (check) {
                                        arrayListAnswerFillData.add(dataRec)
                                    }
                                }
                            }
                        } else {
                            arrayListAnswerFillData.add(dataRec)
                        }


                    }

                }
            }
            edtHamName.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int,
                ) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable) {
                    // write here
                    if (s.toString().isNotEmpty()) {
//                        AppConstant.dataSaveList!![adapterPosition].answerData = s.toString()
                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (listQuestion!![adapterPosition].Qid == AppConstant.dataSaveList!![i].Qid) {
                                AppConstant.dataSaveList!![i].answerData =
                                    s.toString()
                                AppConstant.dataSaveList!![i].ControlName = "EditText"
                            }
                        }
                        /*  mListner?.onListItemSelected(position,
                              AnswerValueFill(listQuestion!![adapterPosition].CatID,
                                  listQuestion[adapterPosition].ControlID,
                                  listQuestion[adapterPosition].Qid,
                                  listQuestion[adapterPosition].ControlName.toString(),
                                  listQuestion[adapterPosition].SectionID,
                                  s.toString(),
                                  ""))*/
                    }
                }
            })
        }
    }


    val arrayLista: ArrayList<AnswerModel> = ArrayList()
    var check = false
    override fun onListItemSelected(itemId: Int, data: Any?) {
        if (itemId == 11) {
            val dataRec = data as ArrayList<AnswerValueFill>
            var check = true
            Coroutines.mainIo() {

                for (l in dataRec) {
                    if (arrayListAnswerFillData.isNotEmpty()) {
                        for (i in arrayListAnswerFillData.indices) {

                            val uniqueCode =
                                (arrayListAnswerFillData[i].Qid.toString() +
                                        arrayListAnswerFillData[i].CatID.toString() + arrayListAnswerFillData[i].SectionID)
                            val uniqueCode1 =
                                (l.Qid.toString() +
                                        l.CatID.toString() + l.SectionID)
                            if (uniqueCode == uniqueCode1) {
                                arrayListAnswerFillData.set(i, l)
                                check = false
                            }
                            if (i == (arrayListAnswerFillData.size - 1)) {
                                if (check) {
                                    arrayListAnswerFillData.add(l)
                                }
                            }
                        }
                    } else {
                        arrayListAnswerFillData.add(l)
                    }
                }

            }
        } else {
            val arrayList: ArrayList<String> = ArrayList()
            val datarec = data as ArrayList<AnswerModel>
            if (datarec.isNotEmpty()) {
                for (i in datarec) {
                    if (i.answer != "") {
                        arrayLista.add(i)
                        arrayList.add(i.answer)
                    } else {
                        arrayLista.remove(i)
                    }
                }
            }
            val commaSepratedString = TextUtils.join(", ", arrayList)
            print(commaSepratedString)
            for (i in arrayListAnswerFillData) {
                for (j in datarec.indices) {
                    val unique1 = i.CatID.toString() + i.SectionID + i.Qid + i.ControlID
                    val unique2 =
                        datarec[j].catID.toString() + datarec[j].sectionID + datarec[j].questionID + datarec[j].controlId
                    if (unique1 == unique2) {
                        i.answerData = commaSepratedString
                    }

                }
            }
        }


    }


}