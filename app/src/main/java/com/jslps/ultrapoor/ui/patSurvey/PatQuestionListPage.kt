package com.jslps.ultrapoor.ui.patSurvey

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.db.ImageListModel
import com.jslps.ultrapoor.data.db.ModelForSurveyId
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.network.response.*
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.listner.OnFragmentListItemSelectListener
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.buttonEnabledOrNot
import com.jslps.ultrapoor.util.toast
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.question_list_activity_pat.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PatQuestionListPage : AppCompatActivity(), OnFragmentListItemSelectListener {
    lateinit var tblAnswer: ArrayList<tblAnswer>
    private lateinit var repository: Repository
    lateinit var activityListMaster: ActivityListMaster
    lateinit var uuidRow: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide()
        setContentView(R.layout.question_list_activity_pat)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)
        repository = Repository(api, db, api2)
        val i = intent
        activityListMaster = i.getSerializableExtra("sectionObject") as ActivityListMaster
        uuidRow = (i.getSerializableExtra("uuidRow") as String?).toString()
        initData()
        textView3?.text = activityListMaster.ActivityName
        saveDataPat?.setOnClickListener {
            var flagsave = false
            tblAnswer = ArrayList<tblAnswer>()
            val data = AppConstant.dataSaveList
            print(data)
            val arrayList: ArrayList<tblAnswer> = ArrayList()
            if (data!!.isNotEmpty()) {
                for (j in data) {
                    if (j.answerData == "") {
                        flagsave = false
                        break
                    } else {
                        flagsave = true
                        if (j.Qid == 10) {
                            val tblAnswer = tblAnswer(userprofileData!!.district_ID,
                                userprofileData!!.block_ID,
                                userprofileData!!.cluster_ID.toLong(),
                                AppConstant.villageId.toLong(),
                                AppConstant.HamletId,
                                AppConstant.vocode,
                                AppConstant.patId,
                                activityListMaster.SequNo,
                                j.CatID,
                                j.Qid,
                                j.answerData,
                                j.ControlID,
                                j.answerId,
                                0,
                                0,
                                0,
                                Date(),
                                Prefs.getString("user", ""),
                                UUID.randomUUID().toString(),
                                0,
                                "", "",
                                j.score,j.questionToBEAdd
                            )
                            arrayList.add(tblAnswer)
                        } else {
                            val tblAnswer = tblAnswer(userprofileData!!.district_ID,
                                userprofileData!!.block_ID,
                                userprofileData!!.cluster_ID.toLong(),
                                AppConstant.villageId.toLong(),
                                AppConstant.HamletId,
                                "",
                                AppConstant.patId,
                                activityListMaster.SequNo,
                                j.CatID,
                                j.Qid,
                                j.answerData,
                                j.ControlID,
                                j.answerId,
                                0,
                                0,
                                0,
                                Date(),
                                Prefs.getString("user", ""),
                                j.uuid,
                                0,
                                "", "",
                                j.score,j.questionToBEAdd
                            )
                            arrayList.add(tblAnswer)
                        }
                    }

                }
                Coroutines.main {
                    if (flagsave) {
                        val repositorysave = repository.saveSectionAnswer(arrayList)
                        toast("Data save successfully")
                        val modelForSurveyId =
                            ModelForSurveyId(AppConstant.HamletId, false)
                        val result1 = repository.addModelForSurveyId(modelForSurveyId)
                        finish()
                    } else {
                        toast("कृपया सभी अनिवार्य फ़ील्ड दर्ज करें")
                    }


                }
            }


        }

    }

    var madapter: PatQuestionAdapter? = null
    var arrData: ArrayList<QuestionTypeMaster>? = null
    var arrDataQuestionControl: ArrayList<ControlDataMasterJoin>? = null
    var userprofileData: UserProfileData? = null
    var updateFlag = false
    private var updateDataBySectionId: ArrayList<tblAnswer>? = null
    private fun initData() {
        Coroutines.main {
            //GET USER PROFILE DATA FROM USER PROFILE TABLE
            userprofileData = repository.getUserProfileData()
            if (!TextUtils.isEmpty(uuidRow)) {
                updateDataBySectionId = repository.getSaveDataBySectionId(AppConstant.HamletId,
                    activityListMaster.CatID,
                    activityListMaster.SequNo,
                    AppConstant.patId) as ArrayList<tblAnswer>
            }
            repository.getUserProfileData().let {
                Villegetext.text = AppConstant.villageName
                Hamlettext.text = AppConstant.hamletName

            }

            arrData = repository.getQuestionListPat(activityListMaster.CatID,
                activityListMaster.SequNo.toString()) as ArrayList<QuestionTypeMaster>
            arrDataQuestionControl =
                repository.getControlDataMaster(activityListMaster.CatID.toString(),
                    activityListMaster.SequNo.toString()) as ArrayList<ControlDataMasterJoin>
            val layoutManager = LinearLayoutManager(this)
            recyQuestionList.layoutManager = layoutManager
            val arrayListAnswerFillData: ArrayList<AnswerValueFill> = ArrayList()
            imageBitMapList = ArrayList<ImageListModel>()
            AppConstant.dataSaveList?.clear()
            if (updateDataBySectionId!!.isEmpty()) {
                if (arrData!!.isNotEmpty()) {
                    for (i in arrData!!) {
                        val answerValueFill = AnswerValueFill(
                            i.CatID, i.ControlID, i.Qid, i.ControlName, i.SectionID, "",
                            "")
                        val answerValueFillnew = AnswerValueFillNew(
                            i.CatID, i.ControlID, i.Qid, i.ControlName, i.SectionID, "",
                            "", "", "", 0, "", UUID.randomUUID().toString())
                        val model = ImageListModel(null, null, null)
                        imageBitMapList!!.add(model)
                        arrayListAnswerFillData.add(answerValueFill)
                        AppConstant.dataSaveList?.add(answerValueFillnew)
                    }
                }
            }
            if (!updateDataBySectionId.isNullOrEmpty()) {
                updateFlag = true
                AppConstant.dataSaveList?.clear()
                for (j in updateDataBySectionId!!) {
                    val answerValueFillnew = AnswerValueFillNew(
                        j.CatID, j.ControlTypeID, j.QID, "", j.SectionID.toString(), j.AnswerValue,
                        j.questionToBeAdd.toString(), j.AnswerID.toString(), j.Score, 0, "", j.uuid)
                    AppConstant.dataSaveList?.add(answerValueFillnew)

                }
                for (j in updateDataBySectionId!!) {
                    if (j.isExported == 1) {
                        flagexported = true
                        break
                    } else {
                        flagexported = false
                    }
                }
                if (flagexported) {
                    buttonEnabledOrNot(saveDataPat, false)
                } else {
                    buttonEnabledOrNot(saveDataPat, true)

                }

            }
            Prefs.putString("flagaa", "show")
            madapter =
                PatQuestionAdapter(this,
                    arrData,
                    arrDataQuestionControl!!,
                    arrayListAnswerFillData, repository, updateDataBySectionId, userprofileData!!)
            madapter?.setListner(this)
            recyQuestionList.adapter = madapter
            recyQuestionList.visibility = View.VISIBLE
        }
    }
    var flagexported = false
    var position = 0
    private val answerValueFillList: ArrayList<AnswerValueFill> = ArrayList()
    override fun onListItemSelected(itemId: Int, data: Any?) {
        if (data == null) {
            position = itemId
            captureImage1()
        } else {
            val recyclerModelProduct = data as AnswerValueFill
            var check = true
            answerValueFillList.add(recyclerModelProduct)
        }
    }

    private val IMAGE_DIRECTORY_NAME = "UltraPoorImage"
    private val CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100
    private fun captureImage1() {
        try {
            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                    m.invoke(null)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
        } catch (e: Exception) {
            Toast.makeText(
                applicationContext,
                "error", Toast.LENGTH_SHORT
            ).show()
        }
    }

    var imageBitMapList: ArrayList<ImageListModel>? = null
    var imagename = ""

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?,
    ) { // TODO Auto-generated method stuRsetiTranDatab
        super.onActivityResult(requestCode, resultCode, data)
        try { // if the result is capturing Image
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val photo = data?.extras!!.get("data") as Bitmap
                        //
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        photo.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream)
                        val imagedata = byteArrayOutputStream.toByteArray()
                        val encodedBase64 = Base64.encodeToString(
                            imagedata,
                            Base64.DEFAULT
                        )
                        Log.d("hgdfjg", "ghsdh$imagedata")
                        val sdcard = Environment.getExternalStorageDirectory()
                            .toString() + "/" + IMAGE_DIRECTORY_NAME
                        val mediaStorageDir = File("$sdcard/", "TrainingRestiImage")
                        val temp = File(mediaStorageDir.path)
                        if (!temp.exists()) {
                            temp.mkdirs()
                        }
                        val timeStamp = SimpleDateFormat(
                            "yyyyMMdd_HHmmss",
                            Locale.getDefault()
                        ).format(Date())
                        imagename = "IMG_$timeStamp.jpg"
                        val model = ImageListModel(photo, position, imagename)
                        imageBitMapList!![position] = model

                        madapter?.updateCartItems(imageBitMapList!!)


                    }
                    Activity.RESULT_CANCELED -> { // user cancelled Image capture
                        Toast.makeText(
                            this, "User cancelled image capture",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    else -> { // failed to capture image
                        Toast.makeText(
                            this, "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            }
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                .show()
        }
    }
}