package com.jslps.ultrapoor.ui.addHammlet

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AddHamlettbl
import com.jslps.ultrapoor.data.listner.OnListItem
import com.jslps.ultrapoor.util.buttonEnabledOrNot
import com.jslps.ultrapoor.util.buttonEnabledOrNotEdittext
import com.jslps.ultrapoor.util.buttonEnabledOrNotImage
import com.jslps.ultrapoor.util.toast
import com.pixplicity.easyprefs.library.Prefs
import java.util.*


class HamletListAdapter(
    private val mContext: Context,
    private val mcount: Int,
    private val getHamletList: ArrayList<AddHamlettbl>?,
    public val arrayListAnswerFillData: ArrayList<AddHamlettbl>?,
) : RecyclerView.Adapter<HamletListAdapter.MailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MailViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.addfeild,
            parent, false
        )
        return MailViewHolder(view)
    }

    override fun onBindViewHolder(holder: MailViewHolder, position: Int) {
        holder.delete_button.setOnClickListener {
            try {
                notifyItemRemoved(position)
                arrayListAnswerFillData!!.remove(arrayListAnswerFillData[position])
                onListItem?.onListItemSelected(arrayListAnswerFillData.size, null, "delete")
            } catch (e: Exception) {
                notifyItemRemoved(position)
                arrayListAnswerFillData!!.remove(arrayListAnswerFillData[position - 1])
                onListItem?.onListItemSelected(arrayListAnswerFillData.size, null, "delete")
            }

        }
        if (arrayListAnswerFillData != null && arrayListAnswerFillData.isNotEmpty()) {
            holder.price.setText(arrayListAnswerFillData[position].hamletName)
            if (arrayListAnswerFillData[position].isExported == 1) {
                arrayListAnswerFillData[position].isExported == 1
                buttonEnabledOrNotImage(holder.delete_button, false)
                buttonEnabledOrNotEdittext(holder.price, false)

            } else {
                buttonEnabledOrNotImage(holder.delete_button, true)
                buttonEnabledOrNotEdittext(holder.price, true)
                arrayListAnswerFillData[position].isExported == 0
            }
        } else {
            arrayListAnswerFillData!![position].isExported == 0
        }

    }


    override fun getItemCount(): Int {
        return arrayListAnswerFillData!!.size
    }

    var onListItem: OnListItem? = null
    fun setListner(addHammletPage: OnListItem) {
        onListItem = addHammletPage
    }

    inner class MailViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var delete_button: LinearLayout = itemView.findViewById(R.id.delete_button)
        var price: EditText = itemView.findViewById(R.id.price)

        init {
            price.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {}
                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int,
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int,
                ) {
                    if (s.isNotEmpty() && s.length > 3) {
                        arrayListAnswerFillData!![adapterPosition].hamletName = s.toString()
                        onListItem?.onListItemSelected(mcount, null, "add")
                    }
                }
            })

        }
    }
}