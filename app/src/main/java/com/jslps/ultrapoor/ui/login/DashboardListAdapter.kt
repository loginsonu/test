package com.jslps.ultrapoor.ui.login

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.ui.addHammlet.AddHammletPage
import com.jslps.ultrapoor.ui.hamletList.ListHammletPage
import com.jslps.ultrapoor.util.AppConstant


class DashboardListAdapter(
    private val mContext: Context,
    private val houseHoldList: List<String>,
) : RecyclerView.Adapter<DashboardListAdapter.MailViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MailViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.view_dashboad,
            parent, false
        )
        return MailViewHolder(view)
    }

    override fun onBindViewHolder(holder: MailViewHolder, position: Int) {
        val ItemsViewModel = houseHoldList[position]
        holder.heading.text = ((position + 1).toString()) + "." + ItemsViewModel
        holder.layout.setOnClickListener {
            when {
                ItemsViewModel.trim() == "हेमलेट जोड़ें".trim() -> {
                    if(AppConstant.villageName != "गाँव चुने") {
                        val intent = Intent(mContext, AddHammletPage::class.java)
                        mContext.startActivity(intent)
                    }else{
                        Toast.makeText(mContext, "कृपया गाँव चुने", Toast.LENGTH_SHORT).show()
                    }
                }
                ItemsViewModel.trim() == "हेमलेट की सूची".trim() -> {
                    if(AppConstant.villageName != "गाँव चुने") {
                        val intent = Intent(mContext, ListHammletPage::class.java)
                        mContext.startActivity(intent)
                    }else{
                        Toast.makeText(mContext, "कृपया गाँव चुने", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }


    }


    override fun getItemCount(): Int {
        return houseHoldList.size
    }

    inner class MailViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var heading: TextView = itemView.findViewById(R.id.heading)
        var layout: ConstraintLayout = itemView.findViewById(R.id.layoutaaay)

    }
}