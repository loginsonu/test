package com.jslps.ultrapoor.ui.endorsementprocess.hhsprofile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.network.response.QuestionTypeMaster
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.ui.endorsementprocess.EndorsementProcessModel
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast
import kotlinx.android.synthetic.main.activity_hhs_profile.*

class HHsProfileActivity : AppCompatActivity() {
    private lateinit var repository: Repository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_hhs_profile)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)

        repository = Repository(api, db, api2)
        val i = intent
        val hhsProfile = i.getSerializableExtra("hhsProfile") as EndorsementProcessModel
        val endmes = i.getSerializableExtra("endmes") as tblAnswer
        txtHHsDidiName.text = ": " + hhsProfile.didiName
        txtHHsDadaName.text = ": " + hhsProfile.dadaName
        patscore.text =
            "विभिन्न श्रेनेयो में परिवार द्वारा \nप्राप्त कल अंक (PAT survey) : " + hhsProfile.sector

        Coroutines.main {
            val tblAnswer = repository.getQuestionControl(7, endmes.PatId)
            val tblAnswer1 = repository.getQuestionControl(12, endmes.PatId)
            val tblAnswer2 = repository.getQuestionControl(11, endmes.PatId)
            if (tblAnswer != null) {
                txtHHsSelfGroup.text = tblAnswer.AnswerValue
            } else
                txtHHsSelfGroup.text = "N/A"
            if (tblAnswer1 != null) {
                txtHHsCastName.text = tblAnswer1.AnswerValue
            }else{
                txtHHsCastName.text = tblAnswer2.AnswerValue
            }
            val getAnswerDataBByScore =
                repository.getAnswerListForScore() as ArrayList<QuestionTypeMaster>
            if (getAnswerDataBByScore.isNotEmpty()) {
                val questionData = repository.getQuestionListHH(37)
                getAnswerDataBByScore.add(questionData)
                val adpHHsProfile =
                    HHsProfileAdapter(this, getAnswerDataBByScore, repository, endmes)
                recyHHsProfile.adapter = adpHHsProfile
            } else {
                toast("No data found")
            }


        }

    }
}