package com.jslps.ultrapoor.ui.patSurvey

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.ImageListModel
import com.jslps.ultrapoor.data.db.ModelForSurveyId
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.response.*
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.listner.OnFragmentListItemSelectListener
import com.jslps.ultrapoor.util.*
import com.pixplicity.easyprefs.library.Prefs
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class PatQuestionAdapter(
    private val mContext: Context,
    private val listQuestion: List<QuestionTypeMaster>?,
    private var arrDataQuestionControl: List<ControlDataMasterJoin>,
    public val arrayListAnswerFillData: ArrayList<AnswerValueFill>?,
    private val repository: Repository,
    private val updateDataBySectionId: ArrayList<tblAnswer>?,
    private val userprofileData: UserProfileData,
) : RecyclerView.Adapter<PatQuestionAdapter.MainViewHolder>() {
    var mSpinnerSelectedItem: HashMap<Int, Int> = HashMap()
    private var imageBitMapList: ArrayList<ImageListModel> = ArrayList()
    var couts = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_questions_pat,
            parent, false)
        return MainViewHolder(view)
    }

    var mListner: OnFragmentListItemSelectListener? = null

    fun setListner(listner: OnFragmentListItemSelectListener?) {
        this.mListner = listner
    }

    val arrayListControlNameMaster: ArrayList<ControlDataMasterJoin>? = null
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        val item = listQuestion!![position]
        holder.txtQuestion.text = (position + 1).toString() + ". " + item.Qname
        val type = item.ControlID
        val controlNameRec = item.ControlName
        //1 text
        //2 dropdown
        //3 edittext
        //4 multiselect
        if (controlNameRec == "EditText") {
            holder.edtHamNameText.visibility = View.VISIBLE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            holder.txtanswer.visibility = View.GONE
            if (AppConstant.dataSaveList!!.isNotEmpty())
                for (i in AppConstant.dataSaveList!!) {
                    if (i.answerData != "") {
                        if (i.ControlID == type) {
                            if (i.Qid == item.Qid) {
                                holder.edtHamNameText.text = i.answerData
                            }
                        }
                    }
                }
        }
        else if (controlNameRec == "Spinner") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.VISIBLE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            holder.txtanswer.visibility = View.GONE
            val arrayListControlNameMaster: ArrayList<ControlDataMasterJoin> = ArrayList()
            if (arrDataQuestionControl.isNotEmpty()) {
                for (i in arrDataQuestionControl) {
                    if (item.Qid == i.QID) {
                        arrayListControlNameMaster.add(i)
                    }
                }
                if (arrayListControlNameMaster.isNotEmpty()) {
                    arrayListControlNameMaster.add(0,
                        ControlDataMasterJoin(0, 0, "Select Item", "", "", 0, 0, 0, "", 0))

                    val adapter = ArrayAdapter(mContext,
                        android.R.layout.simple_expandable_list_item_1, arrayListControlNameMaster)
                    holder.spDropDown.adapter = adapter
                }
            }
            if (AppConstant.dataSaveList!!.isNotEmpty())
                for (i in AppConstant.dataSaveList!!) {
                    if (i.answerData != "") {
                        if (i.ControlID == type) {
                            if (i.Qid == item.Qid) {
                                Prefs.putString("flagaa", "NotShow")
                                SetSpinnerText(holder.spDropDown, i.answerData)
                            }
                        }
                    }
                }

        }
        else if (controlNameRec == "MultiSelect") {
            holder.recyMultipleChoice.visibility = View.VISIBLE
            holder.edtHamNameText.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            holder.txtanswer.visibility = View.GONE

            arrDataQuestionControl = ArrayList()
            if (arrDataQuestionControl.isNotEmpty()) {
                for (i in arrDataQuestionControl) {
                    if (item.Qid == i.QID) {
                        arrayListControlNameMaster?.add(i)
                    }
                }
                if (arrayListControlNameMaster!!.isNotEmpty()) {
                    val adpSection =
                        PatAdapterMultipleChoice(mContext, arrayListControlNameMaster)
                    holder.recyMultipleChoice.adapter = adpSection
                }
            }
        }
        else if (controlNameRec == "EditTextNumeric") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.VISIBLE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            holder.txtanswer.visibility = View.GONE
            if (AppConstant.dataSaveList!!.isNotEmpty())
                for (i in AppConstant.dataSaveList!!) {
                    if (i.answerData != "") {
                        if (i.ControlID == type) {
                            if (i.Qid == item.Qid) {
                                holder.edtHamNameNumric.text = i.answerData
                            }
                        }
                    }
                }
        }
        else if (controlNameRec == "XY") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.VISIBLE
            holder.photoLayout.visibility = View.GONE
            holder.txtanswer.visibility = View.GONE
            if (AppConstant.dataSaveList!!.isNotEmpty())
                for (i in AppConstant.dataSaveList!!) {
                    if (i.answerData != "") {
                        if (i.ControlID == type) {
                            if (i.Qid == item.Qid) {
                                holder.latlongvalue.text = i.answerData
                            }
                        }
                    }
                }
        }
        else if (controlNameRec == "Image") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.VISIBLE
            holder.txtanswer.visibility = View.GONE

        }
        else if (controlNameRec == "Textview") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            holder.txtanswer.visibility = View.VISIBLE

        }
        holder.latlongbutton.setOnClickListener {
            // check if GPS enabled
            val gpsTracker = GpsTracker(mContext)
            if (gpsTracker.isGPSTrackingEnabled) {
                val formater = DecimalFormat("#.##")
                stringLatitude = formater.format(gpsTracker.latitude).toString()
                stringLongituide = formater.format(gpsTracker.longitude).toString()
                holder.latlongvalue.text = "Lat:$stringLatitude,Long:$stringLongituide"
                arrayListAnswerFillData!![position].answerData = "$stringLatitude,$stringLongituide"
                for (i in AppConstant.dataSaveList!!.indices) {
                    if (listQuestion[position].Qid == AppConstant.dataSaveList!![i].Qid) {
                        AppConstant.dataSaveList!![i].answerData = "$stringLatitude,$stringLongituide"
                        AppConstant.dataSaveList!![i].ControlName = "XY"

                    }
                }

            } else
                gpsTracker.showSettingsAlert()
        }
        holder.photoinputmain.setOnClickListener {
            mListner?.onListItemSelected(position, null)
        }
        if (imageBitMapList != null && imageBitMapList.isNotEmpty()) {
            val dataImage = imageBitMapList[position]
            if (dataImage.imageBitmap != null) {
                holder.photoName.visibility = View.VISIBLE
                holder.display_img.setImageBitmap(dataImage.imageBitmap)
                holder.photoName.text = dataImage.imageName
                arrayListAnswerFillData!![position].answerData = dataImage.imageName.toString()
            } else {
                holder.display_img.visibility = View.INVISIBLE
                holder.photoName.visibility = View.INVISIBLE
            }
        }
        if (mSpinnerSelectedItem.containsKey(position)) {
            holder.spDropDown.setSelection(mSpinnerSelectedItem[position]!!)
        }
        if (dataRex != 0) {
            if (item.Qid == 30) {
                holder.txtanswer.text = dataRex.toString()
                showpopupsecond = false
                AppConstant.dataSaveList!![position].answerData = dataRex.toString()
                if (dataRex >= 10000) {
                    AppConstant.dataSaveList!![position].score = "0"
                } else {
                    AppConstant.dataSaveList!![position].score = "1"

                }

            }

        }
        else{
            if (item.Qid == 30) {
                holder.txtanswer.text = dataRex.toString()
                showpopupsecond = false
                AppConstant.dataSaveList!![position].answerData = dataRex.toString()
                if (dataRex >= 10000) {
                    AppConstant.dataSaveList!![position].score = "0"
                } else {
                    AppConstant.dataSaveList!![position].score = "1"

                }

            }
        }
        if (dataRex1 != 0.0) {
            if (item.Qid == 37) {
                holder.txtanswer.text = dataRex1.toString()
                showpopupsecond = false
                AppConstant.dataSaveList!![position].answerData = dataRex1.toString()
                if (dataRex >= 0.5) {
                    AppConstant.dataSaveList!![position].score = "0"
                } else {
                    AppConstant.dataSaveList!![position].score = "1"

                }

            }

        }

    }

    fun updateCartItems(cartItems: ArrayList<ImageListModel>?) {
        if (cartItems != null) {
            imageBitMapList = cartItems
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listQuestion!!.size
    }

    var stringLatitude = ""
    var stringLongituide = ""

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtQuestion: TextView = itemView.findViewById(R.id.txtQuestion)
        var edtHamNameText: TextView = itemView.findViewById(R.id.edtHamNametext)
        var edtHamNameNumric: TextView = itemView.findViewById(R.id.edtHamNameNumric)
        var txtanswer: TextView = itemView.findViewById(R.id.txtanswer)
        var recyMultipleChoice: RecyclerView = itemView.findViewById(R.id.recyMultipleChoice)
        var spDropDown: Spinner = itemView.findViewById(R.id.spDropDown)
        var latlongbutton: AppCompatButton = itemView.findViewById(R.id.latlongbutton)
        var latlongvalue: TextView = itemView.findViewById(R.id.latlongvalue)
        var latlonglayout: LinearLayout = itemView.findViewById(R.id.latlonglayout)
        var photoLayout: LinearLayout = itemView.findViewById(R.id.photoLayout)
        var photoinputmain: TextView = itemView.findViewById(R.id.photoinputmain)
        var photoName: TextView = itemView.findViewById(R.id.photoName)
        var display_img: ImageView = itemView.findViewById(R.id.display_img)
        var selectedControlData: ControlDataMasterJoin? = null

        init {
            spDropDown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    l: Long,
                ) {
                    //String ASIN = gameDataArr.get(position).getAmazonId();
                    if (position == 0) {

                    } else {

                        couts++
                        selectedControlData =
                            parent.getItemAtPosition(position) as ControlDataMasterJoin
                        mSpinnerSelectedItem.put(adapterPosition, position);
                        var controlDataMasterJoin: ControlDataMasterJoin? = null
                        for (i in arrDataQuestionControl) {
                            if (listQuestion!![adapterPosition].Qid == i.QID
                            ) {
                                if (spDropDown.getItemAtPosition(position)
                                        .toString() == i.ControlData
                                ) {
                                    controlDataMasterJoin = i
                                    if (listQuestion[adapterPosition].Qid == 19) {
                                        if (spDropDown.getItemAtPosition(position)
                                                .toString() != "हां"
                                        ) {
                                            Prefs.putString("flagaa", "show")
                                        }
                                    }
                                }
                            }
                        }
                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (listQuestion!![adapterPosition].Qid == AppConstant.dataSaveList!![i].Qid) {
                                AppConstant.dataSaveList!![i].answerData =
                                    spDropDown.getItemAtPosition(position).toString()
                                AppConstant.dataSaveList!![i].ControlName = "Spinner"
                                AppConstant.dataSaveList!![i].answerId = ""

                            }
                        }
                        if (!TextUtils.isEmpty(selectedControlData!!.Score)) {
                            AppConstant.dataSaveList!![adapterPosition].score =
                                selectedControlData!!.Score.toString()
                        }
                        if (listQuestion!![adapterPosition].Qid == 13) {
                            if (selectedControlData!!.CID == 167) {
                                showDialog(mContext as Activity, repository, userprofileData)
                            }
                        }
                        if (listQuestion[adapterPosition].Qid == 94) {
                            if (selectedControlData!!.CID == 209) {
                                showDialog(mContext as Activity, repository, userprofileData)

                            }
                        }
                        if (listQuestion[adapterPosition].Qid == 95) {
                            if (selectedControlData!!.CID == 211) {
                                showDialog(mContext as Activity, repository, userprofileData)

                            }
                        }
                        if (listQuestion[adapterPosition].Qid == 96) {
                            if (selectedControlData!!.CID == 213) {
                                showDialog(mContext as Activity, repository, userprofileData)

                            }
                        }
                        if (listQuestion[adapterPosition].Qid == 97) {
                            if (selectedControlData!!.CID == 215) {
                                showDialog(mContext as Activity, repository, userprofileData)

                            }
                        }
                        if (listQuestion[adapterPosition].Qid == 98) {
                            if (selectedControlData!!.CID == 218) {
                                showDialog(mContext as Activity, repository, userprofileData)

                            }
                        }
                        if (listQuestion[adapterPosition].Qid == 6) {
                            if (selectedControlData!!.CID == 148) {
                                showDialog(mContext as Activity, repository, userprofileData)
                            }
                        }
                        var checkvalue = true
                        if (updateDataBySectionId!!.isNotEmpty()) {
                            if (listQuestion[adapterPosition].SectionID == "2") {
                                if (couts > 2) {
                                    Coroutines.main {
                                        val getDependencyData = repository.getDependencyData(
                                            controlDataMasterJoin!!.QID,
                                            controlDataMasterJoin.CID)
                                        val arrayListAnswerFillDatanewq = arrayListAnswerFillData
                                        if (getDependencyData.isNotEmpty()) {
                                            for (i in getDependencyData.indices) {
                                                for (j in AppConstant.dataSaveList!!.indices) {
                                                    val value1 = AppConstant.dataSaveList!![j]
                                                        .Qid.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                                            AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                                    val value2 =
                                                        getDependencyData[i].Qid.toString() + getDependencyData[i].SectionID.toString() +
                                                                getDependencyData[i].CatID.toString() + AppConstant.patId
                                                    if (value1 == value2) {
                                                        AppConstant.dataSaveList?.set(j,
                                                            AnswerValueFillNew(getDependencyData[i].CatID,
                                                                getDependencyData[i].ControlID,
                                                                getDependencyData[i].Qid,
                                                                getDependencyData[i].ControlName.toString(),
                                                                getDependencyData[i].SectionID,
                                                                AppConstant.dataSaveList!![j].answerData,
                                                                controlDataMasterJoin.QID.toString(),
                                                                controlDataMasterJoin.CID.toString(),
                                                                "",
                                                                0,
                                                                "",
                                                                AppConstant.dataSaveList!![j].uuid))
                                                        checkvalue = false
                                                    }
                                                    if (j == (AppConstant.dataSaveList!!.size - 1)) {
                                                        if (checkvalue) {
                                                            AppConstant.dataSaveList?.add(
                                                                AnswerValueFillNew(
                                                                    getDependencyData[i].CatID,
                                                                    getDependencyData[i].ControlID,
                                                                    getDependencyData[i].Qid,
                                                                    getDependencyData[i].ControlName.toString(),
                                                                    getDependencyData[i].SectionID,
                                                                    "",
                                                                    controlDataMasterJoin.QID.toString(),
                                                                    controlDataMasterJoin.CID.toString(),
                                                                    "",
                                                                    0,
                                                                    "",
                                                                    UUID.randomUUID().toString()))
                                                        }
                                                    }

                                                }
                                            }
                                            val dialog = BottomSheetDialog(mContext)
                                            val view1 = LayoutInflater.from(mContext).inflate(
                                                R.layout.bottom_sheet, null)
                                            val recyclerView: RecyclerView =
                                                view1.findViewById(R.id.recyQuestionList)
                                            val layoutManager = LinearLayoutManager(mContext)
                                            recyclerView.layoutManager = layoutManager

                                            val madapter =
                                                PatQuestionAdapterBottomSheet(mContext,
                                                    getDependencyData,
                                                    arrDataQuestionControl,
                                                    controlDataMasterJoin.QID.toString(),
                                                    updateDataBySectionId,
                                                    repository,
                                                    controlDataMasterJoin.CID.toString(), dialog)
                                            recyclerView.adapter = madapter
                                            val btnClose =
                                                view1.findViewById<ImageView>(R.id.cancelQuestion)
                                            val buttonBottom =
                                                view1.findViewById<Button>(R.id.buttonBottom)
                                            buttonBottom?.setOnClickListener {
                                                var count = 0
                                                for (i in AppConstant.dataSaveList!!) {
                                                    for (j in getDependencyData) {
                                                        if (i.Qid == j.Qid) {
                                                            count++
                                                            if (i.answerData == "") {
                                                                mContext.toast("Please enter mandatory field")
                                                            } else {
                                                                if (count == getDependencyData.size)
                                                                    dialog.dismiss()
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            btnClose.setOnClickListener {
                                                dialog.dismiss()
                                            }
                                            dialog.setContentView(view1)
                                            dialog.setCancelable(false)
                                            dialog.show()
                                        } else {
                                            try {
                                                for (i in AppConstant.dataSaveList!!.indices) {
                                                    if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                        AppConstant.dataSaveList!!.removeAt(i)

                                                    }
                                                }
                                            } catch (e: Exception) {
                                                try {
                                                    for (i in AppConstant.dataSaveList!!.indices) {
                                                        if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                            AppConstant.dataSaveList!!.removeAt(i)

                                                        }
                                                    }
                                                } catch (e: Exception) {

                                                }
                                            }


                                        }

                                    }
                                }
                            } else {
                                if (listQuestion[adapterPosition].SectionID == "4") {
                                    if (Prefs.getString("flagaa", "") == "show") {
                                        Coroutines.main {
                                            val getDependencyData = repository.getDependencyData(
                                                controlDataMasterJoin!!.QID,
                                                controlDataMasterJoin.CID)
                                            val arrayListAnswerFillDatanewq =
                                                arrayListAnswerFillData
                                            if (getDependencyData.isNotEmpty()) {
                                                for (i in getDependencyData.indices) {
                                                    for (j in AppConstant.dataSaveList!!.indices) {
                                                        val value1 = AppConstant.dataSaveList!![j]
                                                            .Qid.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                                                AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                                        val value2 =
                                                            getDependencyData[i].Qid.toString() + getDependencyData[i].SectionID.toString() +
                                                                    getDependencyData[i].CatID.toString() + AppConstant.patId
                                                        if (value1 == value2) {
                                                            AppConstant.dataSaveList?.set(j,
                                                                AnswerValueFillNew(getDependencyData[i].CatID,
                                                                    getDependencyData[i].ControlID,
                                                                    getDependencyData[i].Qid,
                                                                    getDependencyData[i].ControlName.toString(),
                                                                    getDependencyData[i].SectionID,
                                                                    AppConstant.dataSaveList!![j].answerData,
                                                                    controlDataMasterJoin.QID.toString(),
                                                                    controlDataMasterJoin.CID.toString(),
                                                                    "",
                                                                    0,
                                                                    "",
                                                                    AppConstant.dataSaveList!![j].uuid))
                                                            checkvalue = false
                                                        }
                                                        if (j == (AppConstant.dataSaveList!!.size - 1)) {
                                                            if (checkvalue) {
                                                                AppConstant.dataSaveList?.add(
                                                                    AnswerValueFillNew(
                                                                        getDependencyData[i].CatID,
                                                                        getDependencyData[i].ControlID,
                                                                        getDependencyData[i].Qid,
                                                                        getDependencyData[i].ControlName.toString(),
                                                                        getDependencyData[i].SectionID,
                                                                        "",
                                                                        controlDataMasterJoin.QID.toString(),
                                                                        controlDataMasterJoin.CID.toString(),
                                                                        "",
                                                                        0,
                                                                        "",
                                                                        UUID.randomUUID()
                                                                            .toString()))
                                                            }
                                                        }

                                                    }
                                                }

                                                val dialog = BottomSheetDialog(mContext)
                                                val view1 = LayoutInflater.from(mContext).inflate(
                                                    R.layout.bottom_sheet, null)
                                                val recyclerView: RecyclerView =
                                                    view1.findViewById(R.id.recyQuestionList)
                                                val layoutManager = LinearLayoutManager(mContext)
                                                recyclerView.layoutManager = layoutManager

                                                val madapter =
                                                    PatQuestionAdapterBottomSheet(mContext,
                                                        getDependencyData,
                                                        arrDataQuestionControl,
                                                        controlDataMasterJoin.QID.toString(),
                                                        updateDataBySectionId,
                                                        repository,
                                                        controlDataMasterJoin.CID.toString(),
                                                        dialog)
                                                recyclerView.adapter = madapter
                                                val btnClose =
                                                    view1.findViewById<ImageView>(R.id.cancelQuestion)
                                                val buttonBottom =
                                                    view1.findViewById<Button>(R.id.buttonBottom)
                                                buttonBottom?.setOnClickListener {
                                                    var count = 0
                                                    for (i in AppConstant.dataSaveList!!) {
                                                        for (j in getDependencyData) {
                                                            if (i.Qid == j.Qid) {
                                                                count++
                                                                if (i.answerData == "") {
                                                                    mContext.toast("Please enter mandatory field")
                                                                } else {
                                                                    if (count == getDependencyData.size)
                                                                        dialog.dismiss()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                btnClose.setOnClickListener {
                                                    dialog.dismiss()
                                                }
                                                dialog.setContentView(view1)
                                                dialog.setCancelable(false)
                                                dialog.show()
                                            } else {
                                                try {
                                                    for (i in AppConstant.dataSaveList!!.indices) {
                                                        if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                            AppConstant.dataSaveList!!.removeAt(i)

                                                        }
                                                    }
                                                } catch (e: Exception) {
                                                    try {
                                                        for (i in AppConstant.dataSaveList!!.indices) {
                                                            if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                                AppConstant.dataSaveList!!.removeAt(
                                                                    i)

                                                            }
                                                        }
                                                    } catch (e: Exception) {

                                                    }
                                                }


                                            }

                                        }
                                    }
                                } else {
                                    Coroutines.main {
                                        val getDependencyData = repository.getDependencyData(
                                            controlDataMasterJoin!!.QID,
                                            controlDataMasterJoin.CID)
                                        val arrayListAnswerFillDatanewq = arrayListAnswerFillData
                                        if (getDependencyData.isNotEmpty()) {
                                            for (i in getDependencyData.indices) {
                                                for (j in AppConstant.dataSaveList!!.indices) {
                                                    val value1 = AppConstant.dataSaveList!![j]
                                                        .Qid.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                                            AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                                    val value2 =
                                                        getDependencyData[i].Qid.toString() + getDependencyData[i].SectionID.toString() +
                                                                getDependencyData[i].CatID.toString() + AppConstant.patId
                                                    if (value1 == value2) {
                                                        AppConstant.dataSaveList?.set(j,
                                                            AnswerValueFillNew(getDependencyData[i].CatID,
                                                                getDependencyData[i].ControlID,
                                                                getDependencyData[i].Qid,
                                                                getDependencyData[i].ControlName.toString(),
                                                                getDependencyData[i].SectionID,
                                                                AppConstant.dataSaveList!![j].answerData,
                                                                controlDataMasterJoin.QID.toString(),
                                                                controlDataMasterJoin.CID.toString(),
                                                                "",
                                                                0,
                                                                "",
                                                                AppConstant.dataSaveList!![j].uuid))
                                                        checkvalue = false
                                                    }
                                                    if (j == (AppConstant.dataSaveList!!.size - 1)) {
                                                        if (checkvalue) {
                                                            AppConstant.dataSaveList?.add(
                                                                AnswerValueFillNew(
                                                                    getDependencyData[i].CatID,
                                                                    getDependencyData[i].ControlID,
                                                                    getDependencyData[i].Qid,
                                                                    getDependencyData[i].ControlName.toString(),
                                                                    getDependencyData[i].SectionID,
                                                                    "",
                                                                    controlDataMasterJoin.QID.toString(),
                                                                    controlDataMasterJoin.CID.toString(),
                                                                    "",
                                                                    0,
                                                                    "",
                                                                    UUID.randomUUID().toString()))
                                                        }
                                                    }

                                                }
                                            }

                                            val dialog = BottomSheetDialog(mContext)
                                            val view1 = LayoutInflater.from(mContext).inflate(
                                                R.layout.bottom_sheet, null)
                                            val recyclerView: RecyclerView =
                                                view1.findViewById(R.id.recyQuestionList)
                                            val layoutManager = LinearLayoutManager(mContext)
                                            recyclerView.layoutManager = layoutManager

                                            val madapter =
                                                PatQuestionAdapterBottomSheet(mContext,
                                                    getDependencyData,
                                                    arrDataQuestionControl,
                                                    controlDataMasterJoin.QID.toString(),
                                                    updateDataBySectionId,
                                                    repository,
                                                    controlDataMasterJoin.CID.toString(),
                                                    dialog)
                                            recyclerView.adapter = madapter
                                            val btnClose =
                                                view1.findViewById<ImageView>(R.id.cancelQuestion)
                                            val buttonBottom =
                                                view1.findViewById<Button>(R.id.buttonBottom)
                                            buttonBottom?.setOnClickListener {
                                                var count = 0
                                                for (i in AppConstant.dataSaveList!!) {
                                                    for (j in getDependencyData) {
                                                        if (i.Qid == j.Qid) {
                                                            count++
                                                            if (i.answerData == "") {
                                                                mContext.toast("Please enter mandatory field")
                                                            } else {
                                                                if (count == getDependencyData.size)
                                                                    dialog.dismiss()
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            btnClose.setOnClickListener {
                                                dialog.dismiss()
                                            }
                                            dialog.setContentView(view1)
                                            dialog.setCancelable(false)
                                            dialog.show()
                                        } else {
                                            try {
                                                for (i in AppConstant.dataSaveList!!.indices) {
                                                    if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                        AppConstant.dataSaveList!!.removeAt(i)

                                                    }
                                                }
                                            } catch (e: Exception) {
                                                try {
                                                    for (i in AppConstant.dataSaveList!!.indices) {
                                                        if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                            AppConstant.dataSaveList!!.removeAt(i)

                                                        }
                                                    }
                                                } catch (e: Exception) {

                                                }
                                            }


                                        }

                                    }
                                }
                            }
                        } else {
                            if (!showpopupsecond) {
                                if (Prefs.getString("flagaa", "") == "show") {
                                    Coroutines.main {
                                        val getDependencyData = repository.getDependencyData(
                                            controlDataMasterJoin!!.QID,
                                            controlDataMasterJoin.CID)
                                        if (getDependencyData.isNotEmpty()) {
                                            for (i in getDependencyData.indices) {
                                                for (j in AppConstant.dataSaveList!!.indices) {
                                                    val value1 = AppConstant.dataSaveList!![j]
                                                        .Qid.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                                            AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                                    val value2 =
                                                        getDependencyData[i].Qid.toString() + getDependencyData[i].SectionID.toString() +
                                                                getDependencyData[i].CatID.toString() + AppConstant.patId
                                                    if (value1 == value2) {
                                                        AppConstant.dataSaveList?.set(j,
                                                            AnswerValueFillNew(getDependencyData[i].CatID,
                                                                getDependencyData[i].ControlID,
                                                                getDependencyData[i].Qid,
                                                                getDependencyData[i].ControlName.toString(),
                                                                getDependencyData[i].SectionID,
                                                                AppConstant.dataSaveList!![j].answerData,
                                                                controlDataMasterJoin.QID.toString(),
                                                                controlDataMasterJoin.CID.toString(),
                                                                "",
                                                                0,
                                                                "",
                                                                AppConstant.dataSaveList!![j].uuid))
                                                        checkvalue = false
                                                    }
                                                    if (j == (AppConstant.dataSaveList!!.size - 1)) {
                                                        if (checkvalue) {
                                                            AppConstant.dataSaveList?.add(
                                                                AnswerValueFillNew(
                                                                    getDependencyData[i].CatID,
                                                                    getDependencyData[i].ControlID,
                                                                    getDependencyData[i].Qid,
                                                                    getDependencyData[i].ControlName.toString(),
                                                                    getDependencyData[i].SectionID,
                                                                    "",
                                                                    controlDataMasterJoin.QID.toString(),
                                                                    controlDataMasterJoin.CID.toString(),
                                                                    "",
                                                                    0,
                                                                    "",
                                                                    UUID.randomUUID().toString()))
                                                        }
                                                    }

                                                }
                                            }

                                            val dialog = BottomSheetDialog(mContext)
                                            val view1 = LayoutInflater.from(mContext).inflate(
                                                R.layout.bottom_sheet, null)
                                            val recyclerView: RecyclerView =
                                                view1.findViewById(R.id.recyQuestionList)
                                            val layoutManager = LinearLayoutManager(mContext)
                                            recyclerView.layoutManager = layoutManager

                                            val madapter =
                                                PatQuestionAdapterBottomSheet(mContext,
                                                    getDependencyData,
                                                    arrDataQuestionControl,
                                                    controlDataMasterJoin.QID.toString(),
                                                    updateDataBySectionId,
                                                    repository,
                                                    controlDataMasterJoin.CID.toString(),
                                                    dialog)
                                            recyclerView.adapter = madapter
                                            val btnClose =
                                                view1.findViewById<ImageView>(R.id.cancelQuestion)
                                            val buttonBottom =
                                                view1.findViewById<Button>(R.id.buttonBottom)
                                            buttonBottom?.setOnClickListener {
                                                var count = 0
                                                for (i in AppConstant.dataSaveList!!) {
                                                    for (j in getDependencyData) {
                                                        if (i.Qid == j.Qid) {
                                                            count++
                                                            if (i.answerData == "") {
                                                                mContext.toast("Please enter mandatory field")
                                                            } else {
                                                                if (count == getDependencyData.size)
                                                                    dialog.dismiss()
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            btnClose.setOnClickListener {
                                                dialog.dismiss()
                                            }
                                            dialog.setContentView(view1)
                                            dialog.setCancelable(false)
                                            dialog.show()
                                        }
                                        else {
                                            try {
                                                for (i in AppConstant.dataSaveList!!.indices) {
                                                    if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                        AppConstant.dataSaveList!!.removeAt(i)

                                                    }
                                                }
                                            }
                                            catch (e: Exception) {
                                                try {
                                                    for (i in AppConstant.dataSaveList!!.indices) {
                                                        if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                            AppConstant.dataSaveList!!.removeAt(i)

                                                        }
                                                    }
                                                } catch (e: Exception) {
                                                    try {
                                                        for (i in AppConstant.dataSaveList!!.indices) {
                                                            if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                                AppConstant.dataSaveList!!.removeAt(
                                                                    i)

                                                            }
                                                        }
                                                    } catch (e: Exception) {

                                                    }
                                                }
                                            }


                                        }

                                    }
                                }
                            }
                        }
                    }


                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {

                }

            }
            edtHamNameText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int,
                ) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable) {
                    // write here
                    if (s.toString().isNotEmpty()) {
                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (listQuestion!![adapterPosition].Qid == AppConstant.dataSaveList!![i].Qid) {
                                AppConstant.dataSaveList!![i].answerData = s.toString()
                                AppConstant.dataSaveList!![i].ControlName = "EditText"
                                AppConstant.dataSaveList!![i].answerId = ""
                            }
                        }


                    }
                }
            })
            edtHamNameNumric.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int,
                ) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable) {
                    // write here
                    if (s.toString().isNotEmpty()) {
                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (listQuestion!![adapterPosition].Qid == AppConstant.dataSaveList!![i].Qid) {
                                AppConstant.dataSaveList!![i].answerData =
                                    s.toString()
                                AppConstant.dataSaveList!![i].ControlName = "EditTextNumeric"
                                AppConstant.dataSaveList!![i].answerId = ""
                            }
                        }
                        var data = 0
                        var data1 = 0.0
                        var data2 = 0.0
                        val question = listQuestion!![adapterPosition].Qid
                        if (listQuestion[adapterPosition].Qid == 29) {
                            for (i in AppConstant.dataSaveList!!) {
                                when (i.Qid) {
                                    20 -> {
                                        data += if (i.answerData == "")
                                            0
                                        else
                                            (i.answerData.toInt() * 3500)
                                    }
                                    21 -> {
                                        data += if (i.answerData == "")
                                            0
                                        else
                                            (i.answerData.toInt() * 3000)
                                    }
                                    22 -> {
                                        data += if (i.answerData == "")
                                            0
                                        else
                                            (i.answerData.toInt() * 6000)
                                    }
                                    23 -> {
                                        data += if (i.answerData == "")
                                            0
                                        else
                                            (i.answerData.toInt() * 100)
                                    }
                                    26 -> {
                                        data += if (i.answerData == "")
                                            0
                                        else
                                            (i.answerData.toInt() * 1000)
                                    }
                                    29 -> {
                                        data += if (i.answerData == "")
                                            0
                                        else
                                            (i.answerData.toInt() * 5000)
                                    }
                                    else -> {
                                        data += 0
                                    }

                                }

                            }
                            if (data != 0) {
                                try {
                                    txtanswer.text = data.toString()
                                    showpopupsecond = true
                                    updateTextView(data)
                                } catch (e: Exception) {

                                }

//                                updateTextView(data)
                            }else{
                                txtanswer.text = "0"
                                showpopupsecond = true
                                updateTextView(0)
                            }

                        }


                        if (question == 36) {
                            for (i in AppConstant.dataSaveList!!) {
                                when (i.Qid) {
                                    35 -> {
                                        data1 = if (i.answerData == "")
                                            0.0
                                        else
                                            i.answerData.toDouble()
                                    }
                                    36 -> {
                                        data2 = if (i.answerData == "")
                                            0.0
                                        else
                                            i.answerData.toDouble()
                                    }

                                }

                            }
                            if (data1 != 0.0 && data2 != 0.0) {
                                try {
                                    val data3 = (data2 / data1)
                                    if (data3 > data1) {
                                        mContext.toast("response cannot be greater than the response for \"\"परिवार में कुल सदस्य कितने है\"\".")
                                    } else {
                                        showpopupsecond = true
                                        updateTextViewRatio(data3)
                                    }
                                } catch (e: Exception) {

                                }
                            }
                        }

                    }
                }
            })
        }

    }

    var showpopupsecond = false
    private fun updateTextViewRatio(data: Double) {
        dataRex1 = data
        try {
            notifyDataSetChanged()
        } catch (e: Exception) {

        }
    }

    private fun updateTextView(data: Int) {
        dataRex = data
        try {
            notifyDataSetChanged()
        } catch (e: Exception) {

        }


    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }

    var dataRex = 0
    var dataRex1 = 0.0
    fun showDialog(activity: Activity, repository: Repository, userprofileData: UserProfileData) {
        val dialog = Dialog(activity, R.style.NewDialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_success_dailog)
        dialog.getWindow()
            ?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val txtMessage = dialog.findViewById(R.id.txtMessage) as TextView
        val btnOk = dialog.findViewById(R.id.btnOk) as AppCompatButton
        val btnCancel = dialog.findViewById(R.id.btnCancel) as AppCompatButton

        btnOk.setOnClickListener {
            val data = AppConstant.dataSaveList
            print(data)
            val arrayList: ArrayList<tblAnswer> = ArrayList()
            if (data!!.isNotEmpty()) {
                for (j in data) {
                    val tblAnswer = tblAnswer(userprofileData.district_ID,
                        userprofileData.block_ID,
                        userprofileData.cluster_ID.toLong(),
                        AppConstant.villageId.toLong(),
                        AppConstant.HamletId,
                        "",
                        AppConstant.patId,
                        3,
                        j.CatID,
                        j.Qid,
                        j.answerData,
                        j.ControlID,
                        j.answerId,
                        0,
                        0,
                        1,
                        Date(),
                        Prefs.getString("user", ""),
                        UUID.randomUUID().toString(), 0, "", "",
                        j.score
                    ,j.questionToBEAdd)
                    arrayList.add(tblAnswer)
                }

            }
            Coroutines.main {
                val repositorysave = repository.saveSectionAnswer(arrayList)
                val modelForSurveyId =
                    ModelForSurveyId(AppConstant.HamletId, false)
                val result1 = repository.addModelForSurveyId(modelForSurveyId)
                activity.toast(mContext.getString(R.string.save))


            }
            dialog.dismiss()
            activity.finish()
        }
        btnCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

    }
}

