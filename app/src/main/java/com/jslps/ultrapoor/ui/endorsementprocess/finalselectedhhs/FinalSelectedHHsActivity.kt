package com.jslps.ultrapoor.ui.endorsementprocess.finalselectedhhs

import android.Manifest
import android.app.Dialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.TextUtils
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.db.tblUploadImage
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast
import kotlinx.android.synthetic.main.activity_final_selected_hhs.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class FinalSelectedHHsActivity : AppCompatActivity() {
    private val REQUEST_PERMISSION = 100
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_PICK_IMAGE = 2
    private var stringBase64 = ""
    private var imageFileName = ""
    private lateinit var repository: Repository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_final_selected_hhs)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)
        repository = Repository(api, db, api2)
        checkCameraPermission()
        Coroutines.main {
            getListDataPat =
                repository.getPatListAfterApprove(AppConstant.HamletId) as ArrayList<tblAnswer>
            if (getListDataPat!!.isNotEmpty()) {
                val adpEndorseMent = FinalSelectedHHsAdapter(this, getListDataPat, repository)
                recyFinalSelectedHHs.adapter = adpEndorseMent
            }
            val getImageData = repository.getImageData(AppConstant.HamletId)
            if (getImageData.isNotEmpty()) {
                if (TextUtils.isEmpty(getImageData[0].base64)) {
                    imageFileName = getImageData[0].imageName
                    Glide
                        .with(this)
                        .load(getImageData[0].imageURL)
                        .centerCrop()
                        .placeholder(R.drawable.document_placeholder)
                        .into(imvSelectedImage);
                } else {
                    imageFileName = getImageData[0].imageName

                    try {
                        val decodedString: ByteArray =
                            android.util.Base64.decode(getImageData[0].base64,
                                android.util.Base64.DEFAULT)
                        val bitmap =
                            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                        imvSelectedImage.setImageBitmap(bitmap)
                        imvSelectedImage.invalidate()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

        }
        btnUploadImage.setOnClickListener {
            if (getListDataPat!!.isNotEmpty()) {
                if (TextUtils.isEmpty(imageFileName)) {
                    toast("Please select image")
                } else {
                    val tblImage = tblUploadImage("$imageFileName.jpg", getListDataPat!![0].PatId,
                            stringBase64, AppConstant.HamletId, 0, "")
                    Coroutines.main {
                        repository.saveUploadImage(tblImage)
                        toast("Final data save successfully")
                        finish()
                    }
                }

            } else {
                toast("No data for upload")
            }
        }
        txtAddImage.setOnClickListener {
            dialogCollectImage()
        }

    }

    var getListDataPat: ArrayList<tblAnswer>? = null
    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_PERMISSION
            )
        }
    }


    private fun dialogCollectImage() {
        val dialog = Dialog(this, R.style.NewDialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_upload_image)
        dialog.window
            ?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        val imvCancel: ImageView = dialog.findViewById(R.id.imvCancel)
        val txtTakePhoto: TextView = dialog.findViewById(R.id.txtTakePhoto)
        val txtChooseFromGallary: TextView = dialog.findViewById(R.id.txtChooseFromGallary)

        txtTakePhoto.setOnClickListener {
            openCamera()
            dialog.dismiss()
        }
        txtChooseFromGallary.setOnClickListener {
            openGallery()
            dialog.dismiss()
        }


        imvCancel.setOnClickListener() {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
            intent.resolveActivity(packageManager)?.also {
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun openGallery() {
        Intent(Intent.ACTION_GET_CONTENT).also { intent ->
            intent.type = "image/*"
            intent.resolveActivity(packageManager)?.also {
                startActivityForResult(intent, REQUEST_PICK_IMAGE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                val bitmap = data?.extras?.get("data") as Bitmap
                imvSelectedImage.setImageBitmap(bitmap)
                val dateFormat = SimpleDateFormat("yyyyMMdd_HH_mm_ss")
                val currentTimeStamp: String = dateFormat.format(Date())
                saveImage(bitmap, "ULTRA_POOR $currentTimeStamp")
                imageFileName = "ULTRA_POOR $currentTimeStamp"
                stringBase64 = encodedBase64(bitmap)

            } else if (requestCode == REQUEST_PICK_IMAGE) {
                val uri = data?.data
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                val dateFormat = SimpleDateFormat("yyyyMMdd_HH_mm_ss")
                val currentTimeStamp: String = dateFormat.format(Date())
                imvSelectedImage.setImageBitmap(bitmap)
                saveImage(bitmap, "ULTRA_POOR $currentTimeStamp")
                imageFileName = "ULTRA_POOR $currentTimeStamp"
                stringBase64 = encodedBase64(bitmap)

            }
        }
    }

    private fun saveImage(bitmap: Bitmap, name: String) {
        val fos: OutputStream?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val resolver = contentResolver
            val contentValues = ContentValues()
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "$name.jpg")
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
            contentValues.put(
                MediaStore.MediaColumns.RELATIVE_PATH,
                Environment.DIRECTORY_PICTURES + File.separator + "ULTRAPOOR"
            )
            val imageUri: Uri? =
                resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
            fos = resolver.openOutputStream(Objects.requireNonNull(imageUri)!!)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            Objects.requireNonNull(fos)?.close()
        } else {
            //
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream)
            val imagedata = byteArrayOutputStream.toByteArray()
            val encodedBase64 = android.util.Base64.encodeToString(
                imagedata,
                android.util.Base64.DEFAULT
            )
            val sdcard = Environment.getExternalStorageDirectory().toString() + "/" + "ULTRAPOOR"
            val mediaStorageDir = File("$sdcard/", "ULTRAPOOR")
            val temp = File(mediaStorageDir.path)
            if (!temp.exists()) {
                temp.mkdirs()
            }
            val timeStamp = SimpleDateFormat(
                "yyyyMMdd_HHmmss",
                Locale.getDefault()
            ).format(Date())
        }


    }


    private fun encodedBase64(bitmap: Bitmap): String {
        var stringBase64 = ""
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()

        stringBase64 = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Base64.getEncoder().encodeToString(byteArray)
        } else {
            android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT);
        }

        return stringBase64

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}