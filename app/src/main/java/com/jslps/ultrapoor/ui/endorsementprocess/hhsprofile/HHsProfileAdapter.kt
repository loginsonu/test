package com.jslps.ultrapoor.ui.endorsementprocess.hhsprofile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.response.QuestionTypeMaster
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.Coroutines


class HHsProfileAdapter(
    private val mContext: Context,
    private val hhsProfileModel: List<QuestionTypeMaster>,
    private val repository: Repository,
    private val endmes: tblAnswer,
) : RecyclerView.Adapter<HHsProfileAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_hhs_profile,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.txtParameter.text = hhsProfileModel[position].Qname
        //val arrHHs = hhsProfileModel!![position].HHsOptionModel

        Coroutines.main {
            when (hhsProfileModel[position].Qid) {
                30 -> {
                    val controlMasterDataOption =
                        repository.getControlMasterDataOption1(hhsProfileModel[position].Qid)
                    val adpHHsOption =
                        HHsOptionAdapter(mContext, controlMasterDataOption, repository, endmes)
                    holder.recycOption.adapter = adpHHsOption
                }
                37 -> {
                    val controlMasterDataOption =
                        repository.getControlMasterDataOption1(37)
                    val adpHHsOption =
                        HHsOptionAdapter(mContext, controlMasterDataOption, repository, endmes)
                    holder.recycOption.adapter = adpHHsOption
                }
                else -> {
                    val controlMasterDataOption =
                        repository.getControlMasterDataOption(hhsProfileModel[position].Qid)
                    val adpHHsOption =
                        HHsOptionAdapter(mContext, controlMasterDataOption, repository, endmes)
                    holder.recycOption.adapter = adpHHsOption
                }
            }

        }


    }

    override fun getItemCount(): Int {
        return hhsProfileModel!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtParameter: TextView = itemView.findViewById(R.id.txtParameter)
        var recycOption: RecyclerView = itemView.findViewById(R.id.recycOption)

    }
}