package com.jslps.ultrapoor.ui.login


import android.view.View
import androidx.lifecycle.ViewModel
import com.jslps.ultrapoor.data.db.User
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.ApiException
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.NoInternetException
import com.pixplicity.easyprefs.library.Prefs
import java.util.*


class LoginViewModel(
    val repository: Repository,
) : ViewModel() {

    var username: String? = null
    var password: String? = null
    var authListner: LoginListner? = null


    fun onLoginButtonClick(view: View) {
        authListner?.onStarted()
        if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
            authListner?.onFailure("Userid or password is empty")
        } else {
            Coroutines.main {
                try {
                    val userrPr = Prefs.getString("user", "")
                    val getUser = repository.getUser(
                        username!!.toLowerCase(Locale.ROOT),
                        password!!
                    )
                    if (getUser < 1) {
                        try {
                            val encryptPasswordI = password!!
                            val authResponse = repository.userLogin(username!!, password!!)
                            if (authResponse.data!=null) {
                                Coroutines.mainIo {
                                    val saveVillageList =
                                        repository.saveVillageList(authResponse.data.VillageList)
                                    val saveActivityMaster =
                                        repository.saveActivityMaster(authResponse.data.ActivityMaster)
                                    val saveActivityListMaster =
                                        repository.saveActivityListMaster(authResponse.data.ActivityListMaster)
                                    val saveControlDataMaster =
                                        repository.saveControlDataMaster(authResponse.data.ControlDataMaster)
                                    val saveControlNameMaster =
                                        repository.saveControlNameMaster(authResponse.data.ControlNameMaster)
                                    val saveQuestionTypeMaster =
                                        repository.saveQuestionTypeMaster(authResponse.data.QuestionTypeMaster)
                                    val saveUserProfileData =
                                        repository.saveUserProfileData(authResponse.data.UserProfileData)
                                    val saveDependencyMaster =
                                        repository.saveDependencyMaster(authResponse.data.DependencyMaster)
                                    val saveVoList =
                                        repository.saveVoList(authResponse.data.VoList)
                                    val reasonList =
                                        repository.saveRejectList(authResponse.data.RejectReasonList)
                                    // txn data save
                                    val AddHammletList =
                                        repository.saveHamletList(authResponse.data.AddHammletList)
                                    val HammletTxnList =
                                        repository.saveSectionAnswer(authResponse.data.HammletTxnList)
                                    if (authResponse.data.UpajDocument.isNotEmpty()) {
                                        for (i in authResponse.data.UpajDocument) {
                                            val reasonList =
                                                repository.saveUploadImage(i)
                                        }
                                    }


                                    val user = User(
                                        username!!.toLowerCase(Locale.ROOT),
                                        encryptPasswordI,
                                        "0",
                                        "",
                                        ""
                                    )
                                    repository.saveUser(user)
                                    Prefs.putString("user", username!!.toLowerCase(Locale.ROOT))
                                    Prefs.putString("password", password!!)

                                    authListner?.gotonext()
                                }
                            }else{
                                authListner?.onFailure(authResponse.message)
                            }
                        } catch (e: Exception) {
                            authListner?.onFailure(e.toString())
                        }
                    } else {
                        //if user logged in the device before
                        val getUser1 = repository.getUser(
                            username!!.toLowerCase(Locale.ROOT),
                            password!!
                        )
                        if (getUser != null) {
                            Prefs.putString("user", username!!.toLowerCase(Locale.ROOT))
                            Prefs.putString("password", password!!)
                            authListner?.gotonext()
                        } else {
                            authListner?.onFailure("Username or Password is wrong")
                        }
                    }

                } catch (e: ApiException) {
                    authListner?.onFailure(e.message!!)
                } catch (e: NoInternetException) {
                    authListner?.onFailure(e.message!!)
                }

            }
        }


    }


}
