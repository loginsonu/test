package com.jslps.ultrapoor.ui.section

import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.db.ModelForSurveyId
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.network.response.*
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.AppConstant.Companion.dataSaveList
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.buttonEnabledOrNot
import com.jslps.ultrapoor.util.toast
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.question_list_activity.*
import kotlinx.android.synthetic.main.question_list_activity.Hamlettext
import kotlinx.android.synthetic.main.question_list_activity.Villegetext

import kotlinx.android.synthetic.main.question_list_activity.recyQuestionList
import kotlinx.android.synthetic.main.tool_bar_section.*
import java.util.*
import kotlin.collections.ArrayList

class QuestionListPage : AppCompatActivity() {

    private lateinit var repository: Repository
    var userProfileData: UserProfileData? = null
    lateinit var tblAnswer: ArrayList<tblAnswer>
    lateinit var activityListMaster: ActivityListMaster
    var sectionId = 0
    private var updateDataBySectionId: ArrayList<tblAnswer>? = null
    var adpSection: QuestionSectionAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide()
        setContentView(R.layout.question_list_activity)
        val i = intent
        activityListMaster = i.getSerializableExtra("sectionObject") as ActivityListMaster
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)
        repository = Repository(api, db, api2)
        var arrDataQuestionControl: ArrayList<ControlDataMasterJoin>? = null
        Coroutines.main {
            userProfileData = repository.getUserProfileData()

            //GET USER PROFILE DATA FROM USER PROFILE TABLE
            repository.getUserProfileData().let {
                Villegetext.text = AppConstant.villageName
                Hamlettext.text = AppConstant.hamletName
                txtHeader.text = activityListMaster.ActivityName
            }
            if (!TextUtils.isEmpty(AppConstant.HamletId)) {
                updateDataBySectionId = ArrayList()
                updateDataBySectionId = repository.getSaveDataBySectionId(AppConstant.HamletId,
                    activityListMaster.CatID,
                    activityListMaster.SequNo, AppConstant.patId) as ArrayList<tblAnswer>
            }
            arrDataQuestionControl =
                repository.getControlDataMaster(
                    activityListMaster.CatID.toString(),
                    activityListMaster.SequNo.toString()
                ) as ArrayList<ControlDataMasterJoin>

            val arrData = repository.getQuestionList(
                activityListMaster.CatID,
                activityListMaster.SequNo.toString()
            )
            AppConstant.dataSaveList?.clear()
            val arrayListAnswerFillData: ArrayList<AnswerValueFill> = ArrayList()
            if (updateDataBySectionId!!.isEmpty()) {
                if (arrData.isNotEmpty()) {
                    for (j in arrData) {
                        val answerValueFillnew = AnswerValueFillNew(
                            j.CatID, j.ControlID, j.Qid, j.ControlName, j.SectionID, "",
                            "", "", "", 0, "", UUID.randomUUID().toString())
                        val answerValueFill = AnswerValueFill(
                            j.CatID, j.ControlID, j.Qid, j.ControlName, j.SectionID, "",
                            "",
                        )
                        arrayListAnswerFillData.add(answerValueFill)
                        dataSaveList?.add(answerValueFillnew)
                    }
                }
            }
            if (!updateDataBySectionId.isNullOrEmpty()) {
                AppConstant.dataSaveList?.clear()
                for (j in updateDataBySectionId!!) {
                    val answerValueFillnew = AnswerValueFillNew(
                        j.CatID, j.ControlTypeID, j.QID, "", j.SectionID.toString(), j.AnswerValue,
                        j.questionToBeAdd.toString(),j.AnswerID, j.Score, 0, "",
                        j.uuid)
                    AppConstant.dataSaveList?.add(answerValueFillnew)

                }
                for (j in updateDataBySectionId!!) {
                    if (j.isExported == 1) {
                        flagexported = true
                        break
                    } else {
                        flagexported = false
                    }
                }
                if (flagexported) {
                    buttonEnabledOrNot(btnSaveData, false)
                } else {
                    buttonEnabledOrNot(btnSaveData, true)

                }
            }
            adpSection =
                QuestionSectionAdapter(this,
                    arrData,
                    arrDataQuestionControl!!,
                    repository,
                    arrayListAnswerFillData,
                    updateDataBySectionId)
            recyQuestionList.adapter = adpSection

        }
        btnSaveData.setOnClickListener {
            var flagsave = false
            tblAnswer = ArrayList<tblAnswer>()
            val data = AppConstant.dataSaveList
            print(data)
            val arrayList: ArrayList<tblAnswer> = ArrayList()
            if (data!!.isNotEmpty()) {
                for (j in data) {
                    if (j.answerData == "") {
                        flagsave = false
                        break
                    } else {
                        flagsave = true
                        val tblAnswer = tblAnswer(userProfileData!!.district_ID,
                            userProfileData!!.block_ID,
                            userProfileData!!.cluster_ID.toLong(),
                            AppConstant.villageId.toLong(),
                            AppConstant.HamletId,
                            "",
                            AppConstant.patId,
                            activityListMaster.SequNo,
                            j.CatID,
                            j.Qid,
                            j.answerData,
                            j.ControlID,
                            j.answerId,
                            0,
                            0,
                            0,
                            Date(),
                            Prefs.getString("user", ""),
                            j.uuid,
                            0,
                            "", "", j.score, j.questionToBEAdd
                        )
                        arrayList.add(tblAnswer)
                    }

                }
                Coroutines.main {
                    if (flagsave) {
                        val deletedata =
                            repository.deleteAnswerData(activityListMaster.ID, AppConstant.HamletId)
                        val repositorysave = repository.saveSectionAnswer(arrayList)
                        toast(getString(R.string.save))
                        val modelForSurveyId =
                            ModelForSurveyId(AppConstant.HamletId, false)
                        val result1 = repository.addModelForSurveyId(modelForSurveyId)
                        AppConstant.dataSaveList?.clear()
                        finish()
                    } else {
                        toast("कृपया सभी अनिवार्य फ़ील्ड दर्ज करें")
                    }


                }
            }

        }
    }

    var flagexported = false

}