package com.jslps.ultrapoor.ui.patSurvey

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.response.AnswerValueFill
import com.jslps.ultrapoor.data.network.response.AnswerValueFillNew
import com.jslps.ultrapoor.data.network.response.ControlDataMasterJoin
import com.jslps.ultrapoor.data.network.response.QuestionTypeMasterJoin
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.listner.OnFragmentListItemSelectListener
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.SetSpinnerText
import com.jslps.ultrapoor.util.toast
import com.pixplicity.easyprefs.library.Prefs
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class PatQuestionAdapterBottomSheet(
    private val mContext: Context,
    private val listQuestion: List<QuestionTypeMasterJoin>,
    private val arrDataQuestionControl: List<ControlDataMasterJoin>,
    private val questionNo: String,
    private val arrayListAnswerFillData: ArrayList<tblAnswer>?,
    private val repository: Repository,
    private val answerid: String,
    private val dialogdd: BottomSheetDialog,
) : RecyclerView.Adapter<PatQuestionAdapterBottomSheet.MainViewHolderBottomSheet>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolderBottomSheet {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_questions_pat,
            parent, false)
        return MainViewHolderBottomSheet(view)
    }

    var mListner: OnFragmentListItemSelectListener? = null

    fun setListner(listner: OnFragmentListItemSelectListener?) {
        this.mListner = listner
    }

    override fun onBindViewHolder(holder: MainViewHolderBottomSheet, position: Int) {

        val item = listQuestion[position]
        if (item.Qid == 20) {
            Prefs.putString("flagaa","notshow")
        }
        holder.txtQuestion.text = (position + 1).toString() + ". " + item.Qname
        val type = item.ControlID
        val controlNameRec = item.ControlName

        if (controlNameRec == "EditText") {
            holder.edtHamNameText.visibility = View.VISIBLE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            if (AppConstant.dataSaveList!!.isNotEmpty())
                for (i in AppConstant.dataSaveList!!) {
                    if (i.answerData != "") {
                        if (i.ControlName == "EditText") {
                            if (i.Qid == item.Qid) {
                                holder.edtHamNameText.text = i.answerData
                            }
                        }
                    }
                }
        } else if (controlNameRec == "Spinner") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.VISIBLE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            val arrayListControlNameMaster: ArrayList<ControlDataMasterJoin> = ArrayList()
            if (arrDataQuestionControl.isNotEmpty()) {
                for (i in arrDataQuestionControl) {
                    if (item.Qid == i.QID) {
                        arrayListControlNameMaster.add(i)
                    }
                }
                if (arrayListControlNameMaster.isNotEmpty()) {
                    arrayListControlNameMaster.add(0,
                        ControlDataMasterJoin(0, 0, "Select Item", "", "", 0, 0, 0, "", 0))

                    val adapter = ArrayAdapter(mContext,
                        android.R.layout.simple_expandable_list_item_1, arrayListControlNameMaster)
                    holder.spDropDown.adapter = adapter
                }
            }
            if (AppConstant.dataSaveList!!.isNotEmpty())
                for (i in AppConstant.dataSaveList!!) {
                    if (i.answerData != "") {
                        if (i.ControlName == "Spinner") {
                            if (i.Qid == item.Qid) {
                                SetSpinnerText(holder.spDropDown, i.answerData)
                            }
                        }
                    }
                }

        } else if (controlNameRec == "MultiSelect") {
            holder.recyMultipleChoice.visibility = View.VISIBLE
            holder.edtHamNameText.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE

            val arrayListControlNameMaster: ArrayList<ControlDataMasterJoin> = ArrayList()
            if (arrDataQuestionControl.isNotEmpty()) {
                for (i in arrDataQuestionControl) {
                    if (item.Qid == i.QID) {
                        arrayListControlNameMaster.add(i)
                    }
                }
                if (arrayListControlNameMaster.isNotEmpty()) {
                    val adpSection =
                        PatAdapterMultipleChoice(mContext, arrayListControlNameMaster)
                    holder.recyMultipleChoice.adapter = adpSection
                }
            }
        } else if (controlNameRec == "EditTextNumeric") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.VISIBLE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.GONE
            if (AppConstant.dataSaveList!!.isNotEmpty())
                for (i in AppConstant.dataSaveList!!) {
                    if (i.answerData != "") {
                        if (i.ControlName == "EditTextNumeric") {
                            if (i.Qid == item.Qid) {
                                holder.edtHamNameNumric.text = i.answerData
                            }
                        }
                    }
                }
        } else if (controlNameRec == "XY") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.VISIBLE
            holder.photoLayout.visibility = View.GONE

        } else if (controlNameRec == "Image") {
            holder.edtHamNameText.visibility = View.GONE
            holder.recyMultipleChoice.visibility = View.GONE
            holder.spDropDown.visibility = View.GONE
            holder.edtHamNameNumric.visibility = View.GONE
            holder.latlonglayout.visibility = View.GONE
            holder.photoLayout.visibility = View.VISIBLE

        }

    }


    override fun getItemCount(): Int {
        return listQuestion.size
    }

    var mSpinnerSelectedItem: HashMap<Int, Int> = HashMap()

    inner class MainViewHolderBottomSheet internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtQuestion: TextView = itemView.findViewById(R.id.txtQuestion)
        var edtHamNameText: TextView = itemView.findViewById(R.id.edtHamNametext)
        var edtHamNameNumric: TextView = itemView.findViewById(R.id.edtHamNameNumric)
        var recyMultipleChoice: RecyclerView = itemView.findViewById(R.id.recyMultipleChoice)
        var spDropDown: Spinner = itemView.findViewById(R.id.spDropDown)
        var latlonglayout: LinearLayout = itemView.findViewById(R.id.latlonglayout)
        var photoLayout: LinearLayout = itemView.findViewById(R.id.photoLayout)
        var selectedControlData: ControlDataMasterJoin? = null

        init {
            spDropDown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    l: Long,
                ) {
                    //String ASIN = gameDataArr.get(position).getAmazonId();
                    if (position == 0) {

                    } else {
                        mListner?.onListItemSelected(position,
                            AnswerValueFill(listQuestion[adapterPosition].CatID,
                                listQuestion[adapterPosition].ControlID,
                                listQuestion[adapterPosition].Qid,
                                listQuestion[adapterPosition].ControlName.toString(),
                                listQuestion[adapterPosition].SectionID,
                                spDropDown.getItemAtPosition(position).toString(),
                                questionNo))
                        var controlDataMasterJoin: ControlDataMasterJoin? = null
                        for (i in arrDataQuestionControl) {
                            if (listQuestion[adapterPosition].Qid == i.QID
                            ) {
                                if (spDropDown.getItemAtPosition(position)
                                        .toString() == i.ControlData
                                ) {
                                    controlDataMasterJoin = i
                                }
                            }
                        }


                        for (i in AppConstant.dataSaveList!!) {
                            if (i.Qid == listQuestion[adapterPosition].Qid) {
                                i.answerData = spDropDown.getItemAtPosition(position).toString()
                                if (!TextUtils.isEmpty(controlDataMasterJoin!!.Score)) {
                                    i.score = controlDataMasterJoin.Score.toString()
                                }
                            }
                        }
                        mSpinnerSelectedItem.put(adapterPosition, position);
                        Coroutines.main {
                            val getDependencyData = repository.getDependencyData(
                                controlDataMasterJoin!!.QID,
                                controlDataMasterJoin.CID)
                            if (getDependencyData.isNotEmpty()) {
                                val dialog = BottomSheetDialog(mContext)
                                val view1 = LayoutInflater.from(mContext).inflate(
                                    R.layout.bottom_sheet, null
                                )
                                val recyclerView: RecyclerView =
                                    view1.findViewById(R.id.recyQuestionList)
                                val layoutManager = LinearLayoutManager(mContext)
                                recyclerView.layoutManager = layoutManager
                                var count = 0
                                var countCompare = 0

                                for (i in getDependencyData.indices) {
                                    for (j in AppConstant.dataSaveList!!.indices) {
                                        countCompare = getDependencyData.size
                                        if (getDependencyData[i].Qid != AppConstant.dataSaveList!![j].Qid) {
                                            if (j == AppConstant.dataSaveList!!.size - 1) {
                                                count++
                                                AppConstant.dataSaveList?.add(AnswerValueFillNew(
                                                    getDependencyData[i].CatID,
                                                    getDependencyData[i].ControlID,
                                                    getDependencyData[i].Qid,
                                                    getDependencyData[i].ControlName.toString(),
                                                    getDependencyData[i].SectionID,
                                                    "",
                                                    questionNo.toString(),
                                                    answerid,
                                                    "",
                                                    0,
                                                    "",
                                                    UUID.randomUUID().toString()))
                                                break
                                            }

                                            if (count == countCompare)
                                                break
                                        } else {
                                            AppConstant.dataSaveList?.set(j,
                                                AnswerValueFillNew(getDependencyData[i].CatID,
                                                    getDependencyData[i].ControlID,
                                                    getDependencyData[i].Qid,
                                                    getDependencyData[i].ControlName.toString(),
                                                    getDependencyData[i].SectionID,
                                                    AppConstant.dataSaveList!![j].answerData,
                                                    questionNo,
                                                    answerid,
                                                    "",
                                                    0,
                                                    "",
                                                    AppConstant.dataSaveList!![j].uuid))

                                        }
                                    }
                                }

                                val madapter =
                                    PatQuestionAdapterBottomSheetSecond(
                                        mContext,
                                        repository,
                                        getDependencyData,
                                        arrDataQuestionControl,
                                        questionNo,
                                        arrayListAnswerFillData, answerid,dialog
                                    )
                                recyclerView.adapter = madapter
                                val btnClose = view1.findViewById<ImageView>(R.id.cancelQuestion)
                                val buttonBottom = view1.findViewById<Button>(R.id.buttonBottom)
                                buttonBottom?.setOnClickListener {
                                    var count = 0
                                    for (i in AppConstant.dataSaveList!!) {
                                        for (j in getDependencyData) {
                                            if (i.Qid == j.Qid) {
                                                count++
                                                if (i.answerData == "") {
                                                    mContext.toast("Please enter mandatory field")
                                                } else {
                                                    if (count == getDependencyData.size)
                                                        dialog.dismiss()
                                                }
                                            }
                                        }
                                    }
                                }
                                btnClose.setOnClickListener {
                                    dialog.dismiss()
                                }
                                dialogdd.cancel()
                                dialog.setContentView(view1)
                                dialog.setCancelable(false)
                                dialog.show()
                            } else {
                                try {
                                    for (i in AppConstant.dataSaveList!!.indices) {
                                        if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                            AppConstant.dataSaveList!!.removeAt(i)

                                        }
                                    }
                                } catch (e: Exception) {
                                    try {
                                        for (i in AppConstant.dataSaveList!!.indices) {
                                            if (AppConstant.dataSaveList!![i].questionToBEAdd == listQuestion!![adapterPosition].Qid.toString()) {
                                                AppConstant.dataSaveList!!.removeAt(i)

                                            }
                                        }
                                    } catch (e: Exception) {

                                    }
                                }

                            }

                        }


                    }


                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {

                }
            }
            edtHamNameText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int,
                ) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable) {
                    // write here
                    if (s.toString().isNotEmpty()) {
                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (AppConstant.dataSaveList!![i].Qid == listQuestion[adapterPosition].Qid) {
                                AppConstant.dataSaveList!!.set(i,
                                    AnswerValueFillNew(listQuestion[adapterPosition].CatID,
                                        listQuestion[adapterPosition].ControlID,
                                        listQuestion[adapterPosition].Qid,
                                        listQuestion[adapterPosition].ControlName.toString(),
                                        listQuestion[adapterPosition].SectionID,
                                        s.toString(),
                                        questionNo, answerid, "", 0, "",
                                        AppConstant.dataSaveList!![i].uuid))
                            }
                        }
                    }
                }
            })
            edtHamNameNumric.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int,
                ) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable) {
                    // write here
                    if (s.toString().isNotEmpty()) {
                        for (i in AppConstant.dataSaveList!!.indices) {
                            if (AppConstant.dataSaveList!![i].Qid == listQuestion[adapterPosition].Qid) {
//                                if (AppConstant.dataSaveList!![i].answerId == answerid) {
//                                    AppConstant.dataSaveList!!.set(i,
//                                        AnswerValueFillNew(listQuestion[adapterPosition].CatID,
//                                            listQuestion[adapterPosition].ControlID,
//                                            listQuestion[adapterPosition].Qid,
//                                            listQuestion[adapterPosition].ControlName.toString(),
//                                            listQuestion[adapterPosition].SectionID,
//                                            s.toString(),
//                                            questionNo, answerid, "", 0, ""))
//                                }
                                AppConstant.dataSaveList!![i].answerData = s.toString()
                            }
                        }
                    }
                }
            })
        }
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }
}