package com.jslps.ultrapoor.ui.section

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.response.AnswerValueFill
import com.jslps.ultrapoor.data.network.response.AnswerValueFillNew
import com.jslps.ultrapoor.data.network.response.ControlDataMaster
import com.jslps.ultrapoor.data.network.response.ControlDataMasterJoin
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.listner.OnFragmentListItemSelectListener
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.AppConstant.Companion.dataSaveList
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class AdapterMultipleChoice(
    private val mContext: Context,
    val repository: Repository,
    private val section: List<ControlDataMaster>?,
    private var arrDataQuestionControl: List<ControlDataMasterJoin>,
    val sectionId: String,
    val catId: Int,
    private val answerModelListPage: ArrayList<AnswerModel>?,
    private val updateDataBySectionId: ArrayList<tblAnswer>?,
) : RecyclerView.Adapter<AdapterMultipleChoice.MainViewHolder>(), OnFragmentListItemSelectListener {
    var arrayList: ArrayList<AnswerModel>? = ArrayList()
    var mListner: OnFragmentListItemSelectListener? = null

    fun setListner(listner: OnFragmentListItemSelectListener?) {
        this.mListner = listner
    }

    var hasSet = HashSet<String>()
    var hasMap = HashMap<String, HashSet<String>>()
    var selectedPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_multiple_choice,
            parent, false
        )
        return MainViewHolder(view)
    }

    val arrayListAnswerFillData: ArrayList<AnswerValueFill> = ArrayList()
    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        val section = section!![position]
        holder.check_Box.text = section.ControlData
        if (AppConstant.dataSaveList!!.isNotEmpty()) {
            for (i in AppConstant.dataSaveList!!) {

                if (i.answerData.toString() != "") {
                    if (i.ControlID == 4) {
                        if (i.Qid == section.QID) {
                            if (i.answerData == section.ControlData) {
                                i.answerId = section.CID.toString()
                                holder.check_Box.isChecked = true
                            }
                        }

                    }
                }
            }
        }
        var count1 = 0
        for (i in AppConstant.dataSaveList!!.indices) {
            if (AppConstant.dataSaveList!![i].Qid == section.QID) {
                count1++

                if (i == (AppConstant.dataSaveList!!.size - 1)) {
                    if (count1 == 1) {
                        if (AppConstant.dataSaveList!![i].answerData == section.ControlData) {
                            AppConstant.dataSaveList!![i].answerId = section.CID.toString()
                        }
                    } else {
                        if (AppConstant.dataSaveList!![i].answerData == section.ControlData) {
                            AppConstant.dataSaveList!![i].answerId = section.CID.toString()
                            AppConstant.dataSaveList!![i].answerIdDelete = section.CID.toString()
                            AppConstant.dataSaveList!![i].questionToBEAdd = section.QID.toString()
                        }
                    }

                }
            }
        }
        holder.check_Box.setOnCheckedChangeListener { compoundButton, boolean ->
            if (boolean) {
                var count = 0
                var checkvaluee = true
                for (i in AppConstant.dataSaveList!!.indices) {
                    var dataa = section
                    if (AppConstant.dataSaveList!![i].Qid == section.QID) {
                        count++
                        if (count == 1) {
                            if (AppConstant.dataSaveList!![i].answerData == "") {
                                AppConstant.dataSaveList!![i].answerData = section.ControlData
                                AppConstant.dataSaveList!![i].answerId = section.CID.toString()
                            } else {
                                AppConstant.dataSaveList?.add(
                                    AnswerValueFillNew(2,
                                        4,
                                        section.QID,
                                        "MultiSelect",
                                        sectionId,
                                        section.ControlData,
                                        section.QID.toString(),
                                        section.CID.toString(),
                                        "",
                                        0,
                                        section.CID.toString(),
                                        UUID.randomUUID().toString()))
                                break
                            }
                        } else {
                            AppConstant.dataSaveList?.add(
                                AnswerValueFillNew(2,
                                    4,
                                    section.QID,
                                    "MultiSelect",
                                    sectionId,
                                    section.ControlData,
                                    section.QID.toString(),
                                    section.CID.toString(),
                                    "",
                                    0,
                                    section.CID.toString(),
                                    UUID.randomUUID().toString()))
                            break
                        }

                    }

                }
                var checkvalue = true
                Coroutines.main {
                    val getDependencyData = repository.getDependencyData(section.QID, section.CID)
                    if (getDependencyData.isNotEmpty()) {
                        val dialog = BottomSheetDialog(mContext)
                        val view1 = LayoutInflater.from(mContext).inflate(
                            R.layout.bottom_sheet, null
                        )
                        val recyclerView: RecyclerView =
                            view1.findViewById(R.id.recyQuestionList)
                        val layoutManager = LinearLayoutManager(mContext)
                        recyclerView.layoutManager = layoutManager
                        var count = 0
                        var countCompare = 0

                        for (i in getDependencyData.indices) {
                            for (j in AppConstant.dataSaveList!!.indices) {
                                val value1 = AppConstant.dataSaveList!![j]
                                    .Qid.toString() + AppConstant.dataSaveList!![j]
                                    .answerId.toString() + AppConstant.dataSaveList!![j].SectionID.toString() +
                                        AppConstant.dataSaveList!![j].CatID.toString() + AppConstant.patId
                                val value2 =
                                    getDependencyData[i].Qid.toString() + section.CID.toString() + getDependencyData[i].SectionID.toString() +
                                            getDependencyData[i].CatID.toString() + AppConstant.patId
                                if (value1 == value2) {
                                    AppConstant.dataSaveList?.set(j,
                                        AnswerValueFillNew(getDependencyData[i].CatID,
                                            getDependencyData[i].ControlID,
                                            getDependencyData[i].Qid,
                                            getDependencyData[i].ControlName.toString(),
                                            getDependencyData[i].SectionID,
                                            AppConstant.dataSaveList!![j].answerData,
                                            section.QID.toString(),
                                            section.CID.toString(),
                                            "",
                                            0,
                                            section.CID.toString(),
                                            AppConstant.dataSaveList!![j].uuid))
                                    checkvalue = false
                                }
                                if (j == (AppConstant.dataSaveList!!.size - 1)) {
                                    if (checkvalue) {
                                        AppConstant.dataSaveList?.add(
                                            AnswerValueFillNew(
                                                getDependencyData[i].CatID,
                                                getDependencyData[i].ControlID,
                                                getDependencyData[i].Qid,
                                                getDependencyData[i].ControlName.toString(),
                                                getDependencyData[i].SectionID,
                                                "",
                                                section.QID.toString(),
                                                section.CID.toString(),
                                                "",
                                                0,
                                                section.CID.toString(),
                                                UUID.randomUUID().toString()))
                                    }
                                }

                            }
                        }

                        val madapter =
                            ProfileQuestionAdapterBottomSheet(
                                mContext,
                                repository,
                                getDependencyData,
                                arrDataQuestionControl,
                                section.QID.toString(),
                                updateDataBySectionId,
                                section.CID.toString(),
                                dialog
                            )
                        madapter.setListner(this)
                        recyclerView.adapter = madapter
                        val btnClose = view1.findViewById<ImageView>(R.id.cancelQuestion)
                        btnClose.setOnClickListener {
                            dialog.dismiss()
                        }
                        val buttonBottom = view1.findViewById<Button>(R.id.buttonBottom)
                        buttonBottom?.setOnClickListener {
                            var count = 0
                            for (i in AppConstant.dataSaveList!!) {
                                for (j in getDependencyData) {
                                    if (i.Qid == j.Qid) {
                                        count++
                                        if (i.answerData == "") {
                                            mContext.toast("Please enter mandatory field")
                                        } else {
                                            if (count == getDependencyData.size)
                                                dialog.dismiss()
                                        }
                                    }
                                }
                            }
                        }
                        dialog.setContentView(view1)
                        dialog.setCancelable(false)
                        dialog.show()
                    }


                }
            } else {
                try {
                    var count = 0
                    for (i in AppConstant.dataSaveList!!.indices) {
                        if (AppConstant.dataSaveList!![i].Qid == section.QID) {
                            count++
                        }
                    }
                    for (j in AppConstant.dataSaveList!!.indices) {
                        if (count > 1) {
                            if (AppConstant.dataSaveList!![j].answerId == section.CID.toString()) {
                                AppConstant.dataSaveList!!.removeAt(j)
                            }
                        }  else {
                            if (AppConstant.dataSaveList!![j].Qid == section.QID) {
                                if (AppConstant.dataSaveList!![j].answerId == section.CID.toString()) {
                                    AppConstant.dataSaveList!![j].answerData = ""
                                    AppConstant.dataSaveList!![j].answerId = ""
                                }
                            }
                        }

                    }
                } catch (e: Exception) {

                    try {

                        for (j in AppConstant.dataSaveList!!.indices) {
                            if (AppConstant.dataSaveList!![j].answerId == section.CID.toString()) {
                                AppConstant.dataSaveList!!.removeAt(j)
                            }

                        }
                    } catch (e: Exception) {

                    }
                }
                try {

                    for (i in dataSaveList!!.indices) {
                        if (dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                            if (dataSaveList!![i].answerId == section.CID.toString()) {
                                dataSaveList!!.removeAt(i)
                            }

                        }
                    }
                } catch (e: Exception) {
                    try {

                        for (i in dataSaveList!!.indices) {
                            if (dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                                if (dataSaveList!![i].answerId == section.CID.toString()) {
                                    dataSaveList!!.removeAt(i)
                                }

                            }
                        }
                    } catch (e: Exception) {
                        try {

                            for (i in dataSaveList!!.indices) {
                                if (dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                                    if (dataSaveList!![i].answerId == section.CID.toString()) {
                                        dataSaveList!!.removeAt(i)
                                    }

                                }
                            }
                        } catch (e: Exception) {
                            try {

                                for (i in dataSaveList!!.indices) {
                                    if (dataSaveList!![i].questionToBEAdd == section.QID.toString()) {
                                        if (dataSaveList!![i].answerIdDelete == section.CID.toString()) {
                                            dataSaveList!!.removeAt(i)
                                        }

                                    }
                                }
                            } catch (e: Exception) {

                            }
                        }
                    }
                }
                mListner?.onListItemSelected(11, arrayListAnswerFillData)

            }

        }

    }

    override fun getItemCount(): Int {
        return section!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var check_Box: CheckBox = itemView.findViewById(R.id.check_Box)

    }

    override fun onListItemSelected(itemId: Int, data: Any?) {

    }
}