package com.jslps.ultrapoor.ui.section

class AnswerModel {

    var answer: String
    val controlId: Int
    val questionID: Int
    val sectionID: String
    val catID:Int

    constructor(answer: String,controlId:Int,questionID:Int,sectionID:String,catID:Int) {
        this.answer = answer
        this.controlId = controlId
        this.questionID = questionID
        this.sectionID = sectionID
        this.catID = catID
    }
}