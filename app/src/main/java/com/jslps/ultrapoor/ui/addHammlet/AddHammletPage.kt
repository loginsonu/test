package com.jslps.ultrapoor.ui.addHammlet


import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AddHamlettbl
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.listner.OnListItem
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.network.response.UserProfileData
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.add_hammlet.*
import kotlinx.android.synthetic.main.add_hammlet.Clustertext
import kotlinx.android.synthetic.main.add_hammlet.blocktext
import kotlinx.android.synthetic.main.add_hammlet.districttext
import kotlinx.android.synthetic.main.tool_bar.*
import java.util.*


class AddHammletPage : AppCompatActivity(), OnListItem {
    private lateinit var repository: Repository
    private lateinit var hamletListAdapter: HamletListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide() // hide the title bar
        setContentView(R.layout.add_hammlet)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)
        repository = Repository(api, db, api2)
        val layoutManager = LinearLayoutManager(this);
        recyc.layoutManager = layoutManager
        addTola?.setOnClickListener {
            if (TextUtils.isEmpty(schoolleave_edit_text.text.toString())) {
                toast("कितने टोला जोड़ें करने हैं उसके लिए सांख्य दर्ज़ करे")

            } else {
                if (updateFlag) {
                    val arrayListAnswerFillData: ArrayList<AddHamlettbl> = ArrayList()
                    for (i in arrayListHammlet!!) {
                        val answerValueFill = AddHamlettbl(i.DistrictID,
                            i.BlockID,
                            i.ClusterID,
                            i.VillageID,
                            i.hamletId,
                            i.hamletName,
                            i.isExported,
                            i.CreatedOn,
                            i.CreatedBy)

                        arrayListAnswerFillData.add(answerValueFill)
                    }

                    val count = schoolleave_edit_text.text.toString().toInt()
                    val sixzw = arrayListHammlet!!.size
                    if (sixzw == count) {
                        hamletListAdapter =
                            HamletListAdapter(this,
                                schoolleave_edit_text.text.toString().toInt(),
                                arrayListHammlet, arrayListAnswerFillData)
                        recyc.adapter = hamletListAdapter
                        hamletListAdapter.setListner(this)
                        recyc.visibility = View.VISIBLE
                        addTolaMember.visibility = View.VISIBLE
                    } else if (count > sixzw) {
                        val differnce = count - sixzw
                        for (i in 1..differnce) {
                            val answerValueFill = AddHamlettbl(
                                0, 0, "", "", UUID.randomUUID().toString(), "", 0, Date(),
                                Prefs.getString("user", ""))

                            arrayListAnswerFillData.add(answerValueFill)
                        }
                        hamletListAdapter =
                            HamletListAdapter(this,
                                schoolleave_edit_text.text.toString().toInt(),
                                arrayListHammlet, arrayListAnswerFillData)
                        recyc.adapter = hamletListAdapter
                        hamletListAdapter.setListner(this)
                        recyc.visibility = View.VISIBLE
                        addTolaMember.visibility = View.VISIBLE
                    } else {
                        val diff = sixzw - count
                        val arrayListAnswerFillDatanew: ArrayList<AddHamlettbl> = ArrayList()
                        for (i in 1..count) {
                            arrayListAnswerFillDatanew.add(arrayListAnswerFillData[i])
                        }
                        hamletListAdapter =
                            HamletListAdapter(this,
                                schoolleave_edit_text.text.toString().toInt(),
                                arrayListHammlet, arrayListAnswerFillDatanew)
                        recyc.adapter = hamletListAdapter
                        hamletListAdapter.setListner(this)
                        recyc.visibility = View.VISIBLE
                        addTolaMember.visibility = View.VISIBLE
                    }

                } else {
                    val arrayListAnswerFillData: ArrayList<AddHamlettbl> = ArrayList()
                    for (i in 1..schoolleave_edit_text.text.toString().toInt()) {
                        val answerValueFill = AddHamlettbl(
                            0, 0, "", "", UUID.randomUUID().toString(),
                            "", 0, Date(),
                            Prefs.getString("user", ""))

                        arrayListAnswerFillData.add(answerValueFill)
                    }
                    hamletListAdapter =
                        HamletListAdapter(this,
                            schoolleave_edit_text.text.toString().toInt(),
                            null, arrayListAnswerFillData)
                    recyc.adapter = hamletListAdapter
                    hamletListAdapter.setListner(this)
                    recyc.visibility = View.VISIBLE
                    addTolaMember.visibility = View.VISIBLE

                }
            }
        }
        addTolaMember?.setOnClickListener {
            if (!TextUtils.isEmpty(schoolleave_edit_text.text.toString())) {
                if (hamletListAdapter.arrayListAnswerFillData!!.isEmpty() &&
                    hamletListAdapter.arrayListAnswerFillData!!.size == schoolleave_edit_text.text.toString()
                        .toInt()
                ) {
                    toast("कृपया टोला नाम दर्ज करें")
                } else {
                    var flagsave = false
                    Coroutines.main {
                        val arrayListSave: ArrayList<AddHamlettbl> = ArrayList()
                        if (hamletListAdapter.arrayListAnswerFillData!!.isNotEmpty()) {

                            for (i in hamletListAdapter.arrayListAnswerFillData!!) {
                                if (i.hamletName == "") {
                                    flagsave = false
                                    break
                                } else {
                                    flagsave = true
                                }
                                val addHamlettbl = AddHamlettbl(
                                    userprofileData!!.district_ID,
                                    userprofileData!!.block_ID,
                                    userprofileData!!.cluster_ID,
                                    AppConstant.villageId,
                                    i.hamletId, i.hamletName, i.isExported, i.CreatedOn, i.CreatedBy
                                )
                                arrayListSave.add(addHamlettbl)
                            }
                            if (flagsave) {
                                val delete = repository.deleteHamletData()
                                val saveAddHamlettbl = repository.saveHamletList(arrayListSave)
                                toast(getString(R.string.save))
                                finish()
                            } else {
                                toast("कृपया टोला नाम दर्ज करें")
                            }
                        }
                    }

                }
            }
        }
        getMaster()
    }

    var userprofileData: UserProfileData? = null
    var updateFlag: Boolean = false
    var arrayListHammlet: ArrayList<AddHamlettbl>? = null
    private fun getMaster() {
        Coroutines.main {
            userprofileData = repository.getUserProfileData()
            repository.getUserProfileData().let {
                districttext.text = it.districtname_h
                blocktext.text = it.blockName_H
                Clustertext.text = it.clusterName_H

            }
            arrayListHammlet = repository.getHmaletList(AppConstant.villageId) as ArrayList<AddHamlettbl>
            if (arrayListHammlet!!.isEmpty()) {
                recyc.visibility = View.GONE
                addTolaMember.visibility = View.GONE
                addTola.visibility = View.VISIBLE
            } else {
                updateFlag = true
                recyc.visibility = View.VISIBLE
                addTolaMember.visibility = View.VISIBLE
                val arrayListAnswerFillData: ArrayList<AddHamlettbl> = ArrayList()
                for (i in arrayListHammlet!!) {
                    val answerValueFill = AddHamlettbl(
                        i.DistrictID,
                        i.BlockID,
                        i.ClusterID,
                        i.VillageID,
                        i.hamletId,
                        i.hamletName,
                        i.isExported,
                        i.CreatedOn,
                        i.CreatedBy)
                    arrayListAnswerFillData.add(answerValueFill)
                }
                schoolleave_edit_text.setText(arrayListHammlet!!.size.toString())
                hamletListAdapter =
                    HamletListAdapter(this,
                        arrayListHammlet!!.size,
                        arrayListHammlet,
                        arrayListAnswerFillData)
                recyc.adapter = hamletListAdapter
                hamletListAdapter.setListner(this)
                recyc.visibility = View.VISIBLE
                addTolaMember.visibility = View.VISIBLE
                addTolaMember.text = "हेमलेट संपादित करें"


            }
        }

    }

    override fun onListItemSelected(count: Int, list: AddHamlettbl?, s: String) {
        if (s == "delete") {
            if (!TextUtils.isEmpty(schoolleave_edit_text.text.toString())) {
                val countAfterMinus = schoolleave_edit_text.text.toString().toInt() - 1
                schoolleave_edit_text.setText(count.toString())
            } else {
                recyc.visibility = View.GONE
                addTolaMember.visibility = View.GONE
            }
        }
    }


}
