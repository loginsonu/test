package com.jslps.ultrapoor.ui.patSurvey

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.network.response.ActivityListMaster
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines

class PatSectionAdapter(
    private val mContext: Context,
    private val section: List<ActivityListMaster>?,
    private val uuidRow: String?,
    private val repository: Repository,

    ) : RecyclerView.Adapter<PatSectionAdapter.MainViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.hamlet_section_list,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.txtSection.text = section!![position].ActivityName
        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, PatQuestionListPage::class.java)
            intent.putExtra("sectionObject", section[position])
            intent.putExtra("uuidRow", uuidRow)
            mContext.startActivity(intent)
        }

        Coroutines.main {

            val getdata = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                section[position].SequNo, AppConstant.patId)
            if (getdata.isNotEmpty()) {
                holder.imageView3.visibility = View.VISIBLE
            } else {
                holder.imageView3.visibility = View.GONE
            }

            val data1 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                2,
                AppConstant.patId)
           /* val data2 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                3,
                AppConstant.patId)*/
            val data3 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                4,
                AppConstant.patId)
            val data4 = repository.getSaveDataBySectionId(AppConstant.HamletId,
                section[position].CatID,
                15,
                AppConstant.patId)
            if (data1.isEmpty()) {
                if (section[position].SequNo > 2) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f
                }
            }
            /*else if (data2.isEmpty()) {
                if (section[position].SequNo > 3) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            } */
            else if (data3.isEmpty()) {
                if (section[position].SequNo > 4) {
                    holder.layoutaaay.isEnabled = false
                    holder.layoutaaay.isClickable = false
                    holder.layoutaaay.alpha = 0.5f

                } else {
                    holder.layoutaaay.isEnabled = true
                    holder.layoutaaay.isClickable = true
                    holder.layoutaaay.alpha = 1f

                }
            }

        }
    }

    override fun getItemCount(): Int {
        return section!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtSection: TextView = itemView.findViewById(R.id.txtSection)
        var imageView3: ImageView = itemView.findViewById(R.id.imageView3)
        var layoutaaay: ConstraintLayout = itemView.findViewById(R.id.layoutaaay)

    }
}