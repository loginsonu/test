package com.jslps.ultrapoor.ui.endorsementprocess

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.listner.OnListItemSelected
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.network.response.RejectReasonModelDb
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.ui.endorsementprocess.finalselectedhhs.FinalSelectedHHsActivity
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast
import kotlinx.android.synthetic.main.activity_endorsement_process.*


class EndorsementProcessActivity : AppCompatActivity(), OnListItemSelected {
    private lateinit var repository: Repository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide()
        setContentView(R.layout.activity_endorsement_process)

        // var arrEndorseMentProcess = ArrayList<EndorsementProcessModel>()
        val api2 = MyApiNew(networkConnectionInterceptor)

        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        repository = Repository(api, db, api2)

        getMaster()
        btnNext.setOnClickListener {
            val intent =
                Intent(this@EndorsementProcessActivity, FinalSelectedHHsActivity::class.java)
            startActivity(intent)

        }
    }

    private fun getMaster() {
        Coroutines.main {
            var score = 0
            val arrayListSend: ArrayList<tblAnswer>? = ArrayList()
            val getListDataPat = repository.getPatListApprove(AppConstant.HamletId)
            if (getListDataPat.isNotEmpty()) {
                for (i in getListDataPat.indices) {
                    val getDataForAnswer2 =
                        repository.getPatListApproveSum(getListDataPat[i].HamletID,
                            getListDataPat[i].PatId)

                    if (getDataForAnswer2 >= 7) {
                        arrayListSend?.add(getListDataPat[i])
                    }
                    if (i == (getListDataPat.size - 1)) {
                        val getListreason =
                            repository.getRejectReasonList() as ArrayList<RejectReasonModelDb>
                        if (getListreason.isNotEmpty()) {
                            getListreason.add(0, RejectReasonModelDb(0, "Select Item", "1"))
                            val adpEndorseMent =
                                EndorsementProcessAdapter(this,
                                    arrayListSend,
                                    repository,
                                    getListreason)
                            adpEndorseMent.setListner(this)
                            recyViewEndorseMentProcess.adapter = adpEndorseMent
                        }
                    }

                }

            }


        }
    }

    override fun onListItemSelected(positionGet: Int, list: Any) {
        if (positionGet == 11) {
            val endorsementProcessModel = list as tblAnswer
            Coroutines.main {
                val updateData = repository.saveApproveData(endorsementProcessModel.CatID,
                    endorsementProcessModel.PatId,
                    endorsementProcessModel.HamletID)
                toast("Pat Record Approved Successfully")
                getMaster()
            }
        } else if (positionGet == 12) {
            toast("Pat Record Rejected")
            getMaster()
        }
    }
}