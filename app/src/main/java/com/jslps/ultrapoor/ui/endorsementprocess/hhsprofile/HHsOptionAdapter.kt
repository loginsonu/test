package com.jslps.ultrapoor.ui.endorsementprocess.hhsprofile

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.network.response.ControlDataMaster
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.Coroutines
import java.text.DecimalFormat

class HHsOptionAdapter(
    private val mContext: Context,
    private val hHsOptionModel: List<ControlDataMaster>?,
    private val repository: Repository,
    private val endmes: tblAnswer,

    ) : RecyclerView.Adapter<HHsOptionAdapter.MainViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.row_hhs_options,
            parent, false
        )
        return MainViewHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val qid = hHsOptionModel!![position].QID
        val controlData = hHsOptionModel[position].ControlData
        if (hHsOptionModel[position].QID == 30) {

        } else {
            holder.txtOption.text = hHsOptionModel[position].ControlData

        }
        Coroutines.main {
            if (qid == 30) {
                val tblAnswer = repository.getQuestionControl(qid, endmes.PatId)
                if (tblAnswer != null) {
                    if (!TextUtils.isEmpty(tblAnswer.AnswerValue)) {
                        holder.txtOption.text = tblAnswer.AnswerValue
                    }
                }
                holder.txtOption.setTextColor(ContextCompat.getColor(mContext,
                    R.color.green))
            } else if (qid == 37) {
                val tblAnswer = repository.getQuestionControl(37, endmes.PatId)
                val dec = DecimalFormat("##.##")
                if (!TextUtils.isEmpty(tblAnswer.AnswerValue)) {
                    holder.txtOption.text = dec.format(tblAnswer.AnswerValue.toDouble())
                }
                holder.txtOption.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            } else {
                val tblAnswer = repository.getQuestionControl(qid, endmes.PatId)
                if (tblAnswer != null) {
                    val dat = tblAnswer.AnswerValue
                    if (controlData == dat) {
                        holder.txtOption.setTextColor(ContextCompat.getColor(mContext,
                            R.color.green))
                    } else {
                        holder.txtOption.setTextColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.purple_700
                            )
                        );
                    }
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return hHsOptionModel!!.size
    }

    inner class MainViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtOption: TextView = itemView.findViewById(R.id.txtOption)

    }
}