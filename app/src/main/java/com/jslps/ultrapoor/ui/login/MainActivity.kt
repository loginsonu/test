package com.jslps.ultrapoor.ui.login

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.databinding.ActivityMainBinding
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.hide
import com.jslps.ultrapoor.util.show
import com.jslps.ultrapoor.util.toast
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), LoginListner {

    var PERMISSION_ALL = 1
    private lateinit var appDatabase: AppDatabase

    var PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    var binding: ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        supportActionBar?.hide(); // hide the title bar
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)

        val respository = Repository(api, db, api2)
        val viewModelFactory = LoginViewModelFactory(respository)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        binding?.loginviewmodel = viewModel
        viewModel.authListner = this

        initt()

    }

    private fun initt() {
        // Initialize the Prefs class
        //initilization of backup data
        val preferences = getSharedPreferences("LogindetailUmang", MODE_PRIVATE)
        if (preferences.getString("user_id", "") != "") {
            binding?.loginviewmodel?.username = preferences.getString("user_id", "")
            binding?.loginviewmodel?.password = preferences.getString("password", "")
            rememberme_checkbox!!.isChecked = true
        }
        getVersion()
        appDatabase = Room.databaseBuilder(this, AppDatabase::class.java, AppConstant.dbName)
            .fallbackToDestructiveMigration().allowMainThreadQueries().build()

        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()

        askDynamicPermission()
        rememberme_checkbox!!.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            isRmChecked = isChecked
            remeberMe()
        })

    }

    private fun remeberMe() {

        var cancel = false
        var focusView: View? = null
        if (TextUtils.isEmpty(editTextEmail!!.text.toString())) {
            editTextEmail.error = getString(R.string.error_field_required)
            focusView = editTextEmail
            cancel = true
        }
        if (TextUtils.isEmpty(editTextPassword!!.text.toString())) {
            editTextPassword!!.error = getString(R.string.error_incorrect_password)
            focusView = editTextPassword
            cancel = true
        }
        if (cancel) {
            focusView!!.requestFocus()
        } else {
            if (isRmChecked) {
                val preferences = getSharedPreferences("LogindetailUmang", MODE_PRIVATE)
                val editor = preferences.edit()
                editor.putString("user_id", editTextEmail!!.text.toString())
                editor.putString("password", editTextPassword!!.text.toString())
                editor?.apply()
            } else {
                rememberme_checkbox!!.isChecked = false
            }
        }
    }

    private var isRmChecked = false

    private fun askDynamicPermission() {
        if (!hasPermissions(this, *PERMISSIONS)) {
            ActivityCompat
                .requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }
    }

    private fun hasPermissions(
        context: Context?,
        vararg permissions: String?,
    ): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null
        ) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context, permission!!) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //read phone state permission
                    toast("Permission Granted")
                    Prefs.putString("phone state permission", "true")
                } else {
                    toast("Permission Denied")
                }
            }
        }
    }

    override fun gotonext() {
        //go to next activity
        // progressbarid.hide()
        if (isRmChecked) {
            val preferences = getSharedPreferences("LogindetailUmang", MODE_PRIVATE)
            val editor = preferences.edit()
            editor.putString("user_id", editTextEmail!!.text.toString())
            editor.putString("password", editTextPassword!!.text.toString())
            editor?.apply()
        }
        val intent = Intent(this, Dashboard::class.java)
        startActivity(intent)
        finish()
    }

    override fun permissionmsg() {
        toast("All Permission not granted ,Please grant otherwise you can't use the app")
    }

    override fun onStarted() {
        cirLoginButton.visibility = View.GONE
        progressbarid.show()
    }

    override fun onFailure(s: String) {
        toast(s)
        progressbarid.hide()
        cirLoginButton.visibility = View.VISIBLE
    }

    private fun getVersion() {
        try {
            val pInfo = packageManager.getPackageInfo(packageName, 0)
            versionString = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }


    private var versionString: String? = null


}