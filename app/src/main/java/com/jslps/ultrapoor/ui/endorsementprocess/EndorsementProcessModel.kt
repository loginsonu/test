package com.jslps.ultrapoor.ui.endorsementprocess

import java.io.Serializable

data class EndorsementProcessModel(
    val didiName:String,
    val dadaName:String,
    val sector:String
): Serializable
