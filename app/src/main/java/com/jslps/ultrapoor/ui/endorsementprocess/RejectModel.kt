package com.jslps.ultrapoor.ui.endorsementprocess

import java.io.Serializable

data class RejectModel(
    val id: String,
    val reason: String
) : Serializable {
    override fun toString(): String = reason
}

