package com.jslps.ultrapoor.ui.login


import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.GridLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.gson.Gson
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.*
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.network.response.Village
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.*
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.dashboard.*


class Dashboard : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var dashboardListAdapter: DashboardListAdapter? = null
    var arrayListName: MutableList<String>? = null

    private lateinit var repository: Repository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide() // hide the title bar
        setContentView(R.layout.dashboard)
        val api = MyApi(networkConnectionInterceptor)
        val api2 = MyApiNew(networkConnectionInterceptor)
        val db = AppDatabase(this)
        repository = Repository(api, db, api2)
        val layoutManager = GridLayoutManager(this, 2)
        recylerview.layoutManager = layoutManager
        sppinerVillage.setTitle("..गाँव..")
        sppinerVillage.onItemSelectedListener = this
        arrayListName = ArrayList()
        arrayListName?.add("हेमलेट जोड़ें")
        arrayListName?.add("हेमलेट की सूची")

        getMaster()
        imageView6?.setOnClickListener {
            show_updatedata()
        }

        Coroutines.main {

            //GET USER PROFILE DATA FROM USER PROFILE TABLE
            repository.getUserProfileData().let {
                districttext.text = it.districtname_h
                blocktext.text = it.blockName_H
                Clustertext.text = it.clusterName_H
                userName.text = it.full_Name
            }
            val arrVillage = repository.getVilageList() as ArrayList<Village>
            if (arrVillage.size > 0) {
                val list: ArrayList<Village> = ArrayList()
                val master = Village("", "गाँव चुने", "", 0)
                list.add(master)
                for (districtname in arrVillage) {
                    list.add(districtname)
                }
                val searchmethod =
                    ArrayAdapter(this, android.R.layout.simple_spinner_item, list)
                searchmethod.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
                sppinerVillage!!.adapter = searchmethod
            } else {
                toast("No data found for village")
            }


        }
    }

    private fun getMaster() {
        dashboardListAdapter =
            DashboardListAdapter(this@Dashboard, arrayListName!!)
        recylerview.adapter = dashboardListAdapter

    }

    var selectedVillageList: Village? = null
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position == 0) {

        } else {
            selectedVillageList = parent?.getItemAtPosition(position) as Village
            AppConstant.villageName = selectedVillageList!!.VillageName_H
            AppConstant.villageId = selectedVillageList!!.Village_ID
        }


    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    var dialog: Dialog? = null
    private fun show_updatedata() {
        dialog = Dialog(this)
        val inflater =
            getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.popup_sync, null, false)
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setContentView(view)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(0))
        dialog?.show()
        val cardView: CardView = dialog?.findViewById(R.id.card_view)!!
        val cardView1: CardView = dialog?.findViewById(R.id.card_view1)!!
        cardView.setOnClickListener {
            dialog?.cancel()
            exportData()
        }
        cardView1.setOnClickListener {
            dialog?.cancel()
            downloadData()
        }
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    var dataCount = 0
    var uploadFailedGroupCode: ArrayList<String>? = null
    var tblMstHouseholdSurveyArrayList: ArrayList<tblAnswer>? = null
    var tblSurveyData1: ArrayList<AddHamlettbl>? = null

    private fun exportData() {
        arrayListMasterData = ArrayList()
//        progressbaridupload?.show()
        Coroutines.main {
            dataCount = 0
            uploadFailedGroupCode = ArrayList()
            val modelForSurveyId = repository.getModelForSurveyId()
            if (modelForSurveyId.isNotEmpty()) {
                for (i in modelForSurveyId) {
                    tblMstHouseholdSurveyArrayList = ArrayList()
                    tblMstHouseholdSurveyArrayList = repository.getAayAewnAdikarExported(
                        0,
                        1, i.exportedHammletId
                    ) as ArrayList<tblAnswer>?

                    tblSurveyData1 =
                        repository.getSurveyData1(0,
                            i.exportedHammletId) as ArrayList<AddHamlettbl>?
                    if (tblMstHouseholdSurveyArrayList!!.isNotEmpty()) {
                        for (j in tblMstHouseholdSurveyArrayList!!) {
                            j.isExported = 1
                        }
                    }
                    if (tblSurveyData1!!.isNotEmpty()) {
                        for (j in tblSurveyData1!!) {
                            j.isExported = 1
                        }
                    }

                    val masterModelForUpload = MasterModelForUpload(
                        tblSurveyData1,
                        tblMstHouseholdSurveyArrayList
                    )
                    val data = Gson().toJson(masterModelForUpload)
                    print("fcgufcuhfcgh" + data)
                    if (masterModelForUpload.tblAddHamletUltrapoor!!.isEmpty() && masterModelForUpload.tblTxnDataSaveUltrapoor!!.isEmpty()) {
//                        toast("There is no data to upload")
                        arrayListMasterData = ArrayList()
                    } else {
                        val groupJsonStringModel = GroupJsonStringModel(
                            i.exportedHammletId,
                            masterModelForUpload
                        )
                        arrayListMasterData!!.add(groupJsonStringModel)
                    }
                }
            }
            if (arrayListMasterData!!.size > 0) {
                val alert = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                alert.titleText = "Are you sure?"
                alert.contentText = "you can't edit after this"
                alert.confirmText = "OK"
                alert.setCustomImage(R.drawable.upaj_logo)
                alert.setConfirmClickListener { sDialog ->
                    //upload code here
                    DialogUtil.displayProgress(this,
                        "Please wait,data is loading\n 0/" + arrayListMasterData!!.size)
                    alert.dismiss()
                    Coroutines.main {
                        uploadFinalData()
                    }
                    //groupMeetingViewModel.dataUpload(finalJsonString, imei, androidId, user)
                }
                alert.setCancelable(true)
                alert.show()
            } else {
                toast("There is no data to upload")
            }
        }
    }

    var groupCode: String = ""
    private fun uploadFinalData() {
        if (dataCount < arrayListMasterData!!.size) {
            val jsoString = arrayListMasterData!!.get(dataCount).Json
            groupCode = arrayListMasterData!!.get(dataCount).surveyId
            dataCount++
            if (!uploadFailedGroupCode!!.contains(groupCode)) {
                val data = Gson().toJson(jsoString)
                val newData = data
                print("dfsds$data")
                Coroutines.main {
                    val getImageData = repository.getImageData(groupCode)
                    try {
                        val response = repository.uploadData(jsoString)
                        if (response.code == "200") {
                            val dataMessage = response.message
                            if (dataMessage == "Upload SucessFully") {
                                if (getImageData.isNotEmpty()) {
                                    for (i in getImageData) {
                                        try {

                                            val uploadImage = repository.uploadImageData(i.base64,
                                                    i.imageName,
                                                    i.patId,
                                                    i.hammletId,Prefs.getString("user",""))

                                            if (uploadImage.Data == "SUCCESS") {
                                                //updating data
                                                if (tblMstHouseholdSurveyArrayList!!.size > 0) {
                                                    repository.updateVprpAnswerDataModelSendDataExported(
                                                        tblMstHouseholdSurveyArrayList!!)
                                                }
                                                if (tblSurveyData1!!.size > 0) {
                                                    repository.updateVprpMeetingSavetblDataExported(
                                                        tblSurveyData1!!)
                                                }
                                                val deleteSurveyExport =
                                                    repository.deleteModelForSurveyId(groupCode)
                                                DialogUtil.stopProgressDisplay()
                                                Toast.makeText(
                                                    applicationContext,
                                                    "Data Upload SuccessFully",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        } catch (e: Exception) {
                                            DialogUtil.stopProgressDisplay()
                                            toast("Please try again")
                                        }catch (e: ApiException) {
                                            DialogUtil.stopProgressDisplay()
                                            toast(e.message!!)
                                        } catch (e: NoInternetException) {
                                            DialogUtil.stopProgressDisplay()
                                            toast(e.message!!)
                                        }

                                    }
                                }else{
                                    if (tblMstHouseholdSurveyArrayList!!.size > 0) {
                                        repository.updateVprpAnswerDataModelSendDataExported(
                                            tblMstHouseholdSurveyArrayList!!)
                                    }
                                    if (tblSurveyData1!!.size > 0) {
                                        repository.updateVprpMeetingSavetblDataExported(
                                            tblSurveyData1!!)
                                    }
                                    val deleteSurveyExport =
                                        repository.deleteModelForSurveyId(groupCode)
                                    DialogUtil.stopProgressDisplay()
                                    Toast.makeText(
                                        applicationContext,
                                        "Data Upload SuccessFully",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }


                            } else {
                                DialogUtil.stopProgressDisplay()
                                toast(response.message)
                            }
                        } else {
                            DialogUtil.stopProgressDisplay()
                            toast("Please try again")
                        }
                    } catch (e: ApiException) {
                        DialogUtil.stopProgressDisplay()
                        toast(e.message!!)
                    } catch (e: NoInternetException) {
                        DialogUtil.stopProgressDisplay()
                        toast(e.message!!)
                    }
                }
            }
        }


    }

    var arrayListMasterData: ArrayList<GroupJsonStringModel>? = null
    private fun downloadData() {
        val alert = SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
        alert.contentText = "क्या आप मास्टर डाटा को सिंक करना चाहते हैं ?"
        alert.confirmText = "हाँ"
        alert.cancelText = "नहीं"

        alert.setCustomImage(R.drawable.upaj_logo)
        alert.setConfirmClickListener { sDialog ->
            sDialog.dismissWithAnimation()
            val userId = Prefs.getString("user", "").toString()
            val password = Prefs.getString("password", "").toString()
            DialogUtil.displayProgress(this)
            Coroutines.main {
                val authResponse = repository.userLogin(userId, password)
                Coroutines.mainIo {
                    val saveVillageList =
                        repository.saveVillageList(authResponse.data.VillageList)
                    val saveActivityMaster =
                        repository.saveActivityMaster(authResponse.data.ActivityMaster)
                    val saveActivityListMaster =
                        repository.saveActivityListMaster(authResponse.data.ActivityListMaster)
                    val saveControlDataMaster =
                        repository.saveControlDataMaster(authResponse.data.ControlDataMaster)
                    val saveControlNameMaster =
                        repository.saveControlNameMaster(authResponse.data.ControlNameMaster)
                    val saveQuestionTypeMaster =
                        repository.saveQuestionTypeMaster(authResponse.data.QuestionTypeMaster)
                    val saveUserProfileData =
                        repository.saveUserProfileData(authResponse.data.UserProfileData)
                    val saveDependencyMaster =
                        repository.saveDependencyMaster(authResponse.data.DependencyMaster)
                    val saveVoList =
                        repository.saveVoList(authResponse.data.VoList)
                    toast("Data download successfully")
                    DialogUtil.stopProgressDisplay()
                }
            }

        }
        alert.setCancelClickListener { sDialog ->
            sDialog.dismissWithAnimation()

        }
        alert.setCancelable(true)
        alert.show()

    }
}
