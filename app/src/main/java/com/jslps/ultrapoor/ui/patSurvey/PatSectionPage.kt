package com.jslps.ultrapoor.ui.patSurvey

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import kotlinx.android.synthetic.main.question_section_activity.*
import kotlinx.android.synthetic.main.tool_bar.*
import java.util.*


class PatSectionPage : AppCompatActivity() {
    private lateinit var repository: Repository
    var uuidRow: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide()
        setContentView(R.layout.pat_section_activity)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)

        repository = Repository(api, db, api2)

        uuidRow = if (savedInstanceState == null) {
            val extras = intent.extras
            extras?.getString("uuidGet")
        } else {
            savedInstanceState.getSerializable("uuidGet") as String?
        }
        if (TextUtils.isEmpty(uuidRow)) {
            AppConstant.patId = UUID.randomUUID().toString()
        } else {
            AppConstant.patId = uuidRow.toString()
        }
        Coroutines.main {

            //GET USER PROFILE DATA FROM USER PROFILE TABLE
            repository.getUserProfileData().let {
                districttext.text = it.districtname_h
                blocktext.text = it.blockName_H
                Clustertext.text = it.clusterName_H
                Villegetext.text = AppConstant.villageName
                Hamlettext.text = AppConstant.hamletName
                txtHeader.text = "अनुभाग सूची"
            }


        }

    }

    override fun onResume() {
        super.onResume()
        Coroutines.main {
            val activityList = repository.getActivityListMasterPat(1)
            val adpSection =
                PatSectionAdapter(this, activityList, uuidRow, repository)
            recycSection.adapter = adpSection
            recycSection.visibility = View.VISIBLE
        }
    }
}