package com.jslps.ultrapoor.ui.patList

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.newtblAnswer
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.ui.patSurvey.PatSectionPage
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.DialogUtil
import com.jslps.ultrapoor.util.dateFormate
import com.jslps.ultrapoor.util.toast


class PatListAdapter(
    private val mContext: Context,
    private val getListDataPat: List<newtblAnswer>,
    private val repository: Repository,
) : RecyclerView.Adapter<PatListAdapter.MailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MailViewHolder {
        val view = LayoutInflater.from(mContext).inflate(
            R.layout.pat_list_row,
            parent, false
        )
        return MailViewHolder(view)
    }

    override fun onBindViewHolder(holder: MailViewHolder, position: Int) {
        holder.date.text = "Date : " + dateFormate(getListDataPat[position].CreatedOn)
        Coroutines.main {
            val data = repository.getDataForQuestion(getListDataPat[position].HamletID,
                getListDataPat[position].CatID,
                getListDataPat[position].PatId,
                4)
            val data1 = repository.getDataForQuestion(getListDataPat[position].HamletID,
                getListDataPat[position].CatID,
                getListDataPat[position].PatId,
                5)
            if (data.isNotEmpty()) {
                holder.Name.text = "Didi Name : " + data[0].AnswerValue
                if (data[0].AnswerValue != "")
                    holder.mIcon.text = data[0].AnswerValue.substring(0, 1)
            }
            if (data1.isNotEmpty()) {
                holder.fatherName.text = "Pati Name : " + data1[0].AnswerValue
            }
            if (getListDataPat[position].isExported == 1) {
                holder.exportedIcon.visibility = View.VISIBLE
                holder.delete_button.visibility = View.GONE
            } else {
                holder.exportedIcon.visibility = View.GONE
                holder.delete_button.visibility = View.VISIBLE
            }

            if (getListDataPat[position].ultrapoor == 1) {
                holder.layoutdelete.visibility = View.GONE
            } else {
                holder.layoutdelete.visibility = View.VISIBLE
            }

        }
        holder.delete_button.setOnClickListener {
            if (getListDataPat[position].ultrapoor == 0) {
                val alert = SweetAlertDialog(mContext, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                alert.titleText = "मिटाना"
                alert.contentText = "क्या आप निश्चित रूप से पैट रिकॉर्ड हटाना चाहते हैं ?"
                alert.confirmText = "ठीक है"
                alert.setCustomImage(R.drawable.upaj_logo)
                alert.setConfirmClickListener { sDialog ->
                    //write delete code here
                    DialogUtil.displayProgressWithMessage(
                        mContext as Activity,
                        "कृपया प्रतीक्षा करें पैट डेटा हटा रहा है."
                    )
                    Coroutines.main {
                        val result = repository.deletePatRecord(getListDataPat[position].PatId)
                        mContext.toast("पैट रिकॉर्ड सफलतापूर्वक हटाया गया।")
                        DialogUtil.stopProgressDisplay()
                        mContext.finish()

                    }
                    sDialog.dismissWithAnimation()
                }
                alert.setCancelable(true)
                alert.show()
            } else {
                mContext.toast("इस सर्वेक्षण को अति गरीब श्रेणी में नहीं माना जाता है")
            }
        }

        holder.mLayout.setOnClickListener {
            if (getListDataPat[position].ultrapoor == 0) {
                val intent = Intent(mContext, PatSectionPage::class.java)
                intent.putExtra("uuidGet", getListDataPat[position].PatId)
                mContext.startActivity(intent)
            } else {
                mContext.toast("इस सर्वेक्षण को अति गरीब श्रेणी में नहीं माना जाता है")
            }
        }

    }


    override fun getItemCount(): Int {
        return getListDataPat.size ?: 0
    }

    inner class MailViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var mIcon: TextView = itemView.findViewById(R.id.tvIcon)
        var delete_button: ImageView = itemView.findViewById(R.id.delete_button)
        var exportedIcon: ImageView = itemView.findViewById(R.id.imageView3)
        var Name: TextView = itemView.findViewById(R.id.Name)
        var fatherName: TextView = itemView.findViewById(R.id.fatherName)
        var date: TextView = itemView.findViewById(R.id.date)
        var mLayout: LinearLayout = itemView.findViewById(R.id.layout)
        var layoutdelete: LinearLayout = itemView.findViewById(R.id.layoutdelete)

    }
}