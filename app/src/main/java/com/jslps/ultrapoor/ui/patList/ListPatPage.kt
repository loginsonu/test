package com.jslps.ultrapoor.ui.patList


import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.ui.patSurvey.PatSectionPage
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import kotlinx.android.synthetic.main.list_pat_activity.*

class ListPatPage : AppCompatActivity() {

    private lateinit var repository: Repository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide() // hide the title bar
        setContentView(R.layout.list_pat_activity)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)

        repository = Repository(api, db, api2)
        val layoutManager = LinearLayoutManager(this)
        recyc.layoutManager = layoutManager
        textView3?.text = "पैट सर्वेक्षण"

        Coroutines.main {
            //GET USER PROFILE DATA FROM USER PROFILE TABLE
            repository.getUserProfileData().let {
                districttext.text = it.districtname_h
                blocktext.text = it.blockName_H
                Clustertext.text = it.clusterName_H
                Villegetext.text = AppConstant.villageName
                Hamlettext.text = AppConstant.hamletName
            }
        }

//        getMaster()
        addnewpat?.setOnClickListener {
            val intent = Intent(this, PatSectionPage::class.java)
            AppConstant.patId = ""
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        getMaster()
    }

    private fun getMaster() {
        Coroutines.main {
            val getListDataPat = repository.getPatList(AppConstant.HamletId)
            val hamletListAdapter =
                PatListAdapter(this, getListDataPat, repository)
            recyc.adapter = hamletListAdapter
            recyc.visibility = View.VISIBLE
        }

    }
}
