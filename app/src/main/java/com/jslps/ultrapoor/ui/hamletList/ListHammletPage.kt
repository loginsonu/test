package com.jslps.ultrapoor.ui.hamletList


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.jslps.ultrapoor.R
import com.jslps.ultrapoor.data.db.AddHamlettbl
import com.jslps.ultrapoor.data.db.AppDatabase
import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.NetworkConnectionInterceptor
import com.jslps.ultrapoor.data.network.response.UserProfileData
import com.jslps.ultrapoor.data.repository.Repository
import com.jslps.ultrapoor.util.AppConstant
import com.jslps.ultrapoor.util.Coroutines
import com.jslps.ultrapoor.util.toast
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.add_hammlet.Clustertext
import kotlinx.android.synthetic.main.add_hammlet.blocktext
import kotlinx.android.synthetic.main.add_hammlet.districttext
import kotlinx.android.synthetic.main.hammlet_list_display_activity.*
import java.util.*


class ListHammletPage : AppCompatActivity() {

    private lateinit var repository: Repository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        supportActionBar?.hide() // hide the title bar
        setContentView(R.layout.hammlet_list_display_activity)
        val api = MyApi(networkConnectionInterceptor)
        val db = AppDatabase(this)
        val api2 = MyApiNew(networkConnectionInterceptor)
        repository = Repository(api, db, api2)
        val layoutManager = LinearLayoutManager(this);
        recyc.layoutManager = layoutManager
        getMaster()
        addnewhamlet.setOnClickListener {
            val dialog = BottomSheetDialog(this)
            val view1 = LayoutInflater.from(this).inflate(
                R.layout.bottom_sheet_addhammlet, null)
            val price: TextInputEditText = view1.findViewById(R.id.price)
            val addTolaMember: AppCompatButton = view1.findViewById(R.id.addTolaMember)
            addTolaMember.setOnClickListener {
                if (price.text.toString() == "") {
                    toast("कृपया टोला नाम दर्ज करें")
                } else {
                    Coroutines.main {
                        val arrayListSave: ArrayList<AddHamlettbl> = ArrayList()
                        val addHamlettbl = AddHamlettbl(
                            userprofileData!!.district_ID,
                            userprofileData!!.block_ID,
                            userprofileData!!.cluster_ID,
                            AppConstant.villageId,
                            UUID.randomUUID().toString(),
                            price.text.toString(),
                            0,
                            Date(),
                            Prefs.getString("user", "")
                        )
                        arrayListSave.add(addHamlettbl)
                        val saveAddHamlettbl = repository.saveHamletList(arrayListSave)
                        toast(getString(R.string.save))
                        dialog.dismiss()
                        getMaster()


                    }
                }
            }
            val btnClose = view1.findViewById<ImageView>(R.id.cancelQuestion)
            btnClose.setOnClickListener {
                dialog.dismiss()
            }
            dialog.setContentView(view1)
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    var userprofileData: UserProfileData? = null
    private fun getMaster() {
        Coroutines.main {
            //GET USER PROFILE DATA FROM USER PROFILE TABLE
            userprofileData = repository.getUserProfileData()
            repository.getUserProfileData().let {
                districttext.text = it.districtname_h
                blocktext.text = it.blockName_H
                Clustertext.text = it.clusterName_H

            }
            val getHamletList = repository.getHmaletList(AppConstant.villageId) as ArrayList<AddHamlettbl>
            val hamletListAdapter =
                HamletDisplayListAdapter(this, getHamletList, repository)
            recyc.adapter = hamletListAdapter
            recyc.visibility = View.VISIBLE
        }

    }
}
