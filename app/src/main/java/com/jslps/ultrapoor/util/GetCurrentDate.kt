package com.jslps.ultrapoor.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class GetCurrentDate {
    val date: String
        get() {
            val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")
            val date = Date()
            return dateFormat.format(date)
        }
}