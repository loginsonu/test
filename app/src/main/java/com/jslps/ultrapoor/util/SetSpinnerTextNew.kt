package com.jslps.ultrapoor.util

import android.widget.Spinner

class SetSpinnerTextNew(spin: Spinner, text: String, b: Boolean) {
    init {
        for (i in 0 until spin.adapter.count) {
            if (spin.adapter.getItem(i).toString() == text) {
                 spin.setSelection(i,b)

            }
        }
    }
}