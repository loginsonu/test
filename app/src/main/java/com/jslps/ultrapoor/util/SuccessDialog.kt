package com.jslps.ultrapoor.util

import android.app.Activity
import android.app.Dialog
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.jslps.ultrapoor.R

class SuccessDialog {

    public fun showDialog(activity:Activity,message: String) {
        val dialog = Dialog(activity, R.style.NewDialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_success_dailog)
        dialog.getWindow()?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val txtMessage = dialog.findViewById(R.id.txtMessage) as TextView
        txtMessage.text = message
        val btnOk = dialog.findViewById(R.id.btnOk) as AppCompatButton
        val btnCancel = dialog.findViewById(R.id.btnCancel) as AppCompatButton

        btnOk.setOnClickListener {
            dialog.dismiss()
            activity.finish()
        }
        btnCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

    }
}