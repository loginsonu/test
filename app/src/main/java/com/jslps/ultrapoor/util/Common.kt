package com.jslps.ultrapoor.util


import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.jslps.ultrapoor.R
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*




fun roundOffDecimal(number: Float): Float? {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.FLOOR
    return df.format(number).toFloat()
}

fun dateFormate(date: Date): String {
    val formatter5 = SimpleDateFormat("dd MMM yyyy")
    val formats1 = formatter5.format(date)
    return formats1

}

fun dateFormateConvert(date: String): String {
    val strDate = date
    var dateFormat = SimpleDateFormat("dd MMM yyyy")
    try {
        val varDate = dateFormat.parse(strDate)
        dateFormat = SimpleDateFormat("dd-MM-yyyy")
        println("Date :" + dateFormat.format(varDate))
        return dateFormat.format(varDate)
    } catch (e: Exception) {
        e.printStackTrace()
        return ""
    }


}

fun getNextMonth(month: String): String {
    var result: String? = null
    when (month) {
        "January" -> result = "February"
        "February" -> result = "March"
        "March" -> result = "April"
        "April" -> result = "May"
        "May" -> result = "June"
        "June" -> result = "July"
        "July" -> result = "August"
        "August" -> result = "September"
        "September" -> result = "October"
        "October" -> result = "November"
        "November" -> result = "December"
        "December" -> result = "January"

    }
    return result!!
}

fun MonthNo(month: String): String {
    var result: String? = null
    when (month) {
        "January" -> result = "01"
        "February" -> result = "02"
        "March" -> result = "03"
        "April" -> result = "04"
        "May" -> result = "05"
        "June" -> result = "06"
        "July" -> result = "07"
        "August" -> result = "08"
        "September" -> result = "09"
        "October" -> result = "10"
        "November" -> result = "11"
        "December" -> result = "12"

    }
    return result!!
}

fun changeLang(context: Context, lang: String?) {
    val config = context.resources.configuration
    val locale = Locale(lang!!)
    Locale.setDefault(locale)
    config.locale = locale
    context.resources.updateConfiguration(
        config,
        context.resources.displayMetrics
    )

    //updateText();

}

fun buttonEnabledOrNot(button: Button, isFlag: Boolean) {
    if (isFlag) {
        button.isEnabled = true
        button.isClickable = true
        button.alpha = 1f
    } else {
        button.isEnabled = false
        button.isClickable = false
        button.alpha = 0.6f
    }
}
fun buttonEnabledOrNotImage(button: LinearLayout, isFlag: Boolean) {
    if (isFlag) {
        button.isEnabled = true
        button.isClickable = true
        button.alpha = 1f
    } else {
        button.isEnabled = false
        button.isClickable = false
        button.alpha = 0.6f
    }
}
fun buttonEnabledOrNotEdittext(button: EditText, isFlag: Boolean) {
    if (isFlag) {
        button.isEnabled = true
        button.isClickable = true
        button.alpha = 1f
    } else {
        button.isEnabled = false
        button.isClickable = false
        button.alpha = 0.6f
    }
}





fun convertStringToDate(date: String): Date? {
    val dtStart = date
    val format = SimpleDateFormat("dd-MM-yyyy")
    try {

        val dateObject = format.parse(dtStart)
        return dateObject
        println(dateObject)
    } catch (e: ParseException) {
        e.printStackTrace()
        return null
    }
}

fun dateToString(dateRec: Date): String? {
    println("Date Format with date : $dateRec")
    val date = dateRec
    val formatter = SimpleDateFormat("dd-MM-yyyy")
    val strDate = formatter.format(date)
    println("Date Format with dd-MM-yyyy : $strDate")
    return strDate
}

