package com.jslps.ultrapoor.util

 import com.jslps.ultrapoor.data.network.response.AnswerValueFillNew
import com.jslps.ultrapoor.ui.section.AnswerModel


class AppConstant {
    companion object {
        var patId=""
        var HamletId=""

        //  http://nodejs.swalekha.in:5000/ultrapoor/LoginUltraPoor?userid=1&password=1
        const val domain = "http://nodejs.swalekha.in:5000/ultrapoor/"
//        const val domain = "http://192.168.2.74:5000/ultrapoor/"
        const val domain1 = "http://upaj.swalekha.in/webservice1.asmx/"
        const val loginMethod = "LoginUltraPoor"
        const val dbName = "UltrapoorApp.db"
        const val uploadMethod = "uploadUltraPoorTxnData"
        const val uploadImageMethod = "UPAJDocuments"
        var userNameApp = ""
        var passWordApp = ""
        var childFormCount = 0
        var awcUUId = ""

        var hashMap = HashMap<String, AnswerModel>()
        var hasMapMultipleChoice = HashMap<String, HashSet<String>>()

        var hamletName = ""
        var vocode = ""
        var hamletDetails = "PROFILE DETAILS"
        var villageName = "गाँव चुने"
        var villageId = "0"
        var dataSaveList:ArrayList<AnswerValueFillNew>?=ArrayList()

    }
}