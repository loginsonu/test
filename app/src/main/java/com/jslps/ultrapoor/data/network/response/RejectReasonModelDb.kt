package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

const val REJECT_LIST_ID = 0

@Entity(indices = [Index(value = ["RID"], unique = true)])
data class RejectReasonModelDb(
    @SerializedName("RID") var RID: Int? = null,
    @SerializedName("Reason_for_Rejection") var ReasonForRejection: String? = null,
    @SerializedName("Status") var Status: String? = null,
) : Serializable {
    override fun toString(): String = ReasonForRejection.toString()

    @PrimaryKey(autoGenerate = true)
    var rejListId: Int = REJECT_LIST_ID
}