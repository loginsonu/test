package com.jslps.ultrapoor.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jslps.ultrapoor.util.AppConstant


import com.jslps.ultrapoor.data.network.response.*

import com.jslps.ultrapoor.util.Converter

@Database(
    entities = [User::class, AddHamlettbl::class, ActivityListMaster::class, ActivityMaster::class,
        ControlDataMaster::class, ControlNameMaster::class, QuestionTypeMaster::class, UserProfileData::class, Village::class,
        tblAnswer::class, DependencyMaster::class, VoList::class, ModelForSurveyId::class, RejectReasonModelDb::class,
        tblUploadImage::class,newtblAnswer::class],
    version = 2,
    exportSchema = false,

    )
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getDao(): AppDao

    companion object {
        val MIGRATION_1_2 = object : Migration(1, 2){
             override fun migrate(database: SupportSQLiteDatabase) {
                 database.execSQL("CREATE TABLE IF NOT EXISTS newtblAnswer(" +
                         "DistrictID INTEGER NOT NULL," +

                         " BlockID INTEGER NOT NULL," +

                         " ClusterID INTEGER NOT NULL, " +

                         "VillageID INTEGER NOT NULL," +

                         " HamletID TEXT NOT NULL," +

                         " vocode TEXT NOT NULL," +
                         " PatId TEXT NOT NULL," +
                         " SectionID INTEGER NOT NULL," +
                         " CatID INTEGER NOT NULL," +
                         " QID INTEGER NOT NULL," +
                         " AnswerValue TEXT NOT NULL," +
                         " ControlTypeID INTEGER NOT NULL," +
                         " AnswerID TEXT NOT NULL," +
                         " isExported INTEGER NOT NULL," +
                         " isUpdated INTEGER NOT NULL," +
                         " ultrapoor INTEGER NOT NULL," +
                         " CreatedOn DATETIME NOT NULL," +
                         " CreatedBy TEXT NOT NULL," +
                         " uuid TEXT NOT NULL," +
                         " statusAcepptandRej INTEGER NOT NULL," +
                         " reasonText TEXT NOT NULL," +
                         " reasonCode TEXT NOT NULL," +
                         " Score TEXT NOT NULL," +
                         " questionToBeAdd TEXT ," +
                         " answer INTEGER PRIMARY KEY AUTOINCREMENT," +
                         " CONSTRAINT supplier_name_unique UNIQUE(HamletID,PatId,QID))")

                 database.execSQL("INSERT INTO newtblAnswer SELECT * FROM tblAnswer ")
//
//
//                 database.execSQL("DROP TABLE tblAnswer")
//                 // rename new table
//                 database.execSQL("ALTER TABLE newtblAnswer RENAME TO tblAnswer")

//                 SELECT * FROM table1

 //                database.execSQL("INSERT INTO newtblAnswer(DistrictID," +
 //
 //                        " BlockID," +
 //
 //                        " `ClusterID` , " +
 //
 //                        "`VillageID` ," +
 //
 //                        " `HamletID` ," +
 //
 //                        " `vocode` ," +
 //                        " `PatId` ," +
 //                        " `SectionID` ," +
 //                        " `CatID` ," +
 //                        " `QID` ," +
 //                        " `AnswerValue` ," +
 //                        " `ControlTypeID` ," +
 //                        " `AnswerID` ," +
 //                        " `isExported`," +
 //                        " `isUpdated`," +
 //                        " `ultrapoor` ," +
 //                        " `CreatedOn`," +
 //                        " `CreatedBy` ," +
 //                        " `uuid` ," +
 //                        " `statusAcepptandRej` ," +
 //                        " `reasonText` ," +
 //                        " `reasonCode` ," +
 //                        " `Score` ," +
 //                        " `questionToBeAdd` )" +
 //
 //                        " SELECT DistrictID,BlockID,ClusterID,VillageID,HamletID,vocode," +
 //                        "PatId,SectionID,CatID,QID,AnswerValue,ControlTypeID,AnswerID,isExported,isUpdated," +
 //                        "ultrapoor,CreatedOn,CreatedBy,uuid,statusAcepptandRej,reasonText,reasonCode,Score,questionToBeAdd from tblAnswer")
 //
 //                database.execSQL("DROP TABLE tblAnswer")
 //
 //                database.execSQL("ALTER TABLE newtblAnswer RENAME TO tblAnswer")


 //                database.execSQL("ALTER TABLE tblAnswer   CONSTRAINT  uniquekeyid UNIQUE (HamletID,PatId,QID)")

             }
         }
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            AppConstant.dbName
        ).addMigrations(MIGRATION_1_2).build()

//        private fun buildDatabase(context: Context) = Room.databaseBuilder(
//            context.applicationContext,
//            AppDatabase::class.java,
//            AppConstant.dbName
//        ).build()
    }
}