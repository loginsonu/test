package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable


const val CONTROL_NAME_MASTER_ID = 0

@Entity(indices = [Index(value = ["COID"], unique = true)])
data class ControlNameMaster(
    val COID: Int,
    val ControlName: String
): Serializable {
    @PrimaryKey(autoGenerate = true)
    var controlNameMasterId: Int = CONTROL_NAME_MASTER_ID
}