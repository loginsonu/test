package com.jslps.ultrapoor.data.network

import com.google.gson.GsonBuilder
import com.jslps.ultrapoor.data.network.response.UploadResponse
import com.jslps.ultrapoor.util.AppConstant




import com.jslps.ultrapoor.util.MyJsonConverter2
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface MyApiNew {


    @FormUrlEncoded
    @POST(AppConstant.uploadImageMethod)
    suspend fun uploadImage(
        @Field("imageBase64") imageBase64: String,
        @Field("imagename") fileName: String,
        @Field("patid") patid: String,
        @Field("hammletid") hammletid: String,
        @Field("createdBy") createdBy: String,
    ): Response<UploadResponse>


    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor,
        ): MyApiNew {
            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .build()


            val gson = GsonBuilder().setDateFormat("dd-MM-yyyy").create()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(AppConstant.domain1)
                .addConverterFactory(MyJsonConverter2.create(gson))
                .build()
                .create(MyApiNew::class.java)


        }
    }
}