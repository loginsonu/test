package com.jslps.ultrapoor.data.network

import com.google.gson.GsonBuilder
import com.jslps.ultrapoor.data.db.MasterModelForUpload
import com.jslps.ultrapoor.util.AppConstant

import com.jslps.ultrapoor.data.network.response.Data
import com.jslps.ultrapoor.data.network.response.LoginResponse


import com.jslps.ultrapoor.util.MyJsonConverter
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface MyApi {


    //@FormUrlEncoded
    @GET(AppConstant.loginMethod)
    suspend fun userLogin(
        @Query("userid") email: String,
        @Query("password") password: String
    ): Response<LoginResponse<Data>>

    @Headers("Content-Type: application/json")
    @POST(AppConstant.uploadMethod)
    suspend fun uploadUltraPoorData(@Body body: MasterModelForUpload):Response<LoginResponse<Data>>


    companion object{
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): MyApi {
            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .build()



            val gson = GsonBuilder().setDateFormat("dd-MM-yyyy").create()

            return Retrofit.Builder()
                    .client(okkHttpclient)
                    .baseUrl(AppConstant.domain)
                    .addConverterFactory(MyJsonConverter.create(gson))
                    .build()
                    .create(MyApi::class.java)


        }
    }
}