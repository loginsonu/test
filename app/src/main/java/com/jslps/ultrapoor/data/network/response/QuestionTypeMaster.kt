package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable


const val QUESTION_TYPE_MASTER_ID = 0
@Entity(indices = [Index(value = ["Qid"], unique = true)])
data class QuestionTypeMaster(
    val CatID: Int,
    val ControlID: Int,
    val Header: String,
    val Dependency: String,
    val Link: String,
    val QOrder: String,
    val Qid: Int,
    val Qname: String,
    val ControlName: String?,
    val Sort: Double,
    val Staus: Int,
    val SectionID : String
): Serializable {
    @PrimaryKey(autoGenerate = true)
    var questionTypeMasterId: Int = QUESTION_TYPE_MASTER_ID
}