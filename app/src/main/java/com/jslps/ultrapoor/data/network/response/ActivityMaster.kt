package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable


const val MASTER_ID = 0
@Entity(indices = [Index(value = ["ID"], unique = true)])
data class ActivityMaster(
    val ID :Int,
    val  ActivityName:String,
    val ParentID: Int,
    val Active: Boolean,
    val CreatedBy :String,
    val CreatedOn :String,
    val Level: Int

): Serializable {
    @PrimaryKey(autoGenerate = true)
    var masterId: Int = MASTER_ID
}