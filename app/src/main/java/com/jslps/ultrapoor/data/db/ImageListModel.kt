package com.jslps.ultrapoor.data.db

import android.graphics.Bitmap

data class ImageListModel(
    val imageBitmap:Bitmap?,
    val position:Int?,
    val imageName:String?,

)