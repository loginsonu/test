package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable


const val VILLAGE_LIST_ID = 0

@Entity(indices = [Index(value = ["Village_ID"], unique = true)])
data class Village(
    val VillageName: String,
    val VillageName_H: String,
    val Village_ID: String,
    val userprofile_ID: Int,
) : Serializable {
    override fun toString(): String = VillageName_H

    @PrimaryKey(autoGenerate = true)
    var villageListId: Int = VILLAGE_LIST_ID
}