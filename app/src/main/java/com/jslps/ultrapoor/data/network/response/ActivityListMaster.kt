package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable


const val MASTER_LIST_ID = 0

@Entity(indices = [Index(value = ["ID"], unique = true)])
data class ActivityListMaster(

    val ID: Int,
    val ActivityName: String,
    val Active: Boolean,
    val CreatedBy: String,
    val CreatedOn: String,
    val SequNo: Int,
    val CatID: Int

) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var marterListId: Int = MASTER_LIST_ID
}
