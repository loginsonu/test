package com.jslps.ultrapoor.data.repository


import com.jslps.ultrapoor.data.db.*


import com.jslps.ultrapoor.data.network.MyApi
import com.jslps.ultrapoor.data.network.MyApiNew
import com.jslps.ultrapoor.data.network.SafeApiRequest
import com.jslps.ultrapoor.data.network.response.*


class Repository(
    private val api: MyApi,
    private val db: AppDatabase,
    private val api2: MyApiNew,
) : SafeApiRequest() {

    //user login service
    suspend fun userLogin(email: String, password: String): LoginResponse<Data> {
        return apiRequest { api.userLogin(email, password) }
    }
     // upload service
    suspend fun uploadData(sData: MasterModelForUpload): LoginResponse<Data> {
        return apiRequest { api.uploadUltraPoorData(sData) }
    }

    //get user
    suspend fun getUser(userId: String, password: String) =
        db.getDao().getUser(userId, password)

    //get user
    suspend fun getHmaletList(villageId: String) =
        db.getDao().getHmaletList(villageId)


    //saving user
    suspend fun saveUser(user: User) = db.getDao().addUser(user)

    //saving user
    suspend fun saveHamletList(user: List<AddHamlettbl>) = db.getDao().saveHamletList(user)

    suspend fun saveVoList(user: List<VoList>) = db.getDao().saveVoList(user)
    suspend fun saveRejectList(user: List<RejectReasonModelDb>) =
        db.getDao().saveRejectreasonList(user)

    //SAVE SECTION ANSWER
    suspend fun saveSectionAnswer(tblAnswer: List<tblAnswer>) =
        db.getDao().saveSectionAnswer(tblAnswer)


    suspend fun isRowIsExist(sectionId: Int) =
        db.getDao().isRowIsExist(sectionId)


    suspend fun updateAnswer(answerValue: String, sectionID: String) =
        db.getDao().updateAnswer(answerValue, sectionID)


    //SAVE VILLAGE LIST
    suspend fun saveVillageList(villageList: List<Village>) =
        db.getDao().saveVillageList(villageList)

    //SAVE ACTIVITY MASTER
    suspend fun saveActivityMaster(activityMaster: List<ActivityMaster>) =
        db.getDao().saveActivityMaster(activityMaster)

    //SAVE ACTIVITY MASTER LIST
    suspend fun saveActivityListMaster(activityListMaster: List<ActivityListMaster>) =
        db.getDao().saveActivityListMaster(activityListMaster)

    //SAVE CONTROL MASTER
    suspend fun saveControlDataMaster(controlDataMaster: List<ControlDataMaster>) =
        db.getDao().saveControlDataMaster(controlDataMaster)

    //SAVE CONTROL NAME MASTER
    suspend fun saveControlNameMaster(controlNameMaster: List<ControlNameMaster>) =
        db.getDao().saveControlNameMaster(controlNameMaster)

    //SAVE QUESTION TYPE MASTER
    suspend fun saveQuestionTypeMaster(questionTypeMaster: List<QuestionTypeMaster>) =
        db.getDao().saveQuestionTypeMaster(questionTypeMaster)

    //SAVE QUESTION TYPE MASTER
    suspend fun saveUserProfileData(userProfileData: UserProfileData) =
        db.getDao().saveUserProfileData(userProfileData)

    suspend fun saveDependencyMaster(userProfileData: List<DependencyMaster>) =
        db.getDao().saveDependencyMaster(userProfileData)

    suspend fun getQuestionSection(catId: Int) =
        db.getDao().getQuestionSection(catId)

    suspend fun getVilageList() =
        db.getDao().getVilageList()


    /*suspend fun getQuestionList(catId: Int, status: Int, id: String) =
        db.getDao().getQuestionList(catId, status, id)
*/
    suspend fun getQuestionList(catId: Int, id: String) =
        db.getDao().getQuestionList(catId, id)

    suspend fun getQuestionListHH(catId: Int) =
        db.getDao().getQuestionListHH(catId)


    suspend fun getControlMasterData(status: Int, qid: Int, coid: Int) =
        db.getDao().getControlMasterData(status, qid, coid)


    suspend fun getAllQuestionList() =
        db.getDao().getAllQuestionList()

    suspend fun getActivityListMasterPat(catId: Int) =
        db.getDao().getActivityListMasterPat(catId)

    suspend fun getVoList() =
        db.getDao().getVoList()

    suspend fun getRejectReasonList() =
        db.getDao().getRejectReasonList()

    suspend fun getQuestionListPat(catId: Int, sectionId: String) =
        db.getDao().getQuestionListPat(catId, sectionId)

//    suspend fun getControlDataMaster(catID: String, sectionId: String) =
//       db.getDao().getControlDataMaster(catID,sectionId)


    suspend fun getUserProfileData() = db.getDao().getUserProfileData()

    suspend fun getVillageId(userProfileId: Int) = db.getDao().getVillageId(userProfileId)

    suspend fun getControlDataMaster(catID: String, sectionId: String) =
        db.getDao().getControlDataMaster(catID, sectionId)

    suspend fun getControlDataMasterNew(catID: String) =
        db.getDao().getControlDataMasterNew(catID)

    suspend fun getDependencyData(Qid: Int, controlId: Int) =
        db.getDao().getDependencyData(Qid, controlId)

    suspend fun getPatList(hamletId: String): List<newtblAnswer> =
        db.getDao().getPatSurveyList(hamletId)

    suspend fun getPatListApprove(hamletId: String): List<tblAnswer> =
        db.getDao().getPatListApprove(hamletId)
    suspend fun getPatListApproveSum(hamletId: String,patId:String): Int =
        db.getDao().getPatListApproveSum(hamletId,patId)

    suspend fun getPatListAfterApprove(hamletId: String): List<tblAnswer> =
        db.getDao().getPatListAfterApprove(hamletId)

    // get Save Data
    suspend fun getSaveDataBySectionId(
        hamletId: String,
        catId: Int,
        sectionId: Int,
        patId: String,
    ) =
        db.getDao().getSaveDataBySectionId(hamletId, catId, sectionId, patId)

    suspend fun getSaveDataBySectionIdtest(hamletId: String, catId: Int) =
        db.getDao().getSaveDataBySectionIdTest(hamletId, catId)

    suspend fun getSaveDataBySectionIdtestnew(hamletId: String, catId: Int, patId: String) =
        db.getDao().getSaveDataBySectionIdTest(hamletId, catId, patId)

    suspend fun getDataForQuestion(hamletId: String, catId: Int, patId: String, questionno: Int) =
        db.getDao().getDataForQuestion(hamletId, catId, patId, questionno)

    suspend fun deleteHamletData() =
        db.getDao().deleteHamletData()


    suspend fun deleteAnswerData(sectionId: Int,hamletId: String) =
        db.getDao().deleteAnswerData(sectionId,hamletId)

    suspend fun deletePatRecord(meetingUUID: String) =
        db.getDao().deletePatRecord(meetingUUID)

    suspend fun addModelForSurveyId(arrayListSurveyData3: ModelForSurveyId) =
        db.getDao().addModelForSurveyId(arrayListSurveyData3)
    suspend fun getModelForSurveyId() =
        db.getDao().getModelForSurveyId()

    suspend fun deleteModelForSurveyId(surveyId: String) =
        db.getDao().deleteModelForSurveyId(surveyId)

    // for upload data
    suspend fun getAayAewnAdikarExported(exportedValue: Int, updated: Int, surveyId: String?) =
        db.getDao().getAayAewnAdikarExported(exportedValue, updated, surveyId)

    // for upload data
    suspend fun getSurveyData1(exportedValue: Int, surveyId:String?) =
        db.getDao().getSurveyData1Exported(exportedValue, surveyId)

    suspend fun saveApproveData(catId: Int, patId: String, hamletId: String) =
        db.getDao().saveApproveData(hamletId, catId, patId)

    suspend fun getAnswerListForScore(): List<QuestionTypeMaster> =
        db.getDao().getAnswerListForScore()

    suspend fun getQuestionControl(qId: Int, endmes: String): tblAnswer =
        db.getDao().getQuestionControl(qId, endmes)

    suspend fun updateApproveData(
        reason: String,
        reasonCode: String,
        catId: Int,
        patId: String,
        hamletId: String,
    ) =
        db.getDao().updateApproveData(reason, reasonCode, hamletId, catId, patId)

    suspend fun getControlMasterDataOption(qId: Int): List<ControlDataMaster> =
        db.getDao().getControlMasterDataOption(qId)

    suspend fun getControlMasterDataOption1(qId: Int): List<ControlDataMaster> =
        db.getDao().getControlMasterDataOption1(qId)

    suspend fun updateVprpAnswerDataModelSendDataExported(list: List<tblAnswer>) =
        db.getDao().updateVprpAnswerDataModelSendDataExported(list)

    //updates
    suspend fun updateVprpMeetingSavetblDataExported(list: List<AddHamlettbl>) =
        db.getDao().updateVprpMeetingSavetblDataExported(list)

    suspend fun saveUploadImage(tblUploadImage: tblUploadImage) =
        db.getDao().saveUploadImage(tblUploadImage)

    suspend fun uploadImageData(base64: String?, imagename: String?,patId: String?, hammletId: String?,userId:String): UploadResponse {
        return apiRequest { api2.uploadImage(base64.toString(), imagename.toString(),patId.toString(),hammletId.toString(),userId) }
    }

    suspend fun getImageData(surveyId: String): List<tblUploadImage> =
        db.getDao().getImageData(surveyId)

}