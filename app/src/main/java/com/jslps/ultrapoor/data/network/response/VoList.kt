package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

const val void = 0

@Entity(indices = [Index(value = ["ID"], unique = true)])
data class VoList(
    @SerializedName("ID")
    var ID: Int? = null,

    @SerializedName("userprofile_ID")
    var userprofileID: Int? = null,

    @SerializedName("Village_ID")
    var VillageID: String? = null,

    @SerializedName("Active")
    var Active: Boolean? = null,

    @SerializedName("Created_by")
    var CreatedBy: String? = null,

    @SerializedName("Created_date")
    var CreatedDate: String? = null,

    @SerializedName("vocode")
    var vocode: String? = null,

    @SerializedName("statecode")
    var statecode: String? = null,

    @SerializedName("Districtcode")
    var Districtcode: String? = null,

    @SerializedName("Blockcode")
    var Blockcode: String? = null,

    @SerializedName("Clustercode")
    var Clustercode: String? = null,

    @SerializedName("Villagecode")
    var Villagecode: String? = null,

    @SerializedName("voname")
    var voname: String? = null,


) : Serializable {
    override fun toString(): String = voname.toString()

    @PrimaryKey(autoGenerate = true)
    var martervoListId: Int = void
}