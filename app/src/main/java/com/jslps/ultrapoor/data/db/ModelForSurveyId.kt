package com.jslps.ultrapoor.data.db

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

const val ModelForSurveyIdkey = 0

@Entity(indices = [Index(value = ["exportedHammletId"], unique = true)])
data class ModelForSurveyId(
    val exportedHammletId: String,
    val dataUpload: Boolean
) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var ModelForSurveyIdprimarykey: Int = ModelForSurveyIdkey


}