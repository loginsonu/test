package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable


const val USER_PROFILE_DATA_ID = 0

@Entity(indices = [Index(value = ["userID"], unique = true)])
data class UserProfileData(
    @SerializedName("UserID") val userID: Int,
    @SerializedName("Password") val password: String,
    @SerializedName("Full_Name") val full_Name: String,
    @SerializedName("User_Name") val user_Name: String,
    @SerializedName("Active") val active: Boolean,
    @SerializedName("Created_by") val created_by: String,
    @SerializedName("Created_date") val created_date: String,
    @SerializedName("UserType") val userType: String,
    @SerializedName("District_ID") val district_ID: Int,
    @SerializedName("Block_ID") val block_ID: Int,
    @SerializedName("Cluster_ID") val cluster_ID: String,
    @SerializedName("Village_ID") val village_ID: String,
    @SerializedName("Status") val status: Int,
    @SerializedName("Email_ID") val email_ID: String,
    @SerializedName("districtname") val districtname: String?,
    @SerializedName("BlockName") val blockName: String,
    @SerializedName("ClusterName") val clusterName: String?,
    @SerializedName("VillageName") val villageName: String?,
    @SerializedName("Districtname_h") val districtname_h: String?,
    @SerializedName("BlockName_H") val blockName_H: String?,
    @SerializedName("ClusterName_H") val clusterName_H: String?,
    @SerializedName("VillageName_H") val villageName_H: String?,
    ) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var userProfileDataId: Int = USER_PROFILE_DATA_ID
}