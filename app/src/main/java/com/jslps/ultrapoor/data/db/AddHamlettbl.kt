package com.jslps.ultrapoor.data.db

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

const val HAMLET_ID = 0

@Entity(indices = [Index(value = ["hamletId"], unique = true)])
data class AddHamlettbl(
    val DistrictID: Int,
    val BlockID: Int,
    val ClusterID: String,
    val VillageID: String,
    val hamletId:String?,
    var hamletName:String?,
    var isExported: Int,
    val CreatedOn: Date,
    val CreatedBy: String,

    ): Serializable {
    @PrimaryKey(autoGenerate = true)
    var primaryKeyHalmetId: Int = HAMLET_ID
}

