package com.jslps.ultrapoor.data.network.response

data class AnswerValueFill(
    val CatID: Int,
    val ControlID: Int,
    val Qid: Int,
    val ControlName: String?,
    val SectionID : String,
    var answerData:String,
    var questionToBEAdd:String
)