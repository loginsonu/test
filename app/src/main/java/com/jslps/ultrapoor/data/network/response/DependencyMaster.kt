package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

const val DependencyId = 0

@Entity(indices = [Index(value = ["RecordID"], unique = true)])
data class DependencyMaster(
    val RecordID: Int,
    val ControlId: Int,
    val No: Int,
    val Qid: Int,
    val Yes: Int

    ) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var DependencyIdPrimary: Int = DependencyId
}