package com.jslps.ultrapoor.data.network.response

import com.jslps.ultrapoor.data.db.AddHamlettbl
import com.jslps.ultrapoor.data.db.tblAnswer
import com.jslps.ultrapoor.data.db.tblUploadImage

data class Data(
    val ActivityListMaster: List<ActivityListMaster>,
    val ActivityMaster: List<ActivityMaster>,
    val ControlDataMaster: List<ControlDataMaster>,
    val ControlNameMaster: List<ControlNameMaster>,
    val QuestionTypeMaster: List<QuestionTypeMaster>,
    val UserProfileData: UserProfileData,
    val VillageList: List<Village>,
    val DependencyMaster: List<DependencyMaster>,
    val RejectReasonList: List<RejectReasonModelDb>,
    val VoList: List<VoList>,
    val AddHammletList: List<AddHamlettbl>,
    val HammletTxnList: List<tblAnswer>,
    val UpajDocument: List<tblUploadImage>,

    )