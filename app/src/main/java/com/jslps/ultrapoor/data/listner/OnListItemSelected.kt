package com.jslps.ultrapoor.data.listner

interface OnListItemSelected {
    fun onListItemSelected(
        count: Int, list: Any
    )
}