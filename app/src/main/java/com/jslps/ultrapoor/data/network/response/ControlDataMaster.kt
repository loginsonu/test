package com.jslps.ultrapoor.data.network.response

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable


const val CONTROL_DATA_MASTER_ID = 0

@Entity(indices = [Index(value = ["CID"], unique = true)])
data class ControlDataMaster(
    val CID: Int,
    val COID: Int,
    val ControlData: String,
    val QID: Int,
    val Sort: Int,
    val Status: Int,
    val Score:String?
): Serializable {
    override fun toString(): String {
        return this.ControlData // What to display in the Spinner list.
    }
    @PrimaryKey(autoGenerate = true)
    var controlDataMasterId: Int = CONTROL_DATA_MASTER_ID
}