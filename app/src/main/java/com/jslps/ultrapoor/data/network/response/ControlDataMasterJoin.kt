package com.jslps.ultrapoor.data.network.response

import java.io.Serializable

data class ControlDataMasterJoin(
    val CID: Int,
    val COID: Int,
    val ControlData: String,
    val SectionID: String,
    val CatID: String,
    val QID: Int,
    val Sort: Int,
    val Status: Int,
    val Score:String?,
    val controlDataMasterId: Int
): Serializable {
    override fun toString(): String {
        return this.ControlData // What to display in the Spinner list.
    }
}