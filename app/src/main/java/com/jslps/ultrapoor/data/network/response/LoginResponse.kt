package com.jslps.ultrapoor.data.network.response


data class LoginResponse<T>(
    val code: String,
    val data: Data,
    val message: String
)