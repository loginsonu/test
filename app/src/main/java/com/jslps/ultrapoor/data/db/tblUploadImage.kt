package com.jslps.ultrapoor.data.db

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable


const val IMGAGE_ID = 0

@Entity(indices = [Index(value = ["patId"], unique = true)])
data class tblUploadImage(
    val imageName: String,
    val patId: String,
    val base64: String?,
    val hammletId: String,
    val isExported: Int,
    val imageURL:String
) : Serializable {
    @PrimaryKey(autoGenerate = false)
    var id: Int = IMGAGE_ID
}
