package com.jslps.ultrapoor.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

const val CURRENT_USER_ID = 0

@Entity
data class User(
    val userid:String?,
    val password:String?,
    val VillageCode: String?,
    val VillageName: String?,
    val VillageName_H: String?


): Serializable {
    @PrimaryKey(autoGenerate = false) // this is false only to store one user
    var id: Int = CURRENT_USER_ID
}

//serialization is used when this class is passed to next class as argumnets better to keep it
