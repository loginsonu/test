package com.jslps.ultrapoor.data.db

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*




@Entity(indices = [Index(value = ["HamletID","PatId","QID"], unique = true)])
data class newtblAnswer(
    val DistrictID: Int,
    val BlockID: Int,
    val ClusterID: Long,
    val VillageID: Long,
    val HamletID: String,
    val vocode: String,
    val PatId: String,
    val SectionID: Int,
    val CatID: Int,
    val QID: Int,
    val AnswerValue: String,
    val ControlTypeID: Int,
    var AnswerID: String,
    var isExported: Int,
    val isUpdated: Int,
    val ultrapoor: Int,
    val CreatedOn: Date,
    val CreatedBy: String,
    val uuid: String,
    val statusAcepptandRej: Int,
    val reasonText: String,
    val reasonCode: String,
    val Score:String,
    val questionToBeAdd:String?
) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var answer: Int = 0
}
