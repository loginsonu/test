package com.jslps.ultrapoor.data.db

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.jslps.ultrapoor.data.network.response.*


@Dao
interface AppDao {

    @Query("SELECT count(*) from User WHERE UserID= :userId and Password= :password limit 1")
    suspend fun getUser(userId: String, password: String): Int

    @Insert(onConflict = REPLACE)
    suspend fun addUser(user: User): Long?

    @Query("SELECT * from AddHamlettbl where VillageID=:villageID")
    suspend fun getHmaletList(villageID:String): List<AddHamlettbl>

    @Insert(onConflict = REPLACE)
    suspend fun saveHamletList(villageList: List<AddHamlettbl>): Array<Long>

    @Insert(onConflict = REPLACE)
    suspend fun saveVoList(villageList: List<VoList>): Array<Long>

    @Insert(onConflict = REPLACE)
    suspend fun saveVillageList(villageList: List<Village>): Array<Long>


    @Insert(onConflict = REPLACE)
    suspend fun saveActivityMaster(activityMaster: List<ActivityMaster>): Array<Long>

    @Insert(onConflict = REPLACE)
    suspend fun saveRejectreasonList(activityMaster: List<RejectReasonModelDb>): Array<Long>

    @Insert(onConflict = REPLACE)
    suspend fun saveActivityListMaster(activityListMaster: List<ActivityListMaster>): Array<Long>


    @Insert(onConflict = REPLACE)
    suspend fun saveControlDataMaster(controlDataMaster: List<ControlDataMaster>): Array<Long>


    @Insert(onConflict = REPLACE)
    suspend fun saveControlNameMaster(controlNameMaster: List<ControlNameMaster>): Array<Long>


    @Insert(onConflict = REPLACE)
    suspend fun saveQuestionTypeMaster(questionTypeMaster: List<QuestionTypeMaster>): Array<Long>

    @Insert(onConflict = REPLACE)
    suspend fun saveDependencyMaster(questionTypeMaster: List<DependencyMaster>): Array<Long>

    @Insert(onConflict = REPLACE)
    suspend fun saveUserProfileData(userProfileData: UserProfileData)


    //SAVE SECTION ANSWER
    @Insert(onConflict = REPLACE)
    suspend fun saveSectionAnswer(saveAnswerList: List<tblAnswer>): Array<Long>


    // UPDATE SECTION ANSWER

    @Query("UPDATE  tblAnswer SET AnswerValue =:answer WHERE QID = :questionID")
    suspend fun updateAnswer(answer: String, questionID: String)

    /// @Query("SELECT * FROM tblAnswer WHERE SectionID = :sectionId")
    @Query("SELECT EXISTS(SELECT * FROM tblAnswer WHERE SectionID = :sectionId)")
    suspend fun isRowIsExist(sectionId: Int): Boolean

    //GET QUESTION SECTION
    @Query("SELECT * from ActivityListMaster where CatID=:catId")
    suspend fun getQuestionSection(catId: Int): List<ActivityListMaster>

    @Query("SELECT * from RejectReasonModelDb where Status='1'")
    suspend fun getRejectReasonList(): List<RejectReasonModelDb>


    @Query("SELECT * from ActivityListMaster where CatID=:catId")
    suspend fun getActivityListMaster(catId: Int): List<ActivityListMaster>


    //
    @Query("SELECT * from QuestionTypeMaster where CatID=:catId and Staus='1' and SectionID=:sectionId and Dependency='No' ")
    suspend fun getQuestionList(catId: Int, sectionId: String): List<QuestionTypeMaster>

    @Query("SELECT * from QuestionTypeMaster where Qid=:qid")
    suspend fun getQuestionListHH(qid: Int): QuestionTypeMaster

    @Query("SELECT * from QuestionTypeMaster where CatID=:catId and Staus='1' and SectionID=:sectionId and Dependency='No' order by Sort ")
    suspend fun getQuestionListPat(catId: Int, sectionId: String): List<QuestionTypeMaster>

    @Query("SELECT * from ControlDataMaster where Status =:status and QID=:qid and COID=:coid order by Sort ")
    suspend fun getControlMasterData(status: Int, qid: Int, coid: Int): List<ControlDataMaster>

    @Query("SELECT * from QuestionTypeMaster where CatID='1'")
    suspend fun getAllQuestionList(): List<QuestionTypeMaster>

    @Query("SELECT * from Village")
    suspend fun getVilageList(): List<Village>

    @Query("SELECT * from UserProfileData")
    suspend fun getUserProfileData(): UserProfileData

    @Query("SELECT * from Village where userprofile_ID=:userprofile_ID")
    suspend fun getVillageId(userprofile_ID: Int): Village

    @Query("SELECT * from ActivityListMaster where CatID=:catId and Active = 1 ")
    suspend fun getActivityListMasterPat(catId: Int): List<ActivityListMaster>

    @Query("select t1.*,t2.SectionId,t2.CatID from ControlDataMaster t1 INNER JOIN QuestionTypeMaster t2  on t1.Qid = t2.qid where t2.CatID=:catID and t2.SectionID=:sectionId and t1.Status='1'")
    suspend fun getControlDataMaster(catID: String, sectionId: String): List<ControlDataMasterJoin>

    @Query("select t1.*,t2.SectionId,t2.CatID from ControlDataMaster t1 INNER JOIN QuestionTypeMaster t2  on t1.Qid = t2.qid where t2.CatID=:catID  and t1.Status='1'")
    suspend fun getControlDataMasterNew(catID: String): List<ControlDataMasterJoin>

    @Query("select * from DependencyMaster t1 inner join questiontypemaster t2 on t1.Yes=t2.Qid where t1.Qid=:Qid and t1.ControlId=:controlId and t2.Staus='1'")
    suspend fun getDependencyData(Qid: Int, controlId: Int): List<QuestionTypeMasterJoin>

    @Query("SELECT PatId,*  FROM newtblAnswer where HamletID=:hamletId and CatID='1' GROUP BY PatId ")
    suspend fun getPatSurveyList(hamletId: String): List<newtblAnswer>

    @Query("SELECT PatId,*  FROM tblAnswer where HamletID=:hamletId and CatID='1' GROUP BY PatId ")
    suspend fun getPatListApprove(hamletId: String): List<tblAnswer>

    @Query("SELECT Sum(Score)  FROM tblAnswer where HamletID=:hamletId and CatID='1' and PatId=:patId")
    suspend fun getPatListApproveSum(hamletId: String, patId: String): Int

    @Query("SELECT PatId,*  FROM tblAnswer where HamletID=:hamletId and CatID='1' and statusAcepptandRej='1' GROUP BY PatId ")
    suspend fun getPatListAfterApprove(hamletId: String): List<tblAnswer>

    @Query("SELECT *  FROM volist")
    suspend fun getVoList(): List<VoList>

    // get Save Data
    @Query("SELECT * from tblAnswer where CatID=:catId and HamletID=:hamletId and SectionID=:sectionId and PatId=:patId")
    suspend fun getSaveDataBySectionId(
        hamletId: String,
        catId: Int,
        sectionId: Int,
        patId: String,
    ): List<tblAnswer>

    @Query("SELECT * from tblAnswer where CatID=:catId and HamletID=:hamletId")
    suspend fun getSaveDataBySectionIdTest(
        hamletId: String,
        catId: Int,
    ): List<tblAnswer>

    @Query("SELECT * from tblAnswer where CatID=:catId and HamletID=:hamletId and PatId=:patId")
    suspend fun getSaveDataBySectionIdTest(
        hamletId: String,
        catId: Int,
        patId: String,

        ): List<tblAnswer>

    @Query("SELECT * from tblAnswer where CatID=:catId and HamletID=:hamletId and PatId=:patId and QID=:qid limit 1")
    suspend fun getDataForQuestion(
        hamletId: String,
        catId: Int,
        patId: String,
        qid: Int,
    ): List<tblAnswer>

    @Query("DELETE FROM AddHamlettbl")
    suspend fun deleteHamletData()

    @Query("SELECT * from ModelForSurveyId")
    suspend fun getModelForSurveyId(): List<ModelForSurveyId>

    @Query("DELETE FROM tblAnswer WHERE SectionID=:sectionId and HamletID=:hamletId")
    suspend fun deleteAnswerData(sectionId: Int, hamletId: String)

    @Query("Delete  from tblAnswer WHERE  PatId= :meetingUUID")
    suspend fun deletePatRecord(meetingUUID: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addModelForSurveyId(user: ModelForSurveyId): Long

    @Query("DELETE from ModelForSurveyId WHERE  exportedHammletId= :surveyId")
    suspend fun deleteModelForSurveyId(surveyId: String)

    @Query("SELECT * from tblAnswer where (IsExported= :exported OR IsUpdated= :updated) and HamletID=:surveyId")
    suspend fun getAayAewnAdikarExported(
        exported: Int,
        updated: Int,
        surveyId: String?,
    ): List<tblAnswer>

    @Query("SELECT * from AddHamlettbl where IsExported= :exported and hamletId=:surveyId")
    suspend fun getSurveyData1Exported(exported: Int, surveyId: String?): List<AddHamlettbl>

    @Query("UPDATE  tblAnswer SET statusAcepptandRej ='1' WHERE PatId = :patId and CatID=:catId and HamletID=:hamletId")
    suspend fun saveApproveData(hamletId: String, catId: Int, patId: String)

    @Query("select distinct questiontypemaster.*  from questiontypemaster  inner join controldatamaster  on questiontypemaster.Qid = controldatamaster.Qid where  controldatamaster.Score>=0 and controldatamaster.Score IS NOT NULL and questiontypemaster.CatID='1' ")
    suspend fun getAnswerListForScore(
    ): List<QuestionTypeMaster>

    @Query("SELECT * FROM tblAnswer WHERE QID=:qid and PatId=:endmes")
    suspend fun getQuestionControl(qid: Int, endmes: String): tblAnswer

    @Query("UPDATE  tblAnswer SET statusAcepptandRej ='2',reasonText= :reason,reasonCode =:reasonCode WHERE PatId = :patId and CatID=:catId and HamletID=:hamletId")
    suspend fun updateApproveData(
        reason: String,
        reasonCode: String,
        hamletId: String,
        catId: Int,
        patId: String,
    )

    @Query("SELECT * from ControlDataMaster where QID =:qid and Status='1'")
    suspend fun getControlMasterDataOption(qid: Int): List<ControlDataMaster>

    @Query("SELECT * from tblUploadImage where hammletId =:hammletId ")
    suspend fun getImageData(hammletId: String): List<tblUploadImage>

    @Query("SELECT * from ControlDataMaster where QID =:qid and Status='1' Limit 1")
    suspend fun getControlMasterDataOption1(qid: Int): List<ControlDataMaster>

    @Update
    suspend fun updateVprpAnswerDataModelSendDataExported(shgDataa: List<tblAnswer>?)

    @Update
    suspend fun updateVprpMeetingSavetblDataExported(shgDataa: List<AddHamlettbl>?)

    @Insert(onConflict = REPLACE)
    suspend fun saveUploadImage(tblUploadImage: tblUploadImage)
}