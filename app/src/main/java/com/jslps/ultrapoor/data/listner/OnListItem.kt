package com.jslps.ultrapoor.data.listner

import com.jslps.ultrapoor.data.db.AddHamlettbl

interface OnListItem {
    fun onListItemSelected(
        count: Int, list: AddHamlettbl?, s: String
    )
}