package com.jslps.ultrapoor.data.network.response

data class AnswerValueFillNew(
    val CatID: Int,
    val ControlID: Int,
    val Qid: Int,
    var ControlName: String?,
    val SectionID: String,
    var answerData: String,
    var questionToBEAdd: String,
    var answerId: String,
    var score: String,
    var priority: Int,
    var answerIdDelete: String,
    var uuid:String
)